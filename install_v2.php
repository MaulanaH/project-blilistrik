<?php
require __DIR__.'/../../laravel/vendor/autoload.php';


$app = require_once __DIR__.'/../../laravel/bootstrap/app.php';
$kernel = $app->make(Illuminate\Contracts\Console\Kernel::class);
echo 'Installing...<br>';
$kernel->call('migrate', ['--force' => true]);
// redirect
echo "<script>window.location = '$site_url'</script>";