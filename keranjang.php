<?php 
if(isset($_POST['form']) && $_POST['form']!=null){
	require_once '../koneksi/koneksi.php';
	if(isset($_SESSION['user']['login'])){
		$ruser = chk_login();
	}
	$total = 0;
	if(!isset($_SESSION['user']['keranjang'])){
		$_SESSION['user']['keranjang'] = array();
	}					

	function get_sumjum($id,$token){
		$koneksi = $GLOBALS['koneksi'];
		$qbarker = $koneksi->query("SELECT SUM(jumlah)AS jumlah FROM keranjang where id_barang = $id AND token NOT LIKE '$token'");
		$rbarker = $qbarker->fetch_assoc();
		$stok = $rbarker['jumlah'] ;
		return $stok;
	}
	switch ($_POST['form']) {
		case 'add_keranjang':
			foreach ($_SESSION['user']['keranjang'] as $key => $value) {
				if($_POST['id'] == $value['id']){
					$qweqwe=1;
					$qbarang = mysqli_query($koneksi,"SELECT * FROM barang where id_barang = ".$value['id']);
					$rbarang =  mysqli_fetch_assoc($qbarang);
					$jumlah = $value['jumlah']+get_sumjum($value['id'],$value['token']);
					if($jumlah<$rbarang['stok_barang']){
					$_SESSION['user']['keranjang'][$key]['jumlah']++;
						if(isset($_SESSION['user']['login'])){
							mysqli_query($koneksi,sprintf("UPDATE `keranjang` SET `jumlah`='%s' WHERE `token` = '%s'",$_SESSION['user']['keranjang'][$key]['jumlah'],$value['token']));
							mysqli_query($koneksi,sprintf("UPDATE `invoice` SET `jumlah`='%s' WHERE `token` = '%s'",$_SESSION['user']['keranjang'][$key]['jumlah'],$value['token']));
						}

					echo json_encode(array('notif'=>"Barang telah ada dalam keranjang", 'jumlah'=>$_SESSION['user']['keranjang'][$key]['jumlah']));
				}else{
					echo json_encode(array('notif'=>"Barang telah dalam jumlah maksimum", 'jumlah'=>$_SESSION['user']['keranjang'][$key]['jumlah']));
				}
				}
			}	
			if(!isset($qweqwe)){
				array_push($_SESSION['user']['keranjang'],array('id'=>$_POST['id'],'jumlah'=>1,'token'=>$token));
				if(isset($_SESSION['user']['login'])){
					mysqli_query($koneksi,sprintf("INSERT INTO `keranjang`(`id_user`, `id_barang`, `jumlah`, `token`) values('%s','%s','%s','%s')",$ruser['id_user'],$_POST['id'],1,$token));
				}
				$qbarang = mysqli_query($koneksi,"SELECT * FROM barang where id_barang = ".$_POST['id']);
				$rbarang = mysqli_fetch_assoc($qbarang);
				$totals = $rbarang['harga_barang'];
				echo json_encode(array('count'=>get_countker(),'notif'=>null));
			}
		break;
		case 'plus_keranjang':
			foreach ($_SESSION['user']['keranjang'] as $key => $value) {
				if($_POST['id'] == $value['id']){
					$qbarang = mysqli_query($koneksi,"SELECT * FROM barang where id_barang = ".$value['id']);
					$rbarang =  mysqli_fetch_assoc($qbarang);
					$jumlah = $value['jumlah']+get_sumjum($value['id'],$value['token']);
					if($jumlah<$rbarang['stok_barang']){
					$_SESSION['user']['keranjang'][$key]['jumlah']++;
						if(isset($_SESSION['user']['login'])){
							mysqli_query($koneksi,sprintf("UPDATE `keranjang` SET `jumlah`='%s' WHERE `token` = '%s'",$_SESSION['user']['keranjang'][$key]['jumlah'],$value['token']));

						}
					}
					$total="Rp. ".number_format($rbarang['harga_barang']*$_SESSION['user']['keranjang'][$key]['jumlah'])." ";
					$x=0;
					if(get_diskon($value['id'],$_SESSION['user']['keranjang'][$key]['jumlah'],"umum")){
						$total .= "<i class='diskon_umum'>".($x>0?"+":"").get_diskon($value['id'],$_SESSION['user']['keranjang'][$key]['jumlah'],"umum")."%</i>";
					$x++;
					}
					if (get_diskon($value['id'],$_SESSION['user']['keranjang'][$key]['jumlah'],"reseller"!=null)) {
						$total .= "<i class='diskon_reseller'>".($x>0?"+":"").get_diskon($value['id'],$_SESSION['user']['keranjang'][$key]['jumlah'],"reseller")."%</i>";
						$x++;
					}

					echo json_encode(array('jumlah'=>$_SESSION['user']['keranjang'][$key]['jumlah'],'total'=>$total,'totals'=>"Rp. ".number_format(get_totals())));
				}
			}	
		break;		
		case 'minus_keranjang':
			foreach ($_SESSION['user']['keranjang'] as $key => $value) {
				if($_POST['id'] == $value['id']){
					$qbarang = mysqli_query($koneksi,"SELECT * FROM barang where id_barang = ".$value['id']);
					$rbarang =  mysqli_fetch_assoc($qbarang);
					if($value['jumlah']>1){
					$_SESSION['user']['keranjang'][$key]['jumlah']--;
						if(isset($_SESSION['user']['login'])){
							mysqli_query($koneksi,sprintf("UPDATE `keranjang` SET `jumlah`='%s' WHERE `token` = '%s'",$_SESSION['user']['keranjang'][$key]['jumlah'],$value['token']));
						}			
					}
					$total="Rp. ".number_format($rbarang['harga_barang']*$_SESSION['user']['keranjang'][$key]['jumlah'])." ";
					$x=0;
					if(get_diskon($value['id'],$_SESSION['user']['keranjang'][$key]['jumlah'],"umum")){
						$total .= "<i class='diskon_umum'>".($x>0?"+":"").get_diskon($value['id'],$_SESSION['user']['keranjang'][$key]['jumlah'],"umum")."%</i>";
					$x++;
					}
					if (get_diskon($value['id'],$_SESSION['user']['keranjang'][$key]['jumlah'],"reseller"!=null)) {
						$total .= "<i class='diskon_reseller'>".($x>0?"+":"").get_diskon($value['id'],$_SESSION['user']['keranjang'][$key]['jumlah'],"reseller")."%</i>";
						$x++;
					}
					echo json_encode(array('jumlah'=>$_SESSION['user']['keranjang'][$key]['jumlah'],'total'=>$total,'totals'=>"Rp. ".number_format(get_totals())));
				}
			}	
		break;
		case 'edit_keranjang':
			foreach ($_SESSION['user']['keranjang'] as $key => $value) {
				if($_POST['id'] == $value['id']){
					$qbarang = mysqli_query($koneksi,"SELECT * FROM barang where id_barang = ".$value['id']);
					$rbarang =  mysqli_fetch_assoc($qbarang);
					$vjumlah = $value['jumlah']+get_sumjum($value['id'],$value['token']);
					$pjumlah = $_POST['jumlah']+get_sumjum($value['id'],$value['token']);
					if($value['jumlah']>0 && $vjumlah<=$rbarang['stok_barang'] && $_POST['jumlah']>0 && $pjumlah<=$rbarang['stok_barang']){
					$_SESSION['user']['keranjang'][$key]['jumlah']=$_POST['jumlah'];
						if(isset($_SESSION['user']['login'])){
							mysqli_query($koneksi,sprintf("UPDATE `keranjang` SET `jumlah`='%s' WHERE `token` = '%s'",$_SESSION['user']['keranjang'][$key]['jumlah'],$value['token']));

						}
					}
					$total="Rp. ".number_format($rbarang['harga_barang']*$_SESSION['user']['keranjang'][$key]['jumlah'])." ";
					$x=0;
					if(get_diskon($value['id'],$_SESSION['user']['keranjang'][$key]['jumlah'],"umum")){
						$total .= "<i class='diskon_umum'>".($x>0?"+":"").get_diskon($value['id'],$_SESSION['user']['keranjang'][$key]['jumlah'],"umum")."%</i>";
					$x++;
					}
					if (get_diskon($value['id'],$_SESSION['user']['keranjang'][$key]['jumlah'],"reseller"!=null)) {
						$total .= "<i class='diskon_reseller'>".($x>0?"+":"").get_diskon($value['id'],$_SESSION['user']['keranjang'][$key]['jumlah'],"reseller")."%</i>";
						$x++;
					}
					echo json_encode(array('jumlah'=>$_SESSION['user']['keranjang'][$key]['jumlah'],'total'=>$total,'totals'=>"Rp. ".number_format(get_totals())));
				}
			}	
		break;
		case 'delete_keranjang':
			foreach ($_SESSION['user']['keranjang'] as $key => $value) {
				if($_POST['id'] == $value['id']){
					$qbarang = mysqli_query($koneksi,"SELECT * FROM barang where id_barang = ".$value['id']);
					$rbarang =  mysqli_fetch_assoc($qbarang);
					unset($_SESSION['user']['keranjang'][$key]);
						if(isset($_SESSION['user']['login'])){
							mysqli_query($koneksi,sprintf("DELETE FROM keranjang where token = '%s'",$value['token']));
						}
					echo json_encode(array('total'=>"Rp. ".number_format($total),'totals'=>"Rp. ".number_format(get_totals()),'count'=>get_countker()));
				}
			}	
		break;
		case 'delete_all_keranjang':
			unset($_SESSION['user']['keranjang']);
			if(isset($_SESSION['user']['login'])){
				mysqli_query($koneksi,sprintf("DELETE FROM keranjang where id_user = '%s'",$ruser['id_user']));
			}
			echo json_encode(null);
		break;
		default:
			# code...
			break;
	}
}else{
	require_once '../header.php'; 
?>
<style type="text/css">
	#totalss{
		width: 400px;
		float: right;
	}
	#btn_ok{
		float: right;
	}
	@media(max-width: 768px){
		#cart-table-header{
			display: none;
		}
		#totalss{
			width: unset;
			float: none;
		}
		#btn_ok{
			width: 100%;
			float: unset;
			    -webkit-box-orient: vertical;
    -webkit-box-direction: normal;
    -ms-flex-direction: column;
    flex-direction: column;
    -webkit-box-align: start;
    -ms-flex-align: start;
    align-items: flex-start;
    -webkit-box-pack: center;
    -ms-flex-pack: center;
    justify-content: center;
	}
	#btn_ok > .btn:not(:last-child):not(.dropdown-toggle), .btn-group-vertical > .btn-group:not(:last-child) > .btn {
	    border-bottom-right-radius: 0;
	    border-bottom-left-radius: 0;
	}
	#btn_ok:not(:last-child):not(.dropdown-toggle), .btn-group > .btn-group:not(:last-child) > .btn {
	    border-top-right-radius: 0;
	    border-bottom-right-radius: 0;
	}
	#btn_ok > .btn:not(:first-child), .btn-group-vertical > .btn-group:not(:first-child) > .btn {
	    border-top-left-radius: 0;
	    border-top-right-radius: 0;
	}
	#btn_ok > .btn + .btn, .btn-group-vertical > .btn + .btn-group, .btn-group-vertical > .btn-group + .btn, .btn-group-vertical > .btn-group + .btn-group {
	    margin-top: -1px;
	    margin-left: 0;
	}
		#btn_ok .btn{
			width: 100%;
		}
	}
</style>
<body>
<div class="container-fluid " style="width: 100%;padding: 0; margin-top: 100px">
	<div class="row" style="margin: unset;">
		<div class="col-lg-2 mb-4">
			<?php get_sidebar("keranjang"); ?>
		</div>
	  	<div class="col-lg-10">
			<div class="card mx-auto card-body-putih">
			  <div class="card-header bg-biru text-white text-white h4">Informasi Keranjang</div>
				<div class="card-body card-body-putih text-center" style="margin: 0;" id="cart_content">
					<?php 
					$xx=0;
					if(count($_SESSION['user']['keranjang'])==0){
						echo "<h3 class='w-100 text-capitalize'>Tidak ada barang lagi dikeranjang</h3>";
					}else{?>
						<div class="row border-bottom border-dark h5 card-text" id="cart-table-header" style="height: 50px;">
				  			<div class="col-1" style="padding: 0">
						    	<p class="">No</p>
					  		</div>
							<div class="col-1" style="padding: 0">
								<p class="">Gambar</p>
							</div>
							<div class="col-5" style="padding: 0">
								<p class="">Nama Barang</p>
							</div>
							<div class="col-2" style="padding: 0">
							 	<p class="">Total Harga</p>
							</div>
							<div class="col-2" style="padding: 0">
								<p class="">Jumlah Barang</p>
							</div>
							<div class="col-1" style="padding: 0">
								<p class="">Aksi</p>
							</div>
						</div>
						<?php 
						$qbarang = mysqli_query($koneksi, "SELECT * FROM barang");
						while ($rbarang = mysqli_fetch_assoc($qbarang)) {
							foreach ($_SESSION['user']['keranjang'] as $key => $value) {
								if($rbarang['id_barang']==$value['id']){
									$xx++;
									$total="Rp. ".number_format($rbarang['harga_barang']*$_SESSION['user']['keranjang'][$key]['jumlah'])." ";
					$x=0;
					if(get_diskon($value['id'],$_SESSION['user']['keranjang'][$key]['jumlah'],"umum")){
						$total .= "<i class='diskon_umum'>".($x>0?"+":"").get_diskon($value['id'],$_SESSION['user']['keranjang'][$key]['jumlah'],"umum")."%</i>";
					$x++;
					}
					if (get_diskon($value['id'],$_SESSION['user']['keranjang'][$key]['jumlah'],"reseller"!=null)) {
						$total .= "<i class='diskon_reseller'>".($x>0?"+":"").get_diskon($value['id'],$_SESSION['user']['keranjang'][$key]['jumlah'],"reseller")."%</i>";
						$x++;
					}?>	
						<div class="row my-1 p-3 p-lg-0" style="background-color: <?php if($xx%2==0){echo "#EEEEEE";} ?> ">
					  		<div class="col-1 my-auto" style="padding: 0">
						    	<p class="card-text text-center"><?php echo $xx?></p>
						  	</div>
						  	<div class="col-lg-1 my-auto" style="padding: 0">
						    	<img class="card-img-top rounded" src="<?php echo $rbarang['foto_barang'];?>" alt="Card image cap" style="height: 75px;width: 75px">
						  	</div>
						  	<div class="card-text col-lg-5 my-auto"><?php echo $rbarang['nama_barang']?></div>
						  	<div class="col-lg-2 my-auto">

						    	<p class="card-text" id="barang_total<?php echo $value['id']?>"><?php echo $total;?></p>
						  	</div>
						  	<div class="col-lg-2 col-6 my-auto" style="">
						  		<div class="input-group">
						  			<button class="input-group-btn form-control btn btn-outline-secondary minus_keranjang" style="font-weight: bold;margin: auto;" type="button" id="" valid="<?php echo $value['id']?>">-</button>
						  			<input type="text" min=1 value="<?php echo $value['jumlah']?>" class="form-control barang_jumlah text-center" id="barang_jumlah<?php echo $value['id']?>" valid="<?php echo $value['id']?>">
						  			<button class="input-group-btn form-control btn btn-outline-secondary plus_keranjang" style="font-weight: bold;margin: auto;" type="button" id="" valid="<?php echo $value['id']?>">+</button>
						  		</div>
						  	</div>
						  	<div class="col-lg-1 col-6 my-auto">
						    	<button class="btn btn-outline-danger delete_keranjang float-right float-lg-none" valid="<?php echo $value['id']?>"><i class="material-icons">remove_shopping_cart</i></button>
						  	</div>
						</div>
						<?php }}} ?>
						<div>
							<div class="" >
								<div class="input-group row" id="totalss">
									<label class="form-control col-4 col-lg-3" style="width: 100px;padding-right: 5px;background-color: #ced4da;">TOTAL : </label>
									<input type="text" name="" class="form-control col-auto " disabled="" id="totals" value="<?php echo "Rp. ".number_format(get_totals());?>" >
								</div>
							</div>
						</div>
						<?php } ?>
			    </div> 
			    <div class="card-footer">
				    <div class="btn-group" id="btn_ok">
				    	<?php 
				    	if(count($_SESSION['user']['keranjang'])>0){
				    		?>
				        <button type="button" class="btn btn-outline-danger delete_button" id="delete_all_keranjang">Hapus Semua keranjang</button>    	
				        <a href="../index.php" class="btn btn-outline-secondary " >Lanjutkan Belanja</a>
				        <a href="pembayaran.php" class="btn btn-outline-success delete_button">Lanjutkan Pembayaran</a>
				    		<?php 	}else{?>
				        <a href="../index.php" class="btn btn-outline-secondary ">Lanjutkan Belanja</a>
				    	<?php }?>
			    	</div>
			  	</div>
			</div>
		</div>
	</div>
</div>
<?php 
require_once '../footer.php'; ?>
<script type="text/javascript">
	$(document).ready(function() {
		var total=<?php echo $x==null ? 0 : $x; ?>;
		var id=0;
		$('.minus_keranjang').click(function(event) {
			id=$(this).attr('valid');
			$.ajax({
				url: '',
				type: 'POST',
				dataType: 'json',
				data: {'form': 'minus_keranjang','id':id},
				success(data){
					$('#barang_jumlah'+id).val(data['jumlah']);
					$('#barang_total'+id).html(data['total']);
					$('#totals').val(data['totals']);

				}
			})
		});
		$('.plus_keranjang').click(function(event) {
			id=$(this).attr('valid');
			$.ajax({
				url: '',
				type: 'POST',
				dataType: 'json',
				data: {'form': 'plus_keranjang','id':id},
				success(data){
					$('#barang_jumlah'+id+'').val(data['jumlah']);
					$('#barang_total'+id).html(data['total']);
					$('#totals').val(data['totals']);
				}
			})
		});
		$('.barang_jumlah').change(function(event) {
			id=$(this).attr('valid');
			$.ajax({
				url: '',
				type: 'POST',
				dataType: 'json',
				data: {'form': 'edit_keranjang','id':id, 'jumlah':$(this).val()},
				success(data){
					$('#barang_jumlah'+id+'').val(data['jumlah']);
					$('#barang_total'+id).html(data['total']);
					$('#totals').val(data['totals']);
					}
			})
		});
var mql = window.matchMedia("screen and (max-width: 900px)")		
		$('.delete_keranjang').click(function(event) {
				id=$(this).attr('valid');
				$btn = $(this).parent().parent();
				$.ajax({
					url: '',
					type: 'POST',
					dataType: 'json',
					data: {'form': 'delete_keranjang','id':id},
					success(data){
						$('#totals').val(data['totals']);
			        	if(data['count']>0){
				            $('#countker').html(data['count']);
				            $('#mcount').html(data['count']);
				        }else{
				            $('#countker').addClass('d-none');
				            $('#mcount').addClass('d-none');
				        }
					}
				}).done(function() {
					total--;
					if(total==0){
						$('.card-body').html("<h3 class='w-100 text-capitalize'>Tidak ada barang lagi dikeranjang</h3>");
						$('.delete_button').remove();
					}else{
					$btn.remove();
				}
				})
		});
		$('#delete_all_keranjang').click(function(event) {
				$.ajax({
					url: '',
					type: 'POST',
					dataType: 'json',
					data: {'form': 'delete_all_keranjang'},
				}).done(function() {
					$('.card-body').html("<h3 class='w-100 text-capitalize'>Tidak ada barang lagi dikeranjang</h3>");
					$('.delete_button').remove();
			        $('#countker').html(0);
			        $('#countker').addClass('d-none');
			        $('#mcount').html(0);
			        $('#mcount').addClass('d-none');			        
				})
		});
	});
</script>

<?php }?>