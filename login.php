<?php
@session_start();
require_once "../koneksi/koneksi.php";
ob_start();
if(!isset($_SESSION['token_admin'])){
  $_SESSION['token_admin'] = $token;
}
if(isset($_SESSION['id_admin']['login']['username'])){
  header("location: ../index.php");
  //echo "masuk index";
} //echo md5("password".$_SESSION['token_admin'])
$qadmin = $koneksi->query("SELECT * FROM admin");
if($qadmin->num_rows==0){
  $koneksi->query("INSERT INTO `admin` (`id_admin`, `username`, `email`, `password`, `fnama`, `jenkel`, `created_at`, `updated_at`, `deleted_at`) VALUES (NULL, 'Supervisor', 'spv@blilistrik.com', MD5('supervisorblilistrik'), 'Supervisor', 'Pria', CURRENT_TIME(), CURRENT_TIME(), NULL);");
}
$form = md5("form".$_SESSION['token_admin']);
if(isset($_POST[$form]) && $_POST[$form] == md5("login".$_SESSION['token_admin'])){
$login = mysqli_query($koneksi,sprintf("SELECT * FROM  admin where username='%s' AND password='%s'",$_POST['username'],md5($_POST['password']))) or die(mysqli_error($koneksi));
  $ketemu = mysqli_num_rows($login);
  $data   = mysqli_fetch_assoc($login);
  if($ketemu>0){
    $_SESSION['username'] = $data['username'];
    $_SESSION['password'] = $data['password'];
    $_SESSION['id_admin'] = $data['id_admin'];
    echo "<script>alert('Anda Berhasil Login');window.location='admin.php';</script> ";
    //echo "1";
  }else{
    echo "<script>alert('Anda Gagal Login');window.location='login.php';</script> ";
    //echo "2";
  }
}
//if (isset($_POST['username']) && isset($_POST['password'])) {
//        $username = $_POST['username'];
//        $password = md5($_POST['password']);
//        $login = mysqli_query($koneksi, "SELECT * FROM  admin where username='$username' AND password='$password'") or die(mysqli_error($koneksi));
//
//        $ketemu = mysqli_num_rows($login);
//        $data   = mysqli_fetch_assoc($login);
//        
//        if ($ketemu == 1) {
//            $_SESSION['admin']['login']['username'] = $data['username'];
//            //echo "<script>alert('Anda Berhasil Login');window.location='../index.php';</script> ";
//           if(isset($_SESSION['halaman']) && $_SESSION['halaman']!=null){
//              header('location:'.$_SESSION['halaman']);
//            }else{
//            echo "2";
//            }
//            
//        } else {
//            echo "<script>alert('Data Invalid. Username atau Password Salah');window.location='login.php'</script>";
//        }
//    }
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Halaman Admin</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="../assets/vendor/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="../assets/vendor/font-awesome/css/font-awesome.min.css">
    <!-- Fontastic Custom icon font-->
    <link rel="stylesheet" href="../assets/fonts/fontastic.css">
    <!-- Google fonts - Poppins -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,700">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="../assets/css/style.default.css" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="../assets/css/custom.css">
    <!-- Favicon-->
    <link rel="shortcut icon" href="../assets/img/favicon.ico">
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
  </head>
  <body>
    <div class="page login-page">
      <div class="container d-flex align-items-center" style="width: 100%">
        <div class="form-holder has-shadow col-lg-6" style="margin: auto;">
          <div class="row">
            <!-- Logo & Information Panel-->
            <!-- Form Panel    -->
            <div class="col-lg-12 bg-white">
              <div class="form d-flex align-items-center p-4">
                <div class=" content ">
                  <form id="" method="post" action="" style="max-width: 100% !important;">
                    <div class="form-group col-lg-12">
                      <input id="login-username" type="text" name="username" required="" class="input-material">
                      <label for="login-username" class="label-material">User Name</label>
                    </div>
                    <div class="form-group col-lg-12">
                      <input id="login-password" type="password" name="password" required="" class="input-material">
                      <label for="login-password" class="label-material">Password</label>
                    </div>
                    <input type="hidden" name="<?php echo md5("form".$_SESSION['token_admin']) ?>" value="<?php echo md5("login".$_SESSION['token_admin']) ?>">
                      <input type="submit" value="masuk" class="btn btn-primary">
                    <!-- This should be submit button but I replaced it with <a> for demo purposes-->
                  </form>
                    <a href="#" class="forgot-pass">Forgot Password?</a><br><small>Do not have an account? </small><a href="register.html" class="signup">Signup</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="copyrights text-center">
        <p>Design by <a href="https://bootstrapious.com/admin-templates" class="external">Bootstrapious</a>
          <!-- Please do not remove the backlink to us unless you support further theme's development at https://bootstrapious.com/donate. It is part of the license conditions. Thank you for understanding :)-->
        </p>
      </div>
    </div>
    <!-- Javascript files-->
    <script src="../assets/vendor/jquery/jquery.min.js"></script>
    <script src="../assets/vendor/popper.js/umd/popper.min.js"> </script>
    <script src="../assets/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="../assets/vendor/jquery.cookie/jquery.cookie.js"> </script>
    <script src="../assets/vendor/jquery-validation/jquery.validate.min.js"></script>
    <!-- Main File-->
    <script src="../assets/js/front.js"></script>
  </body>
</html>