select 
`t`.`penerima_nama` AS `penerima_nama`,
`b`.`nama_barang` AS `nama_barang`,
`b`.`harga_barang` AS `harga_barang`,
`tb`.`jumlah` AS `jumlah`,
`d`.`diskon` AS `diskon`,
(`b`.`harga_barang` - `b`.`harga_barang` * `d`.`diskon` / 100) * `tb`.`jumlah` AS `subtotal`,`t`.`status` AS `status`,
`t`.`tanggal_transaksi` AS `tanggal_transaksi` 
from `transaksi` `t` 
join `transaksi_barang` `tb` on(`t`.`id_transaksi` = `tb`.`id_transaksi`) 
join `barang` `b` on(`tb`.`id_barang` = `b`.`id_barang`)
join `transaksi_diskon` `td` on(`tb`.`id_tranbar` = `td`.`id_tranbar`)
join `diskon` `d` on(`td`.`id_diskon` = `d`.`id_diskon`)
order by `t`.`tanggal_transaksi` desc

select *
from `transaksi` `t` 
join `transaksi_barang` `tb` on(`t`.`id_transaksi` = `tb`.`id_transaksi`) 
join `barang` `b` on(`tb`.`id_barang` = `b`.`id_barang`)
left join `transaksi_diskon` `td` on(`tb`.`id_tranbar` = `td`.`id_tranbar`)
left join `diskon` `d` on(`td`.`id_diskon` = `d`.`id_diskon`)
order by `t`.`tanggal_transaksi` desc

select 
t.id_transaksi AS id,
t.kode_transaksi AS kode,
t.id_user AS id_user,
t.id_admin AS id_admin,
t.status AS status,
t.resi AS resi,
 u.fname AS nama_user,
t.tanggal_transaksi AS tanggal,
from `transaksi` `t` 
join `transaksi_barang` `tb` on(`t`.`id_transaksi` = `tb`.`id_transaksi`) 
join `barang` `b` on(`tb`.`id_barang` = `b`.`id_barang`)
left join `transaksi_diskon` `td` on(`tb`.`id_tranbar` = `td`.`id_tranbar`)
left join `diskon` `d` on(`td`.`id_diskon` = `d`.`id_diskon`)
join users u  on t.id_user = u.id_user,
join  
order by `t`.`tanggal_transaksi` desc