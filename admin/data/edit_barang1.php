<?php
require_once "../../koneksi/koneksi.php";
$id    = $_GET['id'];
$query = $koneksi->query("SELECT * FROM barang b  join kategori_sub ks on b.id_sub_kategori = ks.id_sub_kategori WHERE id_barang='$id'");
$row   = $query->fetch_array();
?>
<div class="form-group">
    <label>
        Nama
    </label>
    <input class="form-control" id="edit_nama" name="nama" placeholder="Nama" type="text" value="<?php echo $row['nama_barang']; ?>">
    </input>
</div>
<div class="form-group">
    <label>
        Harga
    </label>
    <input class="form-control" id="edit_harga" name="harga" placeholder="Username" type="text" value="<?php echo $row['harga_barang']; ?>">
    </input>
</div>
<div class="form-group">
    <label>
        Stok
    </label>
    <input class="form-control" id="edit_stok" name="stok" placeholder="Stok Barang..." type="text" value="<?php echo $row['stok_barang']; ?>">
    </input>
</div>
<div class="form-group">
    <label for="kategori2">
        Kategori
    </label>
    <select class="form-control" id="kategori2" name="kategori2">
        <option disabled="disabled" selected="selected">
            Pilih Kategori
        </option>
        <?php
$query3 = $koneksi->query("SELECT * FROM kategori");
    while ($row3 = $query3->fetch_array()) {
      if ($row3['id_kategori'] == $row['id_kategori']) {
        $select = "selected";
      } else {
        $select = "";
      }
      
        echo '
        <option value="' . $row3['id_kategori'] . '" '.$select.'>
            ' . $row3['nama_kategori'] . '
        </option>
        ';
    }
    ?>
    </select>
</div>
<div class="form-group">
    <label for="kategori_sub2">
        Sub Kategori
    </label>
    <select class="form-control" id="kategori_sub2" name="kategori_sub2">
        <option disabled="disabled" selected="selected">
            Pilih Sub Kategori
        </option>
        <?php
$query2 = $koneksi->query("SELECT * FROM kategori_sub WHERE id_kategori = ".$row['id_kategori']);
    while ($row2 = $query2->fetch_array()) {
      if ($row2['id_sub_kategori'] == $row['id_sub_kategori']) {
        $select = "selected";
      } else {
        $select = "";
      }
        echo '
        <option value="' . $row['id_sub_kategori'] . '" '.$select.'>
            ' . $row2['nama_sub_kategori'] . '
        </option>
        ';
    }
    ?>
    </select>
</div>
<div class="form-group">
    <label>
        Deskripsi
    </label>
    <textarea class="form-control" id="edit_deskripsi" name="deskripsi">
        <?php echo $row['deskripsi']; ?>
    </textarea>
</div>
<div class="form-group">
    <img alt="Card image cap" class="card-img-top NO-CACHE" src="/blilistrik/img/177cbf2b2fda8daf8688bd68a5ea6e14/<?php echo $row['foto_barang']; ?>" style="height: 190px; cursor: pointer;">
    </img>
</div>
<div class="form-group ">
    <div class="custom-file">
        <input accept="image/*" class="custom-file-input" id="customFile" name="fotu" type="file">
            <label class="custom-file-label" for="customFileLang">
                Silakan Pilih Foto
            </label>
        </input>
    </div>
</div>
<input id="edit_id" name="id" type="hidden" value="<?php echo $id; ?>">    
</input>