<?php
require_once "../../koneksi/koneksi.php";
$id = $_GET[ 'id' ];
$token = $_SESSION[ 'token' ];
$where = sprintf( "WHERE md5(concat('%s',kode_barang,id_barang)) = '%s'", $_SESSION[ 'token' ], $id );
$query = $koneksi->query( "SELECT * FROM barang INNER JOIN kategori_sub ON barang.id_sub_kategori = kategori_sub.id_sub_kategori $where" );
$row = $query->fetch_assoc();

?>
<div class="form-group">
	<label>Stok Awal</label>
	<input id="stok_awal" type="number" placeholder="Stok Awal" class="form-control" value="<?php echo $row['stok_barang'];?>" name="stoka" readonly>
</div>
<div class="form-group">
	<label>Stok Tambahan</label>
	<input id="stok_tambahan" type="number" placeholder="Stok Tambahan" class="form-control" value="0" name="stokt" min="0">
</div>
<div class="form-group">
	<label>Stok Akhir</label>
	<input id="stok_akhir" type="number" placeholder="Stok Akhir" class="form-control" value="<?php echo $row['stok_barang'];?>" name="stokaa" readonly>
</div>


<input id="edit_id" type="hidden" value="<?php echo $id;?>" style="display:none;" name="id">
<input type="hidden" name="form" value="tambah_stok">

<script type="text/javascript">
	function tambah(){
		if($('#stok_tambahan')!=null){
		$data = parseInt($('#stok_awal').val()) + parseInt($('#stok_tambahan').val())
			$('#stok_akhir').val($data);
		}else{
			$('#stok_akhir').val($('#stok_awal').val());
		}
	};
	$( document ).ready( function () {
		$( '.NO-CACHE' ).attr( 'src', function () {
			return $( this ).attr( 'src' ) + "?upload=" + Math.random()
		} );
		$('#stok_tambahan').change(function(event) {
			tambah();
		});

		$('#stok_tambahan').click(function(event) {
			tambah();
		});

		$('#stok_tambahan').keypress(function(event) {
			tambah();
		});

		$('#stok_tambahan').keydown(function(event) {
			tambah();
		});
		$('#stok_tambahan').keyup(function(event) {
			tambah();
		});
	} );
</script>