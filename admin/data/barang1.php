<?php
if (isset($_POST['form']) && $_POST['form'] != null) {
    switch ($_POST['form']) {
        case 'get_katsub':
            require_once "../../koneksi/koneksi.php";
            $data  = "<option disabled='disabled' selected='selected'>Pilih Sub Kategori</option>";
            $qkota = $koneksi->query("SELECT * FROM kategori_sub WHERE id_kategori = '" . $kategori . "'");
            while ($rkota = $qkota->fetch_array()) {
                $data .= '<option value="' . $rkota['id_sub_kategori'] . '">' . $rkota['nama_sub_kategori'] . '</option>';
            }
            echo json_encode($data);
            break;
        case 'tambah':
            $nama      = $_POST['tambah_nama'];
            $harga     = $_POST['tambah_harga'];
            $stok      = $_POST['tambah_stok'];
            $kategori  = $_POST['tambah_kategori'];
            $deskripsi = $_POST['tambah_deskripsi'];
            $query     = $koneksi->query("INSERT INTO barang (id_barang, nama_barang, id_sub_kategori, deskripsi, harga_barang, stok_barang, foto_barang) VALUES ('', '$nama', '$kategori', '$deskripsi', '$harga', '$stok', '')") or die(get_error());
            $qb        = mysqli_query($koneksi, sprintf("SELECT * FROM barang where nama_barang = '$nama'"));
            $rb        = mysqli_fetch_assoc($qb);
            $foto      = move_uploaded_file($_FILES['foto']['tmp_name'], "../img/177cbf2b2fda8daf8688bd68a5ea6e14/" . $rb['id_barang'] . ".png");
            $fbarang   = "../img/177cbf2b2fda8daf8688bd68a5ea6e14/" . $rb['id_barang'] . ".png";
            $queryy    = $koneksi->query("UPDATE barang SET foto_barang = '$fbarang' WHERE nama_barang = '$nama'");

            if ($query && $foto && $queryy) {
                echo "<script>alert('tambah data berhasil');window.location='admin.php?page=barang';</script> ";
            } else {
                echo "<script>alert('tambah data gagal atau belum upload foto;');window.location='admin.php?page=barang';</script> ";
            }
            break;
    }
} else {
    if (isset($_POST['id'])) {
        $id        = $_POST['id'];
        $nama      = $_POST['nama'];
        $harga     = $_POST['harga'];
        $stok      = $_POST['stok'];
        $deskripsi = $_POST['deskripsi'];
        $kategori  = $_POST['kategori'];
        if (!empty($_FILES['fotu']['name'])) {
            //new image uploaded
            //process your image and data
            $qb      = mysqli_query($koneksi, sprintf("SELECT * FROM barang where nama_barang = '$nama'"));
            $rb      = mysqli_fetch_assoc($qb);
            $foto    = move_uploaded_file($_FILES['fotu']['tmp_name'], "../img/177cbf2b2fda8daf8688bd68a5ea6e14/" . $rb['id_barang'] . ".png");
            $fbarang = "../img/177cbf2b2fda8daf8688bd68a5ea6e14/" . $rb['id_barang'] . ".png";
            $query   = $koneksi->query("UPDATE barang SET stok_barang = '$stok',id_sub_kategori = '$kategori', deskripsi = '$deskripsi', nama_barang = '$nama', harga_barang = '$harga', foto_barang = $fbarang WHERE id_barang = '$id'"); //save to DB with new image name
            if ($foto) {
                echo "<script>alert('update data berhasil');window.location='admin.php?page=barang';</script> ";
            } else {
                echo "<script>alert('update data gagal;');window.location='admin.php?page=barang';</script> ";
            }
        } else {
            // no image uploaded
            // save data, but no change the image column in MYSQL, so it will stay the same value
            $query = $koneksi->query("UPDATE barang SET stok_barang = '$stok',id_sub_kategori = '$kategori', deskripsi = '$deskripsi', nama_barang = '$nama', harga_barang = '$harga' WHERE id_barang = '$id'");
//save to DB but no change image column
            if ($query) {
                echo "<script>alert('update data berhasil');window.location='admin.php?page=barang';</script> ";
            } else {
                echo "<script>alert('update data gagal;');window.location='admin.php?page=barang';</script> ";
            }
        }
    } else if (isset($_GET['del'])) {
        $id      = $_GET['id'];
        $qb      = mysqli_query($koneksi, sprintf("SELECT * FROM barang where id_barang = '$id'"));
        $rb      = mysqli_fetch_assoc($qb);
        $fbarang = "../img/177cbf2b2fda8daf8688bd68a5ea6e14/" . $rb['id_barang'] . ".png";
        if (@!unlink($fbarang)) {
            echo "<script>alert('update foto berhasil');window.location='admin.php?page=barang';</script> ";

        }
        $query = $koneksi->query("DELETE FROM barang WHERE id_barang='$id'");
        if ($query) {
            echo "<script>alert('update data berhasil');window.location='admin.php?page=barang';</script> ";
        } else {
            echo "<script>alert('update data gagal;');window.location='admin.php?page=barang';</script> ";
        }
    }
    ?>
        <!-- Breadcrumb-->
          <div class="breadcrumb-holder container-fluid">
            <ul class="breadcrumb">
              <li class="breadcrumb-item"><a href="index.html">Home</a></li>
              <li class="breadcrumb-item active">Barang</li>
            </ul>
          </div>
          <!-- Forms Section-->
          <section class="forms">
            <div class="container-fluid">
              <div class="row">
                <!-- Form Elements -->
                <div class="col-lg-12">
                  <div class="card">
                    <div class="card-header d-flex align-items-center m-0 p-0">
                      <nav class="navbar" style="min-width: 100%;    z-index: 1;">
                        <div class="container-fluid">
                          <h2 class="no-margin-bottom">DAFTAR BARANG</h2>

                     </nav>
                    </div>
                    <div class="card-body">
                      <div class="input-group">
                        <input class="form-control" type="text" id="q" placeholder="Cari nama barang..." value="<?php echo @$_GET['q'] ?>">
                        <span class="input-group-btn">
                          <a href="javascript:void();" class="btn btn-success" data-toggle="modal" data-target="#modalTambah">+ Tambah</a>
                        </span>
                      </div>
                      <br>
                      <?php
$cari = @$_GET['q'];
    if ($cari != '') {
        echo '<center>Menampilkan pencarian barang <b>' . $cari . '</b>, <a href="admin.php?page=barang">Klik disini</a> untuk menampilkan semua barang.</center> ';
    }
    ?>
                    <div class="table-responsive">
                        <table class="table table-striped table-sm">
                          <thead>
                            <tr>
                              <th>#</th>
                              <th>Nama Barang</th>
                              <th>Harga</th>
                              <th>Stok</th>
                              <th style="text-align: center;">Aksi</th>
                            </tr>
                          </thead>
                          <tbody>
                          <?php
$no    = 0;
    $query = $koneksi->query("SELECT * FROM barang WHERE nama_barang LIKE '%$cari%'");
    while ($row = $query->fetch_array()) {
        $no++;
        ?>
                            <tr>
                              <th scope="row"><?php echo $no; ?></th>
                              <td><?php echo $row['nama_barang']; ?></td>
                              <td><?php echo $row['harga_barang']; ?></td>
                              <td><?php echo $row['stok_barang']; ?></td>
                              <td>
                                <center>
                                  <a href="#" onclick="edit_barang(<?php echo $row['id_barang']; ?>);"><button data-toggle="modal" data-target="#myModal" data-id="<?php echo $row['nama_barang']; ?>" class="btn btn-primary btn-sm"  type="button" ><i class="fa fa-fw fa-edit"></i> Edit</button></a>
                                  <a href="javascript:void();" class="btn btn-danger btn-sm" onclick="hapus_barang(<?php echo $row['id_barang']; ?>);"><i class="fa fa-fw fa-trash"></i> Hapus</a>
                                </center>
                              </td>
                            </tr>
                            <?php
}
    ?>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>


                                <div id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                        <div role="document" class="modal-dialog">
                          <div class="modal-content">
                            <div class="modal-header">
                              <h4 id="exampleModalLabel" class="modal-title">EDIT BARANG </h4>
                              <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
                            </div>
                            <form method="post" action="" enctype="multipart/form-data">
                            <div class="modal-body">
                                <div id="edit_form"></div>
                            </div>
                            <div class="modal-footer">
                              <button type="button" data-dismiss="modal" class="btn btn-secondary">Tutup</button>
                              <button type="submit" class="btn btn-primary" name="update">Simpan</button>
                            </div>
                          </form>

                          </div>
                        </div>
                      </div>
                      <div id="modalTambah" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                        <div role="document" class="modal-dialog">
                          <div class="modal-content">
                            <div class="modal-header">
                              <h4 id="exampleModalLabel" class="modal-title">TAMBAH BARANG</h4>
                              <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
                            </div>
                            <form action="" method="post" enctype="multipart/form-data" >
                            <div class="modal-body">
                                <div class="form-group">
                                  <label>Nama</label>
                                  <input name="tambah_nama" type="text" placeholder="Nama" class="form-control" value="">
                                </div>
                                <div class="form-group">
                                  <label>Harga</label>
                                  <input name="tambah_harga" type="number" placeholder="Harga" class="form-control" value="">
                                </div>
                                <div class="form-group">
                                  <label>Stok</label>
                                  <input name="tambah_stok" type="number" placeholder="Stok Barang..." class="form-control" value="">
                                </div>
                                  <div class="form-group">
                                    <label for="kategori">Kategori</label>
                                    <select class="form-control" id="kategori" name="kategori">
                                        <option disabled="disabled"  selected="selected">Pilih Kategori</option>
                                        <?php
$query = $koneksi->query("SELECT * FROM kategori");
    while ($row = $query->fetch_array()) {
        echo '<option value="' . $row['id_kategori'] . '">' . $row['nama_kategori'] . '</option>';
    }
    ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="kategori_sub">Sub Kategori</label>
                                        <select class="form-control" id="kategori_sub" name="kategori_sub">
                                        <option disabled="disabled"  selected="selected">Pilih Sub Kategori</option>
                                        </select>
                                </div>
                                <div class="form-group">
                                  <label>Deskripsi</label>
                                  <textarea id="tambah_deskripsi" class="form-control" name="tambah_deskripsi" placeholder="Deskripsi barang..."></textarea>
                                </div>
                               <div class="form-group ">
                              <div class="custom-file">
                                <input type="file" class="custom-file-input" id="customFile" accept="image/*" name="foto">
                                <label class="custom-file-label" for="customFileLang">Silakan Pilih Foto</label>
                              </div>
                            </div>
                            </div>
                            <div class="modal-footer">
                              <input type="hidden" name="form" value="tambah">
                              <button type="button" data-dismiss="modal" class="btn btn-secondary">Tutup</button>
                              <button type="submit" class="btn btn-primary" name="tambah">Tambah</button>
                            </div>
                          </form>
                          </div>
                        </div>
                      </div>
    <script src="asset/js/jquery-3.2.1.min.js"></script>
                      <script type="text/javascript">
                        $(document).ready(function() {
                          $('#kategori').change(function(event) {
            var id = $(this,'option:selected').val();
            $.ajax({
              url: '/blilistrik/admin/data/barang.php',
              type: 'POST',
              dataType: 'html',
              data: {'form': 'get_katsub','id': id},
            }).always(function(){
                $('#kategori_sub').html('<option disabled>LOADING...</option>')
            })
            .done(function(data) {
              $('#kategori_sub').html(data);;
            })
            .fail(function() {
             alert("ERROR");
            });
          });
                        });
                      </script><?php }?>