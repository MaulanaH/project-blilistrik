<?php
require_once "../../koneksi/koneksi.php";
$id = $_GET[ 'id' ];
$token = $_SESSION[ 'token' ];
$where = sprintf( "WHERE md5(concat('%s',username,id_user)) = '%s'", $_SESSION[ 'token' ], $id );
$query = $koneksi->query( "SELECT * FROM users $where" );
$row = $query->fetch_assoc();
?>
<div class="form-group row">
    <label class="col-sm-3 form-control-label">Nama</label>
    <div class="col-sm-9">
      <input type="text" placeholder="Nama" class="form-control" value="<?php echo $row['fname'] ?>" name="fname">
    </div>
  </div>
  <div class="form-group row">
    <label class="col-sm-3 form-control-label">Username</label>
    <div class="col-sm-9">
      <input type="text" placeholder="Username" class="form-control" value="<?php echo $row['username'] ?>" name="username" >
    </div>
  </div>
  <!--div class="form-group row">
    <label class="col-sm-3 form-control-label">password</label>
    <div class="col-sm-9">
      <input type="password" placeholder="password" class="form-control" value="" name="password" >
    </div>
  </div-->
  <div class="form-group row">
      <label class="col-sm-3 form-control-label">
          Jenis Kelamin
      </label>
      <div class="btn-group col-sm-9" id="rd_jenkel">
          <label class="btn w-50 btn-outline-secondary form-control" for="pria">
              <input class="rd_jenkel" id="pria" name="jenkel" <?php if($row['jenis_kelamin']=="Pria"){echo 'checked="checked"';}?> required="" type="radio" value="pria">
                  Pria
              </input>
          </label>
          <label class="btn w-50 btn-outline-secondary form-control" for="wanita">
              <input class="rd_jenkel" id="wanita" name="jenkel" <?php if($row['jenis_kelamin']=="Wanita"){echo 'checked="checked"';} ?> required="" type="radio" value="wanita">
                  Wanita
              </input>
          </label>
      </div>
  </div>
  <div class="form-group row">
    <label class="col-sm-3 form-control-label">ttl</label>
    <div class="col-sm-9">
      <input type="text" placeholder="ttl" class="form-control" value="<?php echo $row['tempat_tanggal_lahir'] ?>" name="ttl">
    </div>
  </div>
  <div class="form-group row">
    <label class="col-sm-3 form-control-label">email</label>
    <div class="col-sm-9">
      <input type="text" placeholder="email" class="form-control" value="<?php echo $row['email'] ?>" name="email">
    </div>
  </div>
  <div class="form-group row">
    <label class="col-sm-3 form-control-label">telp</label>
    <div class="col-sm-9">
      <input type="text" placeholder="telp" class="form-control" value="<?php echo $row['telp'] ?>" name="telp">
    </div>
  </div>
  <div class="form-group row">
      <label class="col-sm-3 form-control-label">
          Provinsi
      </label>
      <div class="col-sm-9">
      <select class="form-control" id="provinsi1" name="provinsi">
        <option>Pilih Provinsi</option>
          <?php
          $kota = $row['id_kota_kab'];
          $queryquery_prov = $koneksi->query("SELECT * FROM kota_kab kk inner join provinsi p on kk.id_provinsi = p.id_provinsi WHERE kk.id_kota_kab='$kota'");
          $rowrow_prov = $queryquery_prov->fetch_object();
          $provinsi_id = $rowrow_prov->id_provinsi;
          $kota_id = $rowrow_prov->id_kota_kab;
          $query_prov = $koneksi->query("SELECT * FROM provinsi");
          while ($row_prov = $query_prov->fetch_array()) {
            $selected = ($row_prov['id_provinsi']==$provinsi_id) ? "SELECTED":"";
                  echo '<option value="'.$row_prov['id_provinsi'].'"'.$selected.'>'.$row_prov['nama_provinsi'].'</option>';
          }
          ?>
      </select>
    </div>
  </div>
  <div class="form-group row">
      <label class="col-sm-3 form-control-label">
          Kota/Kabupaten
      </label>
      <div class="col-sm-9">
      <div id="kota_kab_div">
          <select class="form-control" id="kota_kab1" name="kota_kab">
            <option>Pilih Kota/Kabupaten</option>
              <?php
                $query_kota = $koneksi->query("SELECT * FROM kota_kab WHERE id_provinsi=$provinsi_id");
                while ($row_kota = $query_kota->fetch_array()) {
                  $selected = ($row['id_kota_kab']==$kota_id) ? "SELECTED":"";
                        echo '<option value="'.$row_kota['id_kota_kab'].'"'.$selected.'>'.$row_kota['nama_kota_kab'].'</option>';
                }

              ?>
          </select>
        </div>
      </div>
  </div>                
  <div class="form-group row">
    <label class="col-sm-3 form-control-label">alamat</label>
    <div class="col-sm-9">
      <textarea name="alamat" id="alamat" class="form-control w-100" placeholder="alamat" cols="30" rows="2"><?php echo $row['alamat'] ?></textarea>
    </div>
  </div>
  <div class="form-group row">
    <label class="col-sm-3 form-control-label">kode pos</label>
    <div class="col-sm-9">
      <input type="text" placeholder="kode pos" class="form-control" value="<?php echo $row['kode_pos'] ?>" name="kode_pos">
    </div>
  </div>
  <div class="form-group ">
        <div class="custom-file">
          <input type="file" class="custom-file-input" id="customFile" accept="image/*" name="foto">
          <label class="custom-file-label" for="customFileLang">Silakan Pilih Foto</label>
        </div>
      </div> 
</div>
<div class="form-group ">
        <div class="custom-file">
          <input type="file" class="custom-file-input" id="customFile" accept="image/*" name="ktp">
          <label class="custom-file-label" for="customFileLang">Silakan Pilih Ktp</label>
        </div>
      </div> 
</div>
<input type="hidden" name="form" value="update">
<input id="edit_id" type="hidden" value="<?php echo $id?>" style="display:none;" name="id">
<script type="text/javascript">
  $(document).ready(function () {
    $('.NO-CACHE').attr('src', function () { return $(this).attr('src') + "?upload=" + Math.random() });
  }); 
</script>