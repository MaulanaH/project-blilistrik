<?php
require_once "../koneksi/koneksi.php";
$id = $_GET['id'];
$query = $koneksi->query("SELECT * FROM diskon d WHERE d.id_diskon='$id'");
$row = $query->fetch_assoc();
?>
<div class="form-group">
    <label for="id_barang">barang</label>
    <select class="form-control" id="id_barang" name="id_barang">
      <option value="">pilih barang</option>
        <?php
        $query2 = $koneksi->query("SELECT * FROM barang");
            while ($row2 = $query2->fetch_assoc()) {
                $selected = ($row['id_barang']==$row2['id_barang']) ? "SELECTED" : "";
                echo '<option value="' . $row2['id_barang'] . '"'.$selected.'>' . $row2['nama_barang'] . '</option>';
            }
        ?>
    </select>                                    
</div>
<div class="form-group">
    <label for="id_reseller">reseller</label>
    <select class="form-control" id="id_reseller" name="id_reseller">
      <option>pilih reseller</option>
        <?php
        $query2 = $koneksi->query("SELECT * FROM reseller");
            while ($row2 = $query2->fetch_assoc()) {
                $selected = ($row['id_reseller']==$row2['id_reseller']) ? "SELECTED" : "";
                echo '<option value="' . $row2['id_reseller'] . '"'.$selected.'>' . $row2['fname'] . '</option>';
            }
        ?>
    </select>                                    
</div>
<div class="form-group">
  <label>diskon Umum</label>
  <input name="diskon" type="number" min="0" max="100" placeholder="diskon %" class="form-control" value="<?php echo $row['diskon'] ?>">
</div>
<div class="form-group">
  <label>diskon reseller</label>
  <input name="diskonreseller" type="number" min="0" max="100" placeholder="diskon %" class="form-control" value="<?php echo $row['diskonreseller'] ?>">
</div>
<div class="form-group">
  <label>minimum pembelian</label>
  <input name="qty" type="number" placeholder="minimum pembelian" class="form-control" value="<?php echo $row['qty'] ?>">
</div>                              
<div class="form-group">
    <label for="status">status</label>
    <select class="form-control" id="status" name="status">
      <option value="">pilih status</option>
      <option value="1" <?php echo ($row['status']=="aktif") ? "SELECTED" : ""; ?>>Aktif</option>
      <option value="2" <?php echo ($row['status']=="nonaktif") ? "SELECTED" : ""; ?>>Nonaktif</option>

    </select>                                    
</div>
   <div class="form-group ">
</div>                                
    <input id="edit_id" type="text" value="<?php echo $id;?>" style="display:none;" name="id">
<script type="text/javascript">
    $(document).ready(function ()
    {           
    $('.NO-CACHE').attr('src',function () { return $(this).attr('src') + "?upload=" + Math.random() });
    });
    $(document).ready(function(){
    $('#kategorie').on('change',function(){
    var barangID = $(this).val();
    if(barangID){
    $.ajax({
    type:'POST',
    url:'data/aedit_barang.php',
    data:'id_kategori='+barangID,
    success:function(html){
    $('#subkategorie').html(html);
    }
    });
    }else{
    $('#subkategorie').html('<option value="">Kosong</option>');
    }
    });
    });     
</script>