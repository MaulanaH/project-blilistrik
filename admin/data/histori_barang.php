<!-- Breadcrumb>
	<div class="breadcrumb-holder container-fluid">
		<ul class="breadcrumb">
			<li class="breadcrumb-item"><a href="admin.php">Home</a>
			</li>
			<li class="breadcrumb-item active">Barang</li>
		</ul>
	</div>
<!-- Forms Section-->
<section class="forms p-0">
	<div class="container-fluid m-0 p-0">
		<div class="row">
			<div class="col-lg-12">
				<div class="card">
					<div class="card-header d-flex align-items-center m-0 p-0">
						<nav class="navbar bg-white text-dark" style="min-width: 100%;z-index: 1;">
							<div class="container-fluid">
								<h2 class="no-margin-bottom">Historical Barang</h2>
							</div>
						</nav>
					</div>
					<div class="card-body">
					<?php
						$cari = @$_GET[ 'cari' ];
						$id = @$_GET[ 'id' ];
						$jenis = @$_GET[ 'jenis' ];
						if ( $cari != '' ) {
							echo '<center>Menampilkan pencarian barang <b>' . $cari . '</b>, <a href="admin.php?page=barang">Klik disini</a> untuk menampilkan semua barang.</center> ';
						}else if($id!=''){
							echo '<center>Menampilkan kode barang <b>' . $id . '</b>, <a href="admin.php?page=barang">Klik disini</a> untuk menampilkan semua barang.</center> ';
            }
            if($jenis=='masuk'){
              $query = "WHERE jenis = 'pemasukan'";
            }else if($jenis=='keluar'){
              $query = "WHERE jenis = 'pengeluaran'";
            }else{
              $query = "WHERE 1";
            }
						if ( isset( $cari ) && $cari != null ) {
							$query .= " AND nama_barang LIKE '%$cari%'";
						} else if ( isset( $id ) && $id != null ) {
							$query .= " AND b.kode_barang = '" . $id . "'";
						}
						?>
						<div class="table-responsive">
							<table class="table table-striped table-bordered" id="example">
                <thead align="center">
									<tr>
										<th>#</th>
                    <th>Nama Barang</th>
                    <!--th>Stok</th-->
                    <th>Jenis</th>
                    <th>Keterangan</th>
                    <th>Tanggal</th>
                  </tr>
                </thead>
                <tbody>
            <?php
            $cari = @$_GET['q'];
            if ($cari != '') {
              echo '<center>Menampilkan pencarian barang <b>'.$cari.'</b>, <a href="admin.php?page=barang&halaman=1&q=">Klik disini</a> untuk menampilkan semua barang.</center> ';
            }
            $perpage = 25; //per halaman
            $page = isset($_GET["hal"]) ? (int)$_GET["hal"] : 1;         
            $start = ($page > 1) ? ($page * $perpage) - $perpage :0;
            $articles = sprintf("SELECT * FROM histori_mbarang hb left join barang b on hb.id_barang = b.id_barang " . $query . " order by tanggal desc");
            $limit = $articles." LIMIT $start, $perpage";
            $tampil = mysqli_query($koneksi, $limit);
            $result=mysqli_query($koneksi,$articles);
            $total = mysqli_num_rows($result);
            $pages = ceil($total/$perpage);
            while ($row = $tampil->fetch_assoc()) {
              $start++;
              ?>
              <tr>
                <td>
                  <?php echo $start;?>
                  </td>
                    <td><a href="?page=barang&&id=<?php echo $row['kode_barang']?>"><?php echo $row['nama_barang'];?></a></td>
                    <!--td><?php echo $row['stok_barang'];?></td-->
                      <td><?php echo $row['jenis'];?></td>
                    <td><?php echo $row['keterangan'];?></td>
                    <td><?php echo $row['tanggal'];?></td>
                  </tr>
                  <?php } ?>
                  </tbody>
              </table>
            </div>
        </div>
      </div>
    </div>
  </div>
</section>


                      <div id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
              <div role="document" class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <h4 id="exampleModalLabel" class="modal-title">EDIT BARANG </h4>
                    <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
                  </div>
                  <form method="post" action="" enctype="multipart/form-data">
                  <div class="modal-body">
                      <div id="edit_form"></div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" data-dismiss="modal" class="btn btn-secondary">Tutup</button>
                    <button type="submit" class="btn btn-primary" name="update">Simpan</button>
                  </div>
                </form>
                
                </div>
              </div>
            </div>


                      <div id="modalTambah" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
              <div role="document" class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <h4 id="exampleModalLabel" class="modal-title">TAMBAH BARANG</h4>
                    <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
                  </div>
                  <form action="" method="post" enctype="multipart/form-data" > 
                  <div class="modal-body">                   
                      <div class="form-group">
                        <label>Nama</label>
                        <input name="tambah_nama" type="text" placeholder="Nama" class="form-control" value="">
                      </div>
                      <div class="form-group">
                        <label>Harga</label>
                        <input name="tambah_harga" type="number" placeholder="Harga" class="form-control" value="">
                      </div>                                                               
                      <div class="form-group">       
                        <label>Stok</label>
                        <input name="tambah_stok" type="number" placeholder="Stok Barang..." class="form-control" value="">
                      </div>  
                      
                      <div class="form-group">
                          <label for="exampleSelect1">Kategori</label>
                          <select class="form-control" id="barang" name="tambah_kategori">
                            <option value="">pilih kategori</option>
                              <?php
                              $query = $koneksi->query("SELECT * FROM kategori");
                                  while ($row = $query->fetch_array()) {
                                      echo '<option value="' . $row['id_kategori'] . '">' . $row['nama_kategori'] . '</option>';
                                  }
                              ?>
                          </select>                                    
                      </div>
                      
                      <div class="form-group">
                          <label for="exampleSelect1">Sub Kategori</label>
                          <select class="form-control" id="subkategori" name="subkategori">
                            <option value="">pilih kategori dulu</option>
                              </select>
                      </div>
                      <div class="form-group">
                          <label for="exampleSelect1">Brand</label>
                          <select class="form-control" id="brand" name="brand">
                            <option value="">pilih brand</option>
                              <?php
                              $query = $koneksi->query("SELECT * FROM brand");
                                  while ($row = $query->fetch_array()) {
                                      echo '<option value="' . $row['id_brand'] . '">' . $row['nama_brand'] . '</option>';
                                  }
                              ?>
                          </select>                                    
                      </div>                                                                                                    
                      <div class="form-group">       
                        <label>Deskripsi</label>
                        <textarea id="tambah_deskripsi" class="form-control" name="tambah_deskripsi" placeholder="Deskripsi barang..."></textarea>
                      </div>
                      <div class="form-group ">
                    <div class="custom-file">
                      <input type="file" class="custom-file-input" id="customFile" accept="image/*" name="foto">
                      <label class="custom-file-label" for="customFileLang">Silakan Pilih Foto</label>
                    </div>
                  </div>                                                               
                  </div>
                  <div class="modal-footer">
                    <input type="hidden" name="form" value="tambah">
                    <button type="button" data-dismiss="modal" class="btn btn-secondary">Tutup</button>
                    <button type="submit" class="btn btn-primary" name="tambah">Tambah</button>
                  </div>
                </form>                     
                </div>
              </div>
            </div>