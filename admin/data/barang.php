<?php

if ( isset( $_GET[ 'del' ] ) ) {
	$_POST[ 'form' ] = "delete";
}
if ( isset( $_POST[ 'form' ] ) && $_POST[ 'form' ] != NULL ) {
	$query_admin = $koneksi->query( "SELECT * FROM admin where id_admin = " . $_SESSION[ 'id_admin' ] );
	$radmin = $query_admin->fetch_assoc();
	$token = $_SESSION[ 'token' ];
	if ( ( isset( $_GET[ 'id' ] ) && $_GET[ 'id' ] != null ) || ( isset( $_POST[ 'id' ] ) && $_POST[ 'id' ] != null ) ) {
		if ( isset( $_GET[ 'id' ] ) ) {
			$id = $_GET[ 'id' ];
		} else {
			$id = $_POST[ 'id' ];
		}
		$where_barang = sprintf( "WHERE md5(concat('%s',kode_barang,id_barang)) = '%s'", $_SESSION[ 'token' ], $id );
		$query_barang = $koneksi->query( "SELECT * FROM barang $where_barang" );
		$row_barang = $query_barang->fetch_assoc();
	}

	switch ( $_POST[ 'form' ] ) {
		case 'tambah':
			$kode_barang = $_POST[ 'kode_barang' ];
			$nama = $_POST[ 'tambah_nama' ];
			$harga = $_POST[ 'tambah_harga' ];
			$satuan = $_POST[ 'tambah_satuan_stok' ];
			$berat = $_POST[ 'tambah_berat' ];
			$satuan_berat = $_POST[ 'tambah_satuan_berat' ];
			$stok = $_POST[ 'tambah_stok' ];
			$subkategori = $_POST[ 'tambah_subkategori' ];
			$deskripsi = $_POST[ 'tambah_deskripsi' ];
			$brand = $_POST[ 'brand' ];
			$query = $koneksi->query( "INSERT INTO `barang`( `kode_barang`, `id_sub_kategori`, `id_brand`, `nama_barang`, `deskripsi`, `harga_barang`, `stok_barang`, `satuan_stok`, `berat_barang`, `satuan_berat`, `foto_barang`) VALUES ('$kode_barang','$subkategori', '$brand', '$nama', '$deskripsi', '$harga', '$stok', '$satuan', $berat, '$satuan_berat', 'foto' )" )or die( mysqli_error( $koneksi ) );
			$id_barang = $koneksi->insert_id;
			$foto = move_uploaded_file( $_FILES[ 'foto' ][ 'tmp_name' ], "../img/177cbf2b2fda8daf8688bd68a5ea6e14/" . $id_barang . ".png" );
			$fbarang = "../img/177cbf2b2fda8daf8688bd68a5ea6e14/" . $id_barang . ".png";
			$queryy = $koneksi->query( "UPDATE barang SET foto_barang = '$fbarang' WHERE id_barang = '$id_barang'" );
			if ( $query && $foto && $queryy ) {
				histori( $id_barang, "insert", "barang" );
				echo "<script>alert('tambah data berhasil');window.location='admin.php?page=barang&halaman=1&q=';</script> ";
			} else {
				echo "<script>alert('tambah data gagal atau belum upload foto;');window.location='admin.php?page=barang&halaman=1&q=';</script> ";
			}
			break;
		case 'update':
			$nama = $_POST[ 'namae' ];
			$harga = $_POST[ 'hargae' ];
			$stok = $_POST[ 'stoke' ];
			$deskripsi = $_POST[ 'deskripsie' ];
			$subkategori = $_POST[ 'subkategorie' ];
			$brand = $_POST[ 'brande' ];
			if ( $stok < $row_barang[ 'stok_barang' ] ) {
				$ket = "Admin " . $radmin[ 'username' ] . " 	Telah mengurangi stok barang sejumlah " . ( $row_barang[ 'stok_barang' ] - $stok );
				$koneksi->query( sprintf( "INSERT INTO `histori_mbarang`(`id_barang`, `jenis`, `keterangan`) VALUES ('%s','pengeluaran','%s')", $row_barang[ 'id_barang' ], $ket ) )OR die( $koneksi->error );
			} else {
				$ket = "Admin " . $radmin[ 'username' ] . " Telah menambah stok barang sejumlah " . ( $stok - $row_barang[ 'stok_barang' ] );
				$koneksi->query( sprintf( "INSERT INTO `histori_mbarang`(`id_barang`, `jenis`, `keterangan`) VALUES ('%s','pemasukan','%s')", $row_barang[ 'id_barang' ], $ket ) )OR die( $koneksi->error );
			}

			if ( !empty( $_FILES[ 'fotu' ][ 'name' ] ) ) {
				//new image uploaded
				//process your image and data
				$qb = mysqli_query( $koneksi, sprintf( "SELECT * FROM barang where nama_barang = '$nama'" ) );
				$rb = mysqli_fetch_assoc( $qb );
				$foto = move_uploaded_file( $_FILES[ 'fotu' ][ 'tmp_name' ], "../img/177cbf2b2fda8daf8688bd68a5ea6e14/" . $rb[ 'id_barang' ] . ".png" );
				$fbarang = "../img/177cbf2b2fda8daf8688bd68a5ea6e14/" . $rb[ 'id_barang' ] . ".png";
				$query = $koneksi->query( "UPDATE barang SET foto_barang = '$fbarang' WHERE id_barang = '" . $row_barang[ 'id_barang' ] . "'" )or die( mysqli_error( $koneksi ) ); //save to DB with new image name
			}
			// no image uploaded
			// save data, but no change the image column in MYSQL, so it will stay the same value
			$query = $koneksi->query( "UPDATE barang SET id_sub_kategori = '$subkategori',id_brand = '$brand',nama_barang = '$nama',deskripsi = '$deskripsi',harga_barang = '$harga',stok_barang = '$stok' WHERE id_barang = '" . $row_barang[ 'id_barang' ] . "'" )or die( mysqli_error( $koneksi ) );
			//save to DB but no change image column
			if ( $query ) {
				histori( $row_barang[ 'id_barang' ], "update", "barang" );
				echo "<script>alert('update data berhasil');window.location='admin.php?page=barang&halaman=1&q=';</script> ";
			} else {
				echo "<script>alert('update data gagal;');window.location='admin.php?page=barang&halaman=1&q=';</script> ";
			}

			break;
		case 'delete':
			$fbarang = sprintf( "%simg/177cbf2b2fda8daf8688bd68a5ea6e14/%s.png", $root_base, $row_barang[ 'id_barang' ] );
			$tanggal = date('Y-m-d H:i:s',strtotime('NOW'));
			//$queries  = "UPDATE barang SET `status` = 'nonaktif',`deleted_at` = '$tanggal' where id_barang = '".$row_barang['id_barang']."'";
			$queries  = "DELETE FROM barang where id_barang = '".$row_barang['id_barang']."'";
			$query = $koneksi->query($queries);
			if ( $query ) {
				histori( $row_barang[ 'id_barang' ], "delete", "barang" );
				//echo "<script>alert('hapus data berhasil');window.location='admin.php?page=barang&halaman=1&q=';</script> ";
				if ( @!unlink( $fbarang ) ) {
					echo "<script>alert('hapus data berhasil');window.location='admin.php?page=barang&halaman=1&q=';</script> ";
				}
			} else {
				echo "<script>alert('hapus data gagal;');window.location='admin.php?page=barang&halaman=1&q=';</script> ";
			}
			break;
		case 'tambah_stok':
			$stok = ($_POST[ 'stokt' ])+($row_barang[ 'stok_barang' ]);
			$query = $koneksi->query( "UPDATE barang SET stok_barang = '$stok' WHERE id_barang = '" . $row_barang[ 'id_barang' ] . "'" )or die( $koneksi -> mysqli_error() );
			if ( $query ) {
				if ( $stok < $row_barang[ 'stok_barang' ] ) {
					$ket = "Admin " . $radmin[ 'username' ] . " 	Telah mengurangi stok barang sejumlah " . ( $row_barang[ 'stok_barang' ] - $stok );
					$koneksi->query( sprintf( "INSERT INTO `histori_mbarang`(`id_barang`, `jenis`, `keterangan`) VALUES ('%s','pengeluaran','%s')", $row_barang[ 'id_barang' ], $ket ) )OR die( $koneksi->error );
				} else {
					$ket = "Admin " . $radmin[ 'username' ] . " Telah menambah stok barang sejumlah " . ( $stok - $row_barang[ 'stok_barang' ] );
					$koneksi->query( sprintf( "INSERT INTO `histori_mbarang`(`id_barang`, `jenis`, `keterangan`) VALUES ('%s','pemasukan','%s')", $row_barang[ 'id_barang' ], $ket ) )OR die( $koneksi->error );
				}
				histori( $row_barang[ 'id_barang' ], "update", "barang" );
				echo "<script>alert('update stok berhasil');window.location='admin.php?page=barang&halaman=1&q=';</script> ";
			} else {
				echo "<script>alert('update stok gagal;');window.location='admin.php?page=barang&halaman=1&q=';</script> ";
			}
			break;
		case 'update_fotob':
				//$rb = mysqli_fetch_assoc( $qb );
				$foto = move_uploaded_file( $_FILES[ 'fotou' ][ 'tmp_name' ], "../img/177cbf2b2fda8daf8688bd68a5ea6e14/" . $row_barang['id_barang'] . ".png" );
				$fbarang = "../img/177cbf2b2fda8daf8688bd68a5ea6e14/" . $row_barang['id_barang'] . ".png";
				$query = $koneksi->query( "UPDATE barang SET foto_barang = '$fbarang' WHERE id_barang = '" . $row_barang['id_barang'] . "'" )or die( mysqli_error( $koneksi ) ); //save to DB with new image name

		if ( $query ) {
			histori( $row_barang[ 'id_barang' ], "update", "barang" );
			echo "<script>alert('update data berhasil');window.location='admin.php?page=barang';</script> ";
		} else {
			echo "<script>alert('update data gagal;');window.location='admin.php?page=barang';</script> ";
		}
			break;
		default:
			# code...
			break;
	}
}
$_SESSION[ 'token' ] = $token;
?>


<!-- Breadcrumb>
	<div class="breadcrumb-holder container-fluid">
		<ul class="breadcrumb">
			<li class="breadcrumb-item"><a href="admin.php">Home</a>
			</li>
			<li class="breadcrumb-item active">Barang</li>
		</ul>
	</div>
<!-- Forms Section-->
<section class="forms p-0">
	<div class="container-fluid m-0 p-0">
		<div class="row">
			<!-- Form Elements -->
			<div class="col-lg-12">
				<div class="card">
					<div class="card-header d-flex align-items-center m-0 p-0">
						<nav class="navbar bg-white text-dark" style="min-width: 100%;z-index: 1;">
							<div class="container-fluid">
								<h2 class="no-margin-bottom">Daftar Barang</h2>
								<!-- Search Box-->
								<div class="search-box">
									<button class="dismiss"><i class="icon-close"></i></button>
									<input class="form-control cari h-100" table="barang" type="text" id="abarang" placeholder="Cari nama barang..." value="<?php echo @$_GET['qa']?>">
								</div>
								<ul class="nav-menu list-unstyled d-flex flex-md-row align-items-md-center mr-4">
									<!-- Search-->
									<li class="nav-item d-flex align-items-center"><a id="search" href="#"><i class="icon-search"></i></a>
									</li>
								</ul>
								<div class="card-close ml-2 mr-2">
									<div class="dropdown">
										<button type="button" id="closeCard4" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle"><i class="fa fa-ellipsis-v"></i></button>
										<div aria-labelledby="closeCard4" class="dropdown-menu dropdown-menu-right has-shadow">
											<a href="javascipt:void()" class="dropdown-item add" data-toggle="modal" data-target="#modalTambah"><i class="fa fa-plus">
												</i>Tambah Barang</a>

											<a href="data/barang_export.php" class="dropdown-item export"> <i class="fa fa-file-download">
												</i>Export to Excell</a>

											<a href="javascipt:void()" class="dropdown-item import" data-toggle="modal" data-target="#mdlimport">
												<i class="fa fa-file-upload">
												</i>Import to Excell</a>

											<!--a href="javascipt:void()" class="dropdown-item search"> <i class="fa fa-search">
												</i>Pencarian</a-->
											<!--a href="#" class="dropdown-item close"> <i class="fa fa-times"></i>close</a-->
										</div>
									</div>
								</div>
							</div>
						</nav>
					</div>
					<div class="card-body">
					<?php
						$cari = @$_GET[ 'cari' ];
						$id = @$_GET[ 'id' ];
						if ( $cari != '' ) {
							echo '<center>Menampilkan pencarian barang <b>' . $cari . '</b>, <a href="admin.php?page=barang">Klik disini</a> untuk menampilkan semua barang.</center> ';
						}else if($id!=''){
							echo '<center>Menampilkan kode barang <b>' . $id . '</b>, <a href="admin.php?page=barang">Klik disini</a> untuk menampilkan semua barang.</center> ';
						}
						$query = "WHERE STATUS = 'aktif'";
						if ( isset( $cari ) && $cari != null ) {
							$query .= " AND nama_barang LIKE '%$cari%'";
						} else if ( isset( $id ) && $id != null ) {
							$query .= " AND b.kode_barang = '" . $id . "'";
						}
						?>
						<div class="table-responsive">
							<table class="table table-striped table-bordered" id="example">
								<thead align="center">
									<tr>
										<th rowspan="2">#</th>
										<th rowspan="2">kode barang</th>
										<th rowspan="2">Nama barang</th>
										<th rowspan="2">Kategori</th>
										<th rowspan="2">Sub kategori</th>
										<th rowspan="2">Brand</th>
										<th rowspan="2">Berat</th>
										<th rowspan="2">Stok</th>
										<th rowspan="2">Harga</th>
										<th colspan="2" style="border-bottom:none;">Diskon reguler</th>
										<th colspan="3" style="border-bottom:none;">Diskon reseller</th>
										<th rowspan="2" width="75px">Foto</th>
										<th rowspan="2" width="100px">action</th>
									</tr>
									<tr>
										<th width="30px">Minimal Pembelian</th>
										<th width="30px">Diskon</th>
										<th width="30px">Minimal Pembelian</th>
										<th width="30px">Diskon</th>
										<th width="30px" style="solid #dee2e6">komisi</th>
									</tr>
								</thead>
								<tbody>
									<?php
									$articles = "SELECT *,b.id_barang AS id FROM barang_detail b " . $query;
									//$articles = "SELECT *,b.id_barang AS id FROM barang b " . $query;
									$result = mysqli_query( $koneksi, $articles );
									$total = mysqli_num_rows( $result );
									if ( $total == 0 ) {
										echo "<tr><td colspan='16' align='center'><h2>Tidak ada barang.<h2></tr></td>";
									}
									while ( $row = $result->fetch_assoc() ) {
										$start++;
										$id = md5( $_SESSION[ 'token' ] . $row[ 'kode_barang' ] . $row[ 'id' ] );
										?>
									                  <tr>										<th scope="row">
											<?php echo $start;?>
										</th>
										<td>
											<?php echo $row['kode_barang'];?>
										</td>
										<td>
											<?php echo $row['nama_barang'];?>
										</td>
										<td>
											<?php echo $row['nama_kategori'];?>
										</td>
										<td>
											<?php echo $row['nama_sub_kategori'];?>
										</td>
										<td>
											<?php echo $row['nama_brand'];?>
										</td>
										<td>
											<?php echo $row['berat_barang']." ".$row['satuan_berat'];?>
										</td>
										<td align="right">
											<?php echo $row['stok_barang']." ".$row['satuan_stok'];?>
										</td>
										<td align="right">Rp.
											<?php echo number_format($row['harga_barang'],0,',','.');?>
										</td>
										<td align="center" class='p-0'>
                                            <?php
											$darticles = "SELECT * from diskon d left join diskon_reseller dr on d.id_diskon = dr.id_diskon where id_barang = ".$row['id_barang'].' AND id_reseller IS NULL';
                                                $dresult=mysqli_query($koneksi,$darticles);
                                                if($dresult->num_rows==0){
                                                    echo "-";
                                                }else{
                                            ?>
										    <table align='center' class='p-0 h-100' style="background-color: transparent;">
											<?php 
												while($rowdis = $dresult->fetch_assoc()) {
													$no++;
													?>
												<tr class="h-100" style="background-color: transparent;">
													<td align="right" width="90px" style="border-bottom: 1px solid #dee2e6;"><?php echo $rowdis['qty']." ".$row['satuan_stok'];?></td>
												</tr>							
											<?php }?></table><?php }?>
										</td>
										<td align="center" class='p-0'>
                                            <?php
                                                $dresult=mysqli_query($koneksi,$darticles);
                                                if($dresult->num_rows==0){
                                                    echo "-";
                                                }else{
                                            ?>
										    <table align='center' class='table p-0 h-100' style="background-color: transparent;">
											<?php 
												while($rowdis = $dresult->fetch_assoc()) {
													$no++;
													?>
												<tr class="h-100" style="background-color: transparent;">
													<td align="right" width="90px" style="border-bottom: 1px solid #dee2e6;"><?php echo $rowdis['diskon'];?>%</td>
													<!--td align="right" width="90px"><?php echo $rowdis['qty']." ".$row['satuan_stok'];?></td-->
												</tr>							
											<?php }?></table><?php }?>
										</td>

										<td align="center" class='p-0'>
                                            <?php $darticles = "SELECT * from diskon d left join diskon_reseller dr on d.id_diskon = dr.id_diskon where id_barang = ".$row['id_barang'].' AND id_reseller IS NOT NULL';
                                                
                                                $dresult=mysqli_query($koneksi,$darticles);
                                                if($dresult->num_rows==0){
                                                    echo "-";
                                                }else{
                                            ?>
										    <table align='center' class='p-0 h-100' style="background-color: transparent;">
											<?php 
												while($rowdis = $dresult->fetch_assoc()) {
													$no++;
													?>
												<tr class="h-100" style="background-color: transparent;">
													<td align="right" width="90px" style="border-bottom: 1px solid #dee2e6;"><?php echo $rowdis['qty']." ".$row['satuan_stok'];?></td>
												</tr>							
											<?php }?></table><?php }?>
										</td>
										<td align="center" class='p-0'>
                                            <?php
                                               $dresult=mysqli_query($koneksi,$darticles);
                                                if($dresult->num_rows==0){
                                                    echo "-";
                                                }else{
                                            ?>
										    <table align='center' class='table p-0 h-100' style="background-color: transparent;">
											<?php 
												while($rowdis = $dresult->fetch_assoc()) {
													$no++;
													?>
												<tr class="h-100" style="background-color: transparent;">
													<td align="right" width="90px" style="border-bottom: 1px solid #dee2e6;"><?php echo $rowdis['diskon'];?>%</td>
													<!--td align="right" width="90px"><?php echo $rowdis['qty']." ".$row['satuan_stok'];?></td-->
												</tr>							
											<?php }?></table><?php }?>
										</td>
										<td align="center" class='p-0'>
                                            <?php
                                                $dresult=mysqli_query($koneksi,$darticles);
                                                if($dresult->num_rows==0){
                                                    echo "-";
                                                }else{
                                            ?>
										    <table align='center' class='p-0 h-100' style="background-color: transparent;">
											<?php 
												while($rowdis = $dresult->fetch_assoc()) {
													$no++;
													?>
												<tr class="h-100" style="background-color: transparent;">
													<td align="right" width="90px" style="border-bottom: 1px solid #dee2e6;"><?php echo $rowdis['komisi_reseller']." "?>%</td>
												</tr>							
											<?php }?></table><?php }?>
										</td>
										<td width='100px' class="p-1">
											<a href='javascript:void("a")' onclick="lihat_foto('<?php echo $id;?>')">
																	<img src="<?php echo $row['foto_barang'] ?>" alt="Foto Barang" class="img rounded NO-CACHE" style='width: 100px;height:100px;'>
																</a>
										</td>
										<td align="center" >
											<div class="btn-group-vertical">
												<a  href="javascript:void('a')" data-id="<?php echo $row['nama_barang']; ?>" class="btn btn-sm text-info" onclick="stok_barang('<?php echo $id;?>');">
												<i class="fa fa-fw fa-plus"></i>Tambah Stok</a>
												<a href="javascript:void()" data-id="<?php echo $row['nama_barang']; ?>" class="btn btn-sm" onclick="edit_barang('<?php echo $id;?>');">
												<i class="fa fa-fw fa-edit"></i>Edit</a>
												<a href="javascript:void()" data-id="<?php echo $row['nama_barang']; ?>" class="btn btn-sm text-danger" onclick="hapus_barang('<?php echo $id;?>');">
												<i class="fa fa-fw fa-trash"></i>Hapus</a>
											</div>
										</td>
									</tr>
									<?php
									}
									?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<div id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
	<div role="document" class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 id="exampleModalLabel" class="modal-title">EDIT BARANG </h4>
				<button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
			</div>
			<form method="post" action="" enctype="multipart/form-data">
				<div class="modal-body">
					<div id="edit_form"></div>
				</div>
				<div class="modal-footer">
					<button type="button" data-dismiss="modal" class="btn btn-secondary">Tutup</button>
					<button type="submit" class="btn btn-primary" name="update">Simpan</button>
				</div>
			</form>

		</div>
	</div>
</div>

<div id="mdl_stok" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
	<div role="document" class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 id="exampleModalLabel" class="modal-title">Stok Barang</h4>
				<button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
			</div>
			<form method="post" action="" enctype="multipart/form-data">
				<div class="modal-body">
					<div id="stok_form"></div>
				</div>
				<div class="modal-footer">
					<button type="button" data-dismiss="modal" class="btn btn-secondary">Tutup</button>
					<button type="submit" class="btn btn-primary" name="update">Simpan</button>
				</div>
			</form>

		</div>
	</div>
</div>

<div id="mdlimport" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
	<div role="document" class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 id="exampleModalLabel" class="modal-title">Import Excell BARANG </h4>
				<button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
			</div>
			<form name="myForm" id="myForm" onSubmit="return validateForm()" action="data/barang_import.php" method="post" enctype="multipart/form-data">
				<div class="modal-body">
					<input type="file" id="file" name="file"/>
				</div>
				<div class="modal-footer">
					<label class="col-sm-3 form-control-label"><input type="checkbox" name="update" id="importupdate" value="1" /> <u>Ubah data yang telah ada.</u> </label>
					<label class="col-sm-3 form-control-label"><input type="checkbox" name="drop" id="importdrop" value="1" /> <u>Kosongkan tabel sql terlebih dahulu.</u> </label>
					<input type="hidden" name="form" value="submit">
					<button type="button" data-dismiss="modal" class="btn btn-secondary">Tutup</button>
					<button type="submit" class="btn btn-primary" >Simpan</button>
				</div>
			</form>
		</div>
	</div>
</div>

<div id="modalTambah" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
	<div role="document" class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 id="exampleModalLabel" class="modal-title">TAMBAH BARANG</h4>
				<button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
			</div>
			<form action="" method="post" enctype="multipart/form-data">
				<div class="modal-body">
					<div class="form-group row">
						<label class="col-sm-3 form-control-label">kode barang</label><div class="col-sm-9">
						<input name="kode_barang" type="text" placeholder="kode_barang" class="form-control" id="kdbrg" value=""></div>
					</div>
					<div class="form-group row">
						<label class="col-sm-3 form-control-label">Nama</label><div class="col-sm-9">
						<input name="tambah_nama" type="text" placeholder="Nama" class="form-control" value=""></div>
					</div>
					<div class="form-group row">
						<label class="col-sm-3 form-control-label">Harga</label><div class="col-sm-9">
						<input name="tambah_harga" type="number" placeholder="Harga" class="form-control" value=""></div>
					</div>
					<div class="form-group row">
						<label class="col-sm-3 form-control-label">Berat</label>
						<div class="col-sm-6">
							<input name="tambah_berat" type="number" placeholder="Berat Barang..." class="form-control w-100" value="">
						</div>
						<div class="col-3">
							<input name="tambah_satuan_berat" type="text" placeholder="Satuan Barang..." class="form-control w-100" value="Kg">
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-3 form-control-label">Stok</label>
						<div class="col-sm-6">
							<input name="tambah_stok" type="number" placeholder="Stok Barang..." class="form-control" value="0">
							</div>
						<div class="col-3">
							<input name="tambah_satuan_stok" type="text" placeholder="Satuan Barang..." class="form-control" value="pcs">
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-3 form-control-label" for="exampleSelect1">Kategori</label><div class="col-sm-9">
						<select class="form-control" id="kategori" name="tambah_kategori">
							<option value="">pilih kategori</option>
							<?php
							$query = $koneksi->query( "SELECT * FROM kategori" );
							while ( $row = $query->fetch_array() ) {
								echo '<option value="' . $row[ 'id_kategori' ] . '">' . $row[ 'nama_kategori' ] . '</option>';
							}
							?>
						</select></div>
					</div>
					<div class="form-group row">
						<label class="col-sm-3 form-control-label" for="exampleSelect1">Sub Kategori</label><div class="col-sm-9">
						<select class="form-control" id="subkategori" name="tambah_subkategori">
							<option value="">pilih kategori dulu</option>
						</select></div>
					</div>
					<div class="form-group row">
						<label class="col-sm-3 form-control-label" for="exampleSelect1">Brand</label><div class="col-sm-9">
						<select class="form-control" id="brand" name="brand">
							<option value="">pilih brand</option>
							<?php
							$query = $koneksi->query( "SELECT * FROM brand" );
							while ( $row = $query->fetch_array() ) {
								echo '<option value="' . $row[ 'id_brand' ] . '">' . $row[ 'nama_brand' ] . '</option>';
							}
							?>
						</select></div>
					</div>
					<div class="form-group row">
						<label class="col-sm-3 form-control-label">Deskripsi</label><div class="col-sm-9">
						<textarea id="tambah_deskripsi" class="form-control" name="tambah_deskripsi" placeholder="Deskripsi barang..."></textarea></div>
					</div>
					<div class="form-group">
						<div class="custom-file">
							<input type="file" class="custom-file-input" id="customFile" accept="image/*" name="foto">
							<label class="custom-file-label" for="customFileLang">Silakan Pilih Foto</label>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<input type="hidden" name="form" value="tambah">
					<button type="button" data-dismiss="modal" class="btn btn-secondary">Tutup</button>
					<button type="submit" class="btn btn-primary" name="tambah">Tambah</button>
				</div>
			</form>
		</div>
	</div>
</div>

<div id="mdlfoto" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
	<div role="document" class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 id="exampleModalLabel" class="modal-title">Foto barang </h4>
				<button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
			</div>
			<form method="post" action="" enctype="multipart/form-data">
				<div class="modal-body">
					<div id="foto_form">
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" data-dismiss="modal" class="btn btn-secondary">Tutup</button>
					<button type="submit" class="btn btn-primary" name="update">Simpan</button>
				</div>
			</form>

		</div>
	</div>
</div>

<script src="<?php echo $root_base; ?>assets/vendor/jquery/jquery.min.js"></script>
<script>
	function stok_barang( id ) {
		$( '#mdl_stok' ).modal( 'show' );
		$( '#stok_form' ).html( "<center>Tunggu Sebentar...</center>" );
		$( '#stok_form' ).load( 'data/barang_stok.php?id=' + id );
		$( '#mdl_stok' ).show();
		$( '#mdl_stok' ).on( 'show.bs.modal', function ( event ) {
			var button = $( event.relatedTarget )
			var recipient = button.data( 'id' )
			var modal = $( this )
			modal.find( '.modal-title' ).text( 'Stok Barang ' + recipient )
				//modal.find('.modal-body input').val(recipient)
		} )
	}

	function hapus_barang( id ) {

		var retVal = confirm( "Apakah Anda Yakin?" );
		if ( retVal == true ) {
			window.location.href = "admin.php?page=barang&del=&id=" + id;
			return true;
		} else {

			//window.location = 'admin.php?page=barang&halaman=1&q=';
			return false;
		}
	}

	function edit_barang( id ) {
		$( '#myModal' ).modal( 'show' );
		$( '#edit_form' ).html( "<center>Tunggu Sebentar...</center>" );
		$( '#edit_form' ).load( 'data/barang_edit.php?id=' + id );
		$( '#myModal' ).show();
		$( '#myModal' ).on( 'show.bs.modal', function ( event ) {
			var button = $( event.relatedTarget )
			var recipient = button.data( 'id' )
			var modal = $( this )
			//modal.find( '.modal-title' ).text( 'Edit Barang ' + recipient )
				//modal.find('.modal-body input').val(recipient)
		} )
	}

	function lihat_foto( id ) {
		$( '#mdlfoto' ).modal( 'show' );
		$( '#foto_form' ).html( "<center>Tunggu Sebentar...</center>" );
		$( '#foto_form' ).load( 'data/barang_foto.php?diskon1&&id=' + id );
		$( '#mdlfoto' ).on( 'show.bs.modal', function ( event ) {
			var button = $( event.relatedTarget );
			var recipient = button.data( '1' );
			var modal = $( this );
			//modal.find( '.modal-title' ).text( 'Diskon Barang Reguller'  );
				//modal.find('.modal-body input').val(recipient)
		} );
		$( '#mdldiskon' ).show();
	}

	$( document ).ready( function () {

		$( '#kategori' ).on( 'change', function () {
			var barangID = $( this ).val();
			if ( barangID ) {
				$.ajax( {
					type: 'POST',
					url: 'data/abarang.php',
					data: 'id_kategori=' + barangID,
					success: function ( html ) {
						$( '#subkategori' ).html( html );
					}
				} );
			} else {
				$( '#subkategori' ).html( '<option value="">Kosong</option>' );
			}
		} );
		$('#importdrop').on('change',function(){
			 drop = $('#importdrop');	
			if(drop.is(':checked')){
				$('#importupdate').attr('disabled',true);
			}else{
				$('#importupdate').attr('disabled',false);
			}
		});
		$('#modalTambah').on('show.bs.modal',function(){
			var kode = Math.random().toString(36).slice(10);
			$('#kdbrg').val("brg"+kode+kode+Math.random().toString(36).slice(8));
		});
	} );
</script>