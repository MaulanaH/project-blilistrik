<?php
@session_start();
require_once "../koneksi/koneksi.php";
ob_start();
$form = ( ( "form" ) . ( date( "dmYhi", strtotime( "NOW" ) ) ) );
if ( !( isset( $_SESSION[ 'id_admin' ] ) ) || $_SESSION[ 'id_admin' ] == null ) {
	header("location: logout.php");
}

function histori( $id, $jenis, $jenis2, $keterangan = 'null' ) {
	$koneksi = $GLOBALS[ 'koneksi' ];
	$his = $koneksi->query( sprintf( "INSERT INTO `histori`(`id_admin`, `jenis_histori`, `keterangan`) VALUES ('%s','%s','%s')", $_SESSION[ 'id_admin' ], $jenis, $keterangan ) )or die( $koneksi->error );
	$his2 = $koneksi->query( sprintf( "INSERT INTO `histori_" . $jenis2 . "`(`id_histori`, `id_" . $jenis2 . "`) VALUES ('%s','%s')", $koneksi->insert_id, $id ) )or die( $koneksi->error );
}
$a = mysqli_query( $koneksi, sprintf( "SELECT * FROM admin WHERE id_admin = '%s'", $_SESSION[ 'id_admin' ] ) )or die( mysqli_error( $koneksi ) );
$upser = mysqli_fetch_assoc( $a );
if($a->num_rows==0){
	header("location: logout.php");
}

if( isset( $_POST[ 'form' ] ) && $_POST[ 'form' ] != null){
	switch($_POST[ 'form' ]){
		case 'update_foto_ad': 
			$foto = move_uploaded_file( $_FILES[ 'foto' ][ 'tmp_name' ], "asset/foto/" . $upser[ 'id_admin' ] . "_" . $upser[ 'username' ] . ".png" );
			if ( $foto ) {
		echo "<script>alert('update berhasil');window.location='admin.php';</script> ";
	} else {
		echo "<script>alert('update gagal');window.location='admin.php';</script> ";
	}
		break;
}}

?>
<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Admin
		<?php echo $app_name; ?>
	</title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="robots" content="all,follow">
	<!-- Bootstrap CSS-->
	<link rel="stylesheet" href="../assets/vendor/bootstrap/css/bootstrap.min.css">
	<!-- Font Awesome CSS-->
	<!--link rel="stylesheet" href="../assets/vendor/font-awesome/css/fontawesome.css"-->
	<link rel="stylesheet" href="../assets/vendor/font-awesome/css/all.css">
	<link rel="stylesheet" href="../assets/vendor/font-awesome/css/v4-shims.min.css">
	<!-- Fontastic Custom icon font-->
	<link rel="stylesheet" href="../assets/fonts/fontastic.css">
	<!-- Google fonts - Poppins -->
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,700">
	<!-- theme stylesheet-->
	<link rel="stylesheet" href="../assets/css/style.default.css" id="theme-stylesheet">
	<!-- Custom stylesheet - for your changes-->
	<link rel="stylesheet" href="../assets/css/custom.css">
	<link rel="stylesheet" href="../assets/vendor//jquery-dataTables/datatables.min.css">
	<!-- Favicon-->
	<link rel="shortcut icon" href="../assets/img/favicon.ico">
	<script src="../assets/vendor/jquery/jquery.min.js"></script>
	<!-- Tweaks for older IEs-->
	<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
</head>
<style type="text/css">
	nav.navbar {
		-webkit-box-shadow: 2px 2px 2px rgba(0, 0, 0, 0.0);
		box-shadow: 2px 2px 2px rgba(0, 0, 0, 0.0);
	}
	
	.card-header {
		-webkit-box-shadow: 2px 2px 2px rgba(0, 0, 0, 0.0);
		box-shadow: 2px 2px 2px rgba(0, 0, 0, 0.0);
	}
</style>
<body>
	<!-- modal  foto-->
	<div id="foto" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
		<div role="document" class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h4 id="exampleModalLabel" class="modal-title">Foto Profile <?php echo $upser['id_admin']?></h4>
					<button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
				</div>
				<div class="modal-body">
					<img class="img-thumbnail NO-CACHE" src="asset/foto/<?php echo $upser['id_admin']."_".$upser['username'].".png"?>" alt="Card image" style="width:100%;">
				</div>
				<div class="modal-footer">
					<form action="" method="post" enctype="multipart/form-data">
						<div class="form-group form-inline">
							<label for="foto">Upload Foto</label>
							<input type="file" name="foto" id="foto" accept="image/*" class="form-control-file" placeholder="foto">
						</div>
						<button type="button" data-dismiss="modal" class="btn btn-secondary">Close</button>
						<button class="btn btn-primary" name="foto">Save changes</button>
						<input type="hidden" name="form" value="update_foto_ad">
					</form>
				</div>
			</div>
		</div>
	</div>

	<div class="page">
		<!-- Main Navbar-->
		<header class="header">
			<nav class="navbar">
				<div class="container-fluid">
					<div class="navbar-holder d-flex align-items-center justify-content-between">
						<!-- Navbar Header-->
						<div class="navbar-header">
							<!-- Navbar Brand -->
							<a href="admin.php" class="navbar-brand">
								<div class="brand-text brand-big"><span>Admin Blilistrik</span>
									<strong>
										<?php echo $app_name; ?>
									</strong>
								</div>
								<div class="brand-text brand-small"><strong>Admin</strong>
								</div>
							</a>
							<!-- Toggle Button--><a id="toggle-btn" href="#" class="menu-btn active"><span></span><span></span><span></span></a>
						</div>
						<!-- Navbar Menu -->
						<ul class="nav-menu list-unstyled d-flex flex-md-row align-items-md-center">
							<!-- Notifications-->
							<!--li class="nav-item dropdown"> <a id="notifications " rel="nofollow" data-target="#" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link ibu"><i class="fa fa-bell-o"></i><span class="badge bg-red cobu"></span></a>
							<ul aria-labelledby="notifications" class="dropdown-menu nobu">
							</ul>
							</li-->
							<!-- Messages  -->
							<li class="nav-item dropdown"> <a id="notifications " rel="nofollow" data-target="#" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link ibu">B <i class="fa fa-envelope"></i><span class="badge bg-red cobu"></span></a>
								<ul aria-labelledby="notifications" class="dropdown-menu nobu">

								</ul>
							</li>
							<li class="nav-item dropdown"> <a id="messages" rel="nofollow" data-target="#" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link isu">S <i class="fa fa-envelope"></i><span class="badge bg-orange cosu"></span></a>
								<ul aria-labelledby="notifications" class="dropdown-menu nosu">

								</ul>
							</li>
							<!--li class="nav-item dropdown"> <a id="messages" rel="nofollow" data-target="#" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link ilang"><i class="fa fa-envelope-o"></i><span class="badge bg-orange count"></span></a>
								<ul aria-labelledby="notifications" class="dropdown-menu notif">

								</ul>
								</li>
								<li class="nav-item dropdown"> <a id="messages" rel="nofollow" data-target="#" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link ilang"><i class="fa fa-envelope-o"></i><span class="badge bg-orange count"></span></a>
								<ul aria-labelledby="notifications" class="dropdown-menu notif">

								</ul>
								</li-->
							<!-- Logout    -->
							<li class="nav-item"><a href="logout.php" class="nav-link logout">Logout<i class="fa fa-sign-out-alt"></i></a>
							</li>
						</ul>
					</div>
				</div>
			</nav>
		</header>
		<div class="page-content d-flex align-items-stretch" style="">
			<!-- Side Navbar -->
			<nav class="side-navbar">
				<?php
				if ( isset( $_SESSION[ 'id_admin' ] ) && $_SESSION[ 'id_admin' ] != NULL ) {
					$quser = mysqli_query( $koneksi, sprintf( "SELECT * FROM admin WHERE id_admin = '%s'", $_SESSION[ 'id_admin' ] ) )or die( mysqli_error( $koneksi ) );
					$row = mysqli_fetch_assoc( $quser )
					?>
				<!-- Sidebar Header-->
				<div class="sidebar-header d-flex align-items-center">
					<a href="" data-toggle="modal" data-target="#foto">
						<div class="avatar">
							<img src="asset/foto/<?php echo $upser['id_admin']."_".$upser['username'].".png"?>" alt="..." class="img-fluid rounded-circle NO-CACHE">
						</div>
					</a>
					<div class="title">
						<p>Selamat Datang</p>
						<h1 class="h4">
							<?php echo $row["fnama"];?>
						</h1>

					</div>
				</div>
				<?php }
				$notifa = $koneksi->query("SELECT * FROM reseller_komisi_ambil where status != belum");
				$rnotifa = $notifa->num_rows;
				?>
				
				<!-- Sidebar Navidation Menus-->
				<span class="heading">Main</span>
				<ul class="list-unstyled kategori">
					<li><a href="admin.php"> <i class="fa fa-warehouse"></i>Home</a>
					</li>
					<li><a href="?page=profile"> <i class="fa fa-user"></i>Profile</a>
					</li>
					<li><a href="?page=admin"> <i class="fa fa-user-friends"></i>Admin</a>
					</li>
					<li><a href="?page=reseller"> <i class="fa fa-store"></i>Reseller</a>
					</li>
					<li><a href="?page=user"> <i class="fa fa-users"></i>User</a>
					</li>
				</ul>
				<span class="heading">Produk</span>
				<ul>					

					<li><a href="?page=barang"> <i class="fa fa-cubes"></i>Barang</a>
					</li>
					<li><a href="?page=diskon"> <i class="fa fa-percent"></i>Diskon reguler</a>
					</li>
					<li><a href="?page=diskon_reseller"> <i class="fa fa-percent"></i>Diskon reseller</a>
					</li>
					</ul>
				<!--span class="heading">Transaksi</span-->
				<ul>
				<!--li><a href="?page=komisi_reseller_detail"><i class="fa fa-dot-circle-o"></i>Komisi Reseller</a>
				</li-->
				<li><a href="?page=komisi_reseller"><i class="fa fa-dot-circle-o"></i>Penarikan Komisi reseller <?php if($rnotifa!=0){?><sup class="badge badge-info"><?php echo $rnotifa; ?></sup><?php } ?> </a>
					</li>
				</ul>
				<!--ul>
					<li><a href="#exampledropdownDropdown" aria-expanded="false" data-toggle="collapse"> <i class="fa fa-shopping-bag"></i>Transaksi </a>
						<ul id="exampledropdownDropdown" class="collapse list-unstyled ">
							<li><a href="?page=tranbe"> <i class="icon-interface-windows"></i>Belum Dibayar</a>
							</li>
							<li><a href="?page=transu"> <i class="icon-interface-windows"></i>Sudah Dibayar</a>
							</li>
							<li><a href="?page=tranki"> <i class="icon-interface-windows"></i>Dikirim</a>
							</li>
							<li><a href="?page=trante"> <i class="icon-interface-windows"></i>Diterima</a>
							</li>
							<li><a href="?page=tranbat"> <i class="icon-interface-windows"></i>Batal</a>
							</li></ul>
					</li>						</ul-->
				<ul>
					<li><a href="#exampledropdownDropdown" aria-expanded="false" data-toggle="collapse"> <i class="fa fa-shopping-bag"></i>Transaksi </a>
						<ul id="exampledropdownDropdown" class="collapse list-unstyled ">
							<li><a href="?page=transaksi"> <i class="icon-interface-windows"></i>Semua</a>							</li>
							<li><a href="?page=transaksi&&status=tranbe"> <i class="icon-interface-windows"></i>Belum Dibayar</a>							</li>
							<li><a href="?page=transaksi&&status=transu"> <i class="icon-interface-windows"></i>Sudah Dibayar</a>							</li>
							<li><a href="?page=transaksi&&status=tranki"> <i class="icon-interface-windows"></i>Dikirim</a>							</li>
							<li><a href="?page=transaksi&&status=trante"> <i class="icon-interface-windows"></i>Diterima</a>							</li>
							<li><a href="?page=transaksi&&status=tranbat"> <i class="icon-interface-windows"></i>Batal</a>							</li></ul>
					</li>						</ul>
				<span class="heading">Histori</span>
				<ul>

					<li><a href="?page=histori"> <i class="fa fa-history"></i>Histori admin</a>
					</li>
					<li><a href="?page=histori_barang&&jenis=masuk"> <i class="fa fa-history"></i>Histori barang masuk</a>
					</li>
					<li><a href="?page=histori_barang&&jenis=keluar"> <i class="fa fa-history"></i>Histori barang keluar</a>
					</li>
					<!--li><a href="?page=histori_barang_masuk"> <i class="fa fa-history"></i>Histori barang masuk</a>
					</li>
					<li><a href="?page=histori_barang_keluar"> <i class="fa fa-history"></i>Histori barang keluar</a>
					</li>
					<li><a href="?page=histori_transaksi"> <i class="fa fa-history"></i>Histori transaksi</a>
					</li-->
				</ul><ul>
					<li><a href="?page=tambahan"> <i class="icon-interface-windows"></i>Tambahan</a>
					</li>
					</ul>
			</nav>
			<div class="content-inner">
				<?php
				$page = @$_GET[ 'page' ];
				switch ( $page ) {
					case 'profile':
						$data = "profile";
						break;
					case 'user':
						$data = "user";
						break;
					case 'admin':
						$data = "admin";
						break;
					case 'reseller':
						$data = "reseller";
						break;
					case 'histori':
						$data = "histori";
						break;
					case 'barang':
						$data = "barang";
						break;
					case 'diskon':
						$data = "diskon";
						break;
					case 'komisi_reseller':
						$data = "reseller_komisi";
						break;
					case 'komisi_reseller_detail':
						$data = "reseller_komisi_detail";
						break;
					case 'diskon_reseller':
						$data = "diskon_reseller";
						break;
					case 'histori_barang':
						$data = "histori_barang";
						break;					
					/*case 'histori_barang_masuk':
						$data = "histori_barang_masuk";
						break;					
					case 'histori_barang_keluar':
						$data = "histori_barang_keluar";
						break;	*/
					case 'transaksi':
						$data = "transaksi";
						break;
					case 'tranbe':
						$data = "tranbe";
						break;
					case 'transu':
						$data = "transu";
						break;
					case 'tranki':
						$data = "tranki";
						break;
					case 'trante':
						$data = "trante";
						break;
					case 'tranbat':
						$data = "tranbat";
						break;
					case 'tambahan':
						$data = "tambahan";
						break;
					case 'histori_transaksi':
						$data = "histori_transaksi";
						break;
					default:
						$data = "home";
						break;
				}
				include_once "data/" . $data . ".php";
				?>
				<!-- Page Footer-->
				<footer class="main-footer">
					<div class="container-fluid">
						<div class="row">
							<div class="col-sm-6">
								<p>Blilistrik.com &copy; 2018</p>
							</div>
							<div class="col-sm-6 text-right">
								<p>Creator and Design by <a href="https://bootstrapious.com/admin-templates" class="external">Bootstrapious</a>
								</p>
								<!-- Please do not remove the backlink to us unless you support further theme's development at https://bootstrapious.com/donate. It is part of the license conditions. Thank you for understanding :)-->
								<p>ReDesign by Team Mojokerto Developer</p>

							</div>
						</div>
					</div>
				</footer>
			</div>
		</div>
	</div>

	<!-- Javascript files-->
	<!--script src="../assets/vendor/jquery/jquery.min.js"></script-->
	<script src="../assets/vendor/popper.js/umd/popper.min.js"></script>
	<!--script src="../assets/vendor/bootstrap/js/bootstrap.min.js"></script-->	
	<script src="../assets/vendor/chart.js/Chart.min.js"></script>
	<script src="../assets/vendor/jquery.cookie/jquery.cookie.js"></script>
	<!--###-->
	<script src="../assets/vendor/jquery-validation/jquery.validate.min.js"></script>
	<script src="../assets/vendor/jquery-dataTables/dataTables.min.js"></script>
	<script src="../assets/vendor/jquery-dataTables//DataTables-1.10.18/js/dataTables.bootstrap4.min.js"></script>
	<!--- <script src="asset/js/charts-home.js"></script> -->
	<!-- Main File-->
	<script src="../assets/js/front.js"></script>
	<script type="text/javascript">
	function rand_pass(){
		var pass = Math.random().toString(36).slice(-8);
		return pass;
	}	
$( document ).ready( function () {
	$( '.cari' ).keydown( function ( e ) {
		if ( e.keyCode == 13 ) {
			var table = $( this ).attr( 'table' );
			//alert( table );
			window.location.href = '?page=' + table + '&cari=' + $( '.cari' ).val();
		}
	} );
	$( '.NO-CACHE' ).attr( 'src', function () {
		return $( this ).attr( 'src' ) + "?upload=" + Math.random()
	} );
	//    var nods = document.getElementsByClassName('NO-CACHE');
	//for (var i = 0; i < nods.length; i++)
	//{
	//    nods[i].attributes['src'].value += "?upload=" + Math.random();
	//}
	// updating the view with notifications using ajax
	function load_unseen_notification( view = '' ) {
		$.ajax( {
			url: "nobu.php",
			method: "POST",
			data: {
				view: view
			},
			dataType: "json",
			success: function ( data ) {
				$( '.nobu' ).html( data.notification );
				if ( data.unseen_notification > 0 ) {
					$( '.cobu' ).html( data.unseen_notification );
				}
			}
		} );
	}
	load_unseen_notification();
	// load new notifications
	$( document ).on( 'click', '.ibu', function () {
		$( '.cobu' ).html( '' );
		load_unseen_notification( 'yes' );
	} );
	setInterval( function () {
		load_unseen_notification();;
	}, 5000 );
	$( document ).ready( function () {
		$( '#example' ).DataTable( {
		scrollY: '50vh',
		deferRender: true,
		keys: true,
        stateSave: true,
		scrollCollapse: true,
		paging: true,
		//searching: false,
		fixedColumns:{
			rightColumns: 1,
		},
		scrollX: true,
		Responsive: true,order: [[ 0, 'desc' ]],
		RowReorder: true,
		
	} );
	//sudah di bayar
	// updating the view with notifications using ajax
	function load_unseen_notification( view = '' ) {
		$.ajax( {
			url: "nosu.php",
			method: "POST",
			data: {
				view: view
			},
			dataType: "json",
			success: function ( data ) {
				$( '.nosu' ).html( data.notification );
				if ( data.unseen_notification > 0 ) {
					$( '.cosu' ).html( data.unseen_notification );
				}
			}
		} );
	}
	load_unseen_notification();
	// load new notifications
	$( document ).on( 'click', '.isu', function () {
		$( '.cosu' ).html( '' );
		load_unseen_notification( 'yes' );
	} );
	setInterval( function () {
		load_unseen_notification();;
	}, 5000 );
	} );
	$( '#provinsi' ).on( 'change', function () {
		var selectVal = $( "#provinsi option:selected" ).val();
		$( '#kota_kab' ).load( 'action.php?kab_kota=&id=' + selectVal );
	} );
	$( '#provinsi1' ).on( 'change', function () {
		var selectVal = $( "#provinsi1 option:selected" ).val();
		$( '#kota_kab1' ).load( 'action.php?kab_kota=&id=' + selectVal );
	} );
} );

	</script>


</body>
</html>