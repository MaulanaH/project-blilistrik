<?php


function autoload($className)
{
//list comma separated directory name
$directory = array('', 'kernel/', 'model/', 'controller/');

//list of comma separated file format
$fileFormat = array('%s.php', '%s.class.php');

foreach ($directory as $current_dir)
{
    foreach ($fileFormat as $current_format)
    {

        $path = $current_dir.sprintf($current_format, $className);
        if (file_exists($path))
        {
            include $path;
            return ;
        }
    }
}
}
spl_autoload_register('autoload');


function loadfile($dirname) {
	include __DIR__ . '/' . $dirname . '.php';
}





loadfile("config/app");
loadfile("config/database");