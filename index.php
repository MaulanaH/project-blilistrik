<?php require_once "header.php"; ?>
<style type="text/css">
         .quantity input {
  -webkit-appearance: none;
  border: none;
  text-align: center;
  width: 32px;
  font-size: 16px;
  color: #43484D;
  font-weight: 300;
}
 
.a{
  width: 30px;
  height: 30px;
  background-color: #E1E8EE;
  border-radius: 6px;
  border: none;
  cursor: pointer;
}
.minus-btn img {
  margin-bottom: 3px;
}
.plus-btn img {
  margin-top: 2px;
}
 
.a button:focus,
.b input:focus {
  outline:0;
}
.stokout{
        position: absolute;
    z-index: 1;
    width: 100%;
    height: 45px;
}
.card-body {
    min-height: 100px;
}
.wrapper {
  max-width:200px;
  width:100%;
}
.carousel-item-next, .carousel-item-prev, .carousel-item.active {
    display: block !important;
}

</style>
<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel" style="width: 100%;padding: 0;margin-top: 71px; ">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner" role="listbox">
    <div class="carousel-item active">
      <div class="img"><img  onerror="this.onerror=null;this.src='img/10bf08f0bbd6689475be65b4ae441bd9/default.png';" class="d-block img-fluid w-100 slid" src="img/10bf08f0bbd6689475be65b4ae441bd9/c4ca4238a0b923820dcc509a6f75849b356a192b7913b04c54574d18c28d46e6395428ab.png" alt="First slide"></div>
    </div>
    <div class="carousel-item">
      <div class="img"><img onerror="this.onerror=null;this.src='img/10bf08f0bbd6689475be65b4ae441bd9/default.png';" class="d-block img-fluid w-100 slid" src="img/10bf08f0bbd6689475be65b4ae441bd9/c81e728d9d4c2f636f067f89cc14862cda4b9237bacccdf19c0760cab7aec4a8359010b0.png" alt="Second slide"></div>
    </div>
    <div class="carousel-item">
      <div class="img"><img onerror="this.onerror=null;this.src='img/10bf08f0bbd6689475be65b4ae441bd9/default.png';" class="d-block img-fluid w-100 slid" src="img/10bf08f0bbd6689475be65b4ae441bd9/eccbc87e4b5ce2fe28308fd9f2a7baf377de68daecd823babbb58edb1c8e14d7106e83bb.png" alt="Third slide"></div>
    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>

    <br>
        <div class="container-fluid row" style="margin: unset;padding: 0px;">
            <div class="col-lg-12">
                
                <div class="card" style="border-radius:0;">
                    <div class="card-body" style="min-width: 10px !important;width: auto;">
                        <?php
                        $result=mysqli_query($koneksi,"SELECT * FROM brand");
                        while ($a=mysqli_fetch_assoc($result)) { 
                            if (isset($a['logo']) and $a['logo'] != '' and $a['logo'] !== null) {
                            ?>
                            <a href="search.php?b=<?php echo $a['nama_brand'];?>"><img alt="<?php echo $a['nama_brand'];?>" title="<?php echo $a['nama_brand'];?>" style="max-height: 32px;" src="img/8c4d3a946a1fcde2ded7e17651fd0ed7/<?php echo $a['logo'];?>" class=""></a>
                        <?php } else {} } ?>
                    </div>
                </div>
            </div>
            <div class="col-lg-2 col-sm-4" style="margin-top:25px;">
                        <?php
                        $query = mysqli_query($koneksi, "SELECT * FROM kategori");
                        $temp_k = '';
                        while ($a = mysqli_fetch_assoc($query)) {
                        ?>
                        <!-- Default dropright button -->
                        <div class="btn-group dropright" style="display:block;">
                          <a href="javascript:void(0);" class="btn btn-outline-primary btn-block dropdown-toggle" style="border-radius:0;text-align:left;display:inline-block;width:100%;" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <?php echo $a['nama_kategori'];?>
                          </a>
                         <div class="dropdown-menu" style="border-radius:0;max-height: 50vh;overflow-y: scroll;">
                            <?php
                            $idnya = $a['id_kategori'];
                            $query2 = mysqli_query($koneksi, "SELECT * FROM kategori_sub WHERE id_kategori='$idnya'");
                            while ($b = mysqli_fetch_assoc($query2)) { ?>
                            <a class="dropdown-item" href="search.php?sk=<?php echo $b['nama_sub_kategori'];?>"><?php echo $b['nama_sub_kategori'];?></a>
                            <?php } ?>
                            <!--a class="dropdown-item" href="#">Separated link</a-->
                          </div>
                        </div>

                        <?php
                        }
                        ?>
            </div>
            <div class="col-lg-10 col-sm-8">


                <h2 style="margin-top: 25px;">Paling Di Cari!</h2><hr>
                <div class="row ">
                    <?php
                    $result=mysqli_query($koneksi,"SELECT * FROM barang WHERE status='aktif' ORDER BY view DESC LIMIT 4");
                    while ($a=mysqli_fetch_assoc($result)) {
                        $gambar = mysqli_query($koneksi, "SELECT * FROM gambar_barang WHERE id_barang='".$a['id_barang']."'");
                        ?>
                        <!-- echo "|" . $a['nama_barang']; -->
                    <div class="col-lg-3 col-6">
                            <div class="card" style="margin-bottom: 20px;">
                                <a href="view?barang=<?php echo $a['id_barang'];?>" style="border-bottom: 2px solid #e6e6e6; cursor: pointer; height: 190px;width: 100%" alt="Lihat detil barang">
                                    <img  class="card-img-top NO-CACHE " src="img/177cbf2b2fda8daf8688bd68a5ea6e14/<?php echo md5($a['id_barang']);?>/<?php echo mysqli_fetch_assoc($gambar)['foto_barang'];?>" style="max-width: 100%; max-height: 100%;display: block; background-repeat: no-repeat; background-position: center;min-height: 100%;" align="middle">
                                </a>
                                <div class="card-body " style="padding :8px 10px 10px;height: auto;">
                                     <a href="view?barang=<?php echo $a['id_barang'];?>" style="border-bottom: 2px solid #e6e6e6; cursor: pointer; height: 190px;width: 100%;text-decoration: none;color: black;" alt="Lihat detil barang">
                                        <h5 class="card-title judulbarang" style=""><?php echo $a['nama_barang'];?></h5>
                                    </a>
                                        <p class="card-text m-0">
                                            <?php 
                                            $reseller = " AND id_reseller ";
                            $reseller .= isset($_SESSION['user']['login']['reseller']) ? " = ".$_SESSION['user']['login']['reseller']:" IS NULL ";
                            $qdis = sprintf("SELECT * FROM diskon d left join diskon_reseller dr on d.id_diskon = dr.id_diskon WHERE d.id_barang = %s AND status = 1 $reseller ",$a['id_barang']);
                            $dis= $koneksi->query($qdis." AND qty=1");
                            $dis2= $koneksi->query($qdis);
                            $rdis = $dis->fetch_assoc();
                            if($dis->num_rows){
                                ?>
                                <div style="color: #ff9f9f;font-size: 12px; text-decoration: line-through;" class="d-inline-block mr-4">Rp. <?php echo number_format($a['harga_barang']);?></div><div class="d-inline-block">Rp. <?php echo number_format($a['harga_barang']-($a['harga_barang']*$rdis['diskon']/100));?></div>
                                <?php }else{ ?>
                            <?php echo "Rp. ".number_format($a['harga_barang']);?>
                            <?php } ?>
                                        </p>
                                </div>


                                
                            </div>
                        </div>
                    <?php } ?>
                </div>
                
        </div>
            <!--div class="container col-lg-10" style="padding:0;margin:auto;">
                <div class="card" style="background:#f1c40f;padding:20px;">

                <div class="divider" style="height: 25px;"></div>
                <h2>Baru Ditambahkan</h2><hr>
                <div class="row ">
                <?php
                $result=mysqli_query($koneksi,"SELECT * FROM barang WHERE status='aktif' ORDER BY id_barang DESC LIMIT 6");
                while ($a=mysqli_fetch_assoc($result)) { 
                        $gambar = mysqli_query($koneksi, "SELECT * FROM gambar_barang WHERE id_barang='".$a['id_barang']."'");
                        ?>
                    <div class="col-lg-2 col-6" style="margin:auto;">
                            <div class="card" style="margin-bottom: 20px;">
                                <a href="view?barang=<?php echo $a['id_barang'];?>" style="border-bottom: 2px solid #e6e6e6; cursor: pointer; height: 190px;width: 100%" alt="Lihat detil barang">
                                    <img  class="card-img-top NO-CACHE " src="img/177cbf2b2fda8daf8688bd68a5ea6e14/<?php echo md5($a['id_barang']);?>/<?php echo mysqli_fetch_assoc($gambar)['foto_barang'];?>" style="max-width: 100%; max-height: 100%;display: block;  background-repeat: no-repeat; background-position: center;min-height: 100%;" align="middle">
                                </a>
                                <div class="card-body " style="padding :8px 10px 10px;height: auto;">
                                     <a href="view?barang=<?php echo $a['id_barang'];?>" style="border-bottom: 2px solid #e6e6e6; cursor: pointer; height: 190px;width: 100%;text-decoration: none;color: black;" alt="Lihat detil barang">
                                        <h5 class="card-title judulbarang" style=""><?php echo $a['nama_barang'];?></h5>
                                    </a>
                                        <p class="card-text m-0">
                                            <?php 
                                            $reseller = " AND id_reseller ";
                            $reseller .= isset($_SESSION['user']['login']['reseller']) ? " = ".$_SESSION['user']['login']['reseller']:" IS NULL ";
                            $qdis = sprintf("SELECT * FROM diskon d left join diskon_reseller dr on d.id_diskon = dr.id_diskon WHERE d.id_barang = %s AND status = 1 $reseller ",$a['id_barang']);
                            $dis= $koneksi->query($qdis." AND qty=1");
                            $dis2= $koneksi->query($qdis);
                            $rdis = $dis->fetch_assoc();
                            if($dis->num_rows){
                                ?>
                                <div style="color: #ff9f9f;font-size: 12px; text-decoration: line-through;" class="d-inline-block mr-4">Rp. <?php echo number_format($a['harga_barang']);?></div><div class="d-inline-block">Rp. <?php echo number_format($a['harga_barang']-($a['harga_barang']*$rdis['diskon']/100));?></div>
                                <?php }else{ ?>
                            <?php echo "Rp. ".number_format($a['harga_barang']);?>
                            <?php } ?>
                                        </p>
                                </div>                               
                                
                            </div>
                        </div>
                <?php } ?>
                    </div>
                </div>
            </div-->
            <div class="container col-lg-10" style="margin:auto;">
                <?php
            $result=mysqli_query($koneksi,"SELECT * FROM kategori");
            while ($b=mysqli_fetch_assoc($result)) { 
                $ini_id = $b['id_kategori'];
                $data = " WHERE k.id_kategori=" . $ini_id . " AND status='aktif'  ORDER BY RAND() LIMIT 6";
                $i = "SELECT * FROM barang b JOIN kategori_sub ks on b.id_sub_kategori=ks.id_sub_kategori JOIN kategori k on ks.id_kategori=k.id_kategori".$data;
                $tampil = mysqli_query($koneksi, $i);
                if (mysqli_num_rows($tampil) == 0) {} else {
            ?>

            <div class="divider" style="height: 25px;"></div>
            <h2><?php echo $b['nama_kategori']; ?></h2><hr>
            <div class="row ">
                <?php
                while ($a=mysqli_fetch_assoc($tampil)) { 
                        $gambar = mysqli_query($koneksi, "SELECT * FROM gambar_barang WHERE id_barang='".$a['id_barang']."'");
                        ?>
                    <div class="col-lg-2 col-6">
                            <div class="card" style="margin-bottom: 20px;">
                                <a href="view?barang=<?php echo $a['id_barang'];?>" style="border-bottom: 2px solid #e6e6e6; cursor: pointer; height: 190px;width: 100%" alt="Lihat detil barang">
                                    <img  class="card-img-top NO-CACHE " src="img/177cbf2b2fda8daf8688bd68a5ea6e14/<?php echo md5($a['id_barang']);?>/<?php echo mysqli_fetch_assoc($gambar)['foto_barang'];?>" style="max-width: 100%; max-height: 100%;display: block;  background-repeat: no-repeat; background-position: center;min-height: 100%;" align="middle">
                                </a>
                                <div class="card-body " style="padding :8px 10px 10px;height: auto;">
                                     <a href="view?barang=<?php echo $a['id_barang'];?>" style="border-bottom: 2px solid #e6e6e6; cursor: pointer; height: 190px;width: 100%;text-decoration: none;color: black;" alt="Lihat detil barang">
                                        <h5 class="card-title judulbarang" style=""><?php echo $a['nama_barang'];?></h5>
                                    </a>
                                        <p class="card-text m-0">
                                            <?php 
                                            $reseller = " AND id_reseller ";
                            $reseller .= isset($_SESSION['user']['login']['reseller']) ? " = ".$_SESSION['user']['login']['reseller']:" IS NULL ";
                            $qdis = sprintf("SELECT * FROM diskon d left join diskon_reseller dr on d.id_diskon = dr.id_diskon WHERE d.id_barang = %s AND status = 1 $reseller ",$a['id_barang']);
                            $dis= $koneksi->query($qdis." AND qty=1");
                            $dis2= $koneksi->query($qdis);
                            $rdis = $dis->fetch_assoc();
                            if($dis->num_rows){
                                ?>
                                <div style="color: #ff9f9f;font-size: 12px; text-decoration: line-through;" class="d-inline-block mr-4">Rp. <?php echo number_format($a['harga_barang']);?></div><div class="d-inline-block">Rp. <?php echo number_format($a['harga_barang']-($a['harga_barang']*$rdis['diskon']/100));?></div>
                                <?php }else{ ?>
                            <?php echo "Rp. ".number_format($a['harga_barang']);?>
                            <?php } ?>
                                        </p>
                                </div>                               
                                
                            </div>
                        </div>
                <?php } ?>
                <div style="text-align: right;width: 100%;padding-right: 25px;margin-bottom: 20px;"><a class="btn btn-outline-primary" href="search.php?k=<?php echo $b['nama_kategori'];?>">Selengkapnya...</a></div>
            </div>
            <?php } } ?>
            </div>
        </div>
    </br>
</div>
<!-- Modal -->
<div aria-hidden="true" aria-labelledby="keranjanglabel" class="modal fade" id="keranjangmodal" role="dialog" tabindex="-1">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="border: 0px;">
                <h5 class="modal-title" id="keranjanglabel">
                    Keranjang
                </h5>
                <button aria-label="Close" class="close" data-dismiss="modal" type="button">
                    <span aria-hidden="true">
                        ×
                    </span>
                </button>
            </div>
            <div class="modal-body" style="border: 0px;">
                <div class="row" style="margin: 0">
                    <div class="col-lg-3">
                        <img class="card-img-top rounded" id="barang_foto" src="#" style="height: 100px;width: 100px">
                        </img>
                    </div>
                    <div class="col-lg-9">
                        <table width="100%">
                            <tr><td><span class="judulbarang" id="barang_judul">Contoh</span></td><td align="right">
                                <button class="card-text text-center btn text-danger" id="delete_keranjang" style="background-color: transparent;" type="button">
                            <i class="material-icons">
                                remove_shopping_cart
                            </i>
                        </button></td></tr>
                            <tr><td>Harga</td><td><span id="barang_harga">Rp. 9999999999</span></td></tr>
                            <tr><td>Stok</td><td><span id="barang_stok">9999999999</span></td></tr>
                            <tr><td>Jumlah</td><td>
                                <span>
                                    <p style="float: left;margin: 0">
                                        
                                    </p>
                                    <div class="quantity" style="float: left;">
                                        <button class="minus-btn a" id="minus_keranjang" type="button">
                                            -
                                        </button>
                                        <input class="b" id="barang_jumlah" name="name" type="text" value="0">
                                            <button class="plus-btn a" id="plus_keranjang" type="button">
                                                +
                                            </button>
                                        </input>
                                    </div>
                                </span>
                            </td>
                        </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer " style="border: 0px;">
                <button class="btn btn-secondary " data-dismiss="modal" type="button"><p class="m-0 ahh">Lanjutkan Belanja</p>
                    
                </button>
                <a href="<?php echo $root_base?>/transaksi/keranjang" class="btn btn-primary " type="button">
                    <p class="m-0 ahh">Lanjutkan Pembayaran</p>
                </a>
            </div>
        </div>
    </div>
</div>
<?php include 'footer.php' ?>




<?php  
//require_once "koneksi/koneksi.php";
//  $bquary = mysqli_query($koneksi,sprintf("SELECT * FROM kategori WHERE id_barang = '%s'"))or die(get_error());
//  $aa = mysqli_fetch_assoc($bquary);

?>


 <!-- modal  -->

  <div id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
   <div role="document" class="modal-dialog">
      <div class="modal-content"> 
      <div class="modal-header">
        <h4 id="exampleModalLabel" class="modal-title">aa<?php echo $a['nama_barang'];?></h4>
        <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
      </div>
         <div class="modal-body">
            <div id="edit_form">

            </div>
         </div>
        <div class="modal-footer">
    <form action="" method="post" enctype="multipart/form-data" >                     
        <button type="button" data-dismiss="modal" class="btn btn-secondary">Close</button>
        <button  class="btn btn-primary" name="foto">Save changes</button>

        <input type="hidden" name="form" value="update_foto">
    </form>          
        </div>           
       </div>
    </div>
  </div>









<script type="text/javascript">
    var id=0;


                  $(document).ready(function(){
                $('.carousel').carousel();
            
$('#minus_keranjang').click(function(event) {
    $.ajax({
        url: 'transaksi/keranjang.php',
        type: 'POST',
        dataType: 'json',
        data: {'form': 'minus_keranjang','id':id},
        success(data){
            $('#barang_jumlah').val(data['jumlah']);
        }
    })
});
$('#plus_keranjang').click(function(event) {
    $.ajax({
        url: 'transaksi/keranjang.php',
        type: 'POST',
        dataType: 'json',
        data: {'form': 'plus_keranjang','id':id},
        success(data){
                $('#barang_jumlah').val(data['jumlah']);
        }
    })
});
$('#barang_jumlah').change(function(event) {
    $.ajax({
        url: 'transaksi/keranjang.php',
        type: 'POST',
        dataType: 'json',
        data: {'form': 'edit_keranjang','id':id, 'jumlah':$(this).val()},
        success(data){
                $('#barang_jumlah').val(data['jumlah']);
            }
    })
});
var mql = window.matchMedia("screen and (max-width: 900px)")
$('#delete_keranjang').click(function(event) {
    $.ajax({
        url: 'transaksi/keranjang.php',
        type: 'POST',
        dataType: 'json',
        data: {'form': 'delete_keranjang','id':id},
    }).done(function(data) {
        $('#keranjangmodal').modal('hide');
        if (mql.matches) {
            if(data['count']>0){
            $('#mcount').html(data['count']);
        }else{
            $('#mcount').addClass('d-none');

        }
        }else{
            if(data['count']>0){
            $('#countker').html(data['count']);
        }else{
            $('#countker').addClass('d-none');

        }            
        }
        
    })
});
$('.buy').click(function(event) {
    var harga = "Rp. "+$(this).attr('val2');
    var nama = $(this).attr('val1');
    var foto = $(this).attr('val3');
    var stok = $(this).attr('val5');
    id = $(this).attr('val4');
    $('#barang_harga').html(harga); 
    $('#barang_judul').html(nama);    
    $('#barang_stok').html(stok);
    $('#barang_foto').attr('src', foto);

    $.ajax({
        url: 'transaksi/keranjang.php',
        type: 'POST',
        dataType: 'json',
        data: {'form': 'add_keranjang','id':id},
        success(data){
            if (mql.matches) {
            if(data['notif']!=null){
                $('#barang_jumlah').val(data['jumlah']);
                alert(data['notif']);
            }else{
                $('#barang_jumlah').val(1);
                $('#mcount').html(data['count']);
                $('#mcount').removeClass('d-none');
                $('#countker').removeClass('count');
            }    

        }else{
            if(data['notif']!=null){
                $('#barang_jumlah').val(data['jumlah']);
                alert(data['notif']);
            }else{
                $('#barang_jumlah').val(1);
                $('#countker').html(data['count']);
                $('#countker').removeClass('d-none');

            } 
        
        }
        }
    })
    .done(function() {
        $('#keranjangmodal').modal('show');
    })
    
});



});

</script>
<script type="text/javascript">
         function lihat(id){
          $('#myModal').modal('show');
          $('#edit_form').html("<center>Tunggu Sebentar...</center>");
          $('#edit_form').load('lihat.php?id='+id); 
    }

       
</script>