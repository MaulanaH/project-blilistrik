var animInterval = 500;
$('.drawer-toggle').click(function(){
	$('.drawer-bg').fadeIn(animInterval);
	$('.drawer').fadeIn(animInterval);
	$('body').css({'overflow': 'hidden'});
});
$('.drawer-bg').click(function(){
	$('.drawer-bg').fadeOut(animInterval);
	$('.drawer').fadeOut(animInterval);
	$('body').css({'overflow': 'auto'});
});