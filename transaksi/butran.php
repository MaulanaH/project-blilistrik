<?php 
if(isset($_POST['form'])&&$_POST['form']!=null){
  require_once "../koneksi/koneksi.php"; 
  $ruser = chk_login();
  $iduser = $ruser['id_user'];
  $ua = $koneksi->query("SELECT * FROM users WHERE id_user='$iduser'") or die(get_error());
  $uupser = mysqli_fetch_assoc($ua);
}else{
  require_once '../header.php';
  $ruser = chk_login();
  $iduser = $ruser['id_user'];
  $ua = $koneksi->query("SELECT * FROM users WHERE id_user='$iduser'") or die(get_error());
  $uupser = mysqli_fetch_assoc($ua);
  $idd = $_POST['id_foto'];
  if (isset($_GET['id_foto'])) {
    $idtransaksii = $_GET['id_foto'];
    $aa = $koneksi->query("SELECT * FROM transaksi_barang WHERE id_transaksi='$idtransaksii'") or die(get_error());
    $upserr = mysqli_fetch_array($aa);
      echo "<script>alert('update berhasil ".$upser['id_transaksi']."');</script> ";
    }
?> 
<body>
<div class="container-fluid" style="width: 100%;padding: 0;margin-top: 80px;">

<div class="row" style="margin: unset;">
  <div class="col-lg-2">
    <?php get_sidebar('butran'); ?>
  </div>
  <div class="col-lg-10">
<div class="card card-register mx-auto card-body-putih">
  <div class="card-header bg-biru text-white h4">Cetak Transaksi</div>
    <div class="card-body card-body-putih row" style="margin: 0;">
      <div class="table-responsive">   
          <table class="table table-striped table-sm">
            <thead>
               <tr>
                  <th>#</th>
                  <th>Barang Dibeli</th>
                  <th>Harga Total</th>
                  <th>Status</th>
                  <th>Tanggal Pembelian</th>
                  <th>Cetak</th>
                </tr>
            </thead>
            <tbody>
            <?php
            $iduser = $ruser['id_user'];
              $perpage = 20; //per halaman
              $page = isset($_GET["hal"]) ? (int)$_GET["hal"] : 1;
              $start = ($page > 1) ? ($page * $perpage) - $perpage :0;
              $articles = sprintf("SELECT * FROM transaksi INNER JOIN kurir ON transaksi.id_kurir = kurir.id_kurir WHERE id_user=%s ORDER BY `transaksi`.`id_transaksi` DESC",$iduser);
              $limit= $articles." LIMIT $start, $perpage";
              $rtrans= $koneksi->query($articles);
              $tampil = mysqli_query($koneksi, $limit);
              $total = mysqli_num_rows($rtrans);
              $pages = ceil($total/$perpage);
            $no = $start;
            while ($row = $tampil->fetch_assoc()) {
              $maria=$koneksi->query("SELECT * FROM saldo WHERE id_transaksi='".$row['id_transaksi']."'") or die(get_error());
              $ozawa=$maria->fetch_assoc();
              $kimochi=$ozawa['saldo']; 
              $total = $row['total'];
              $kurir = $row['harga_kurir'];
              $kutol = $total+$kurir+$kimochi;
              $no++;
            ?>
                <tr>
                  <th scope="row"><?php echo $no;?></th>
                  <td><a href="javascript:void();" class="btn btn-primary btn-sm" onclick="lihat_barang(<?php echo $row['id_transaksi'];?>);">Lihat Semua Barang</a></td>
                  <td><?php echo $kutol;?></td>
                  <td id="status<?php echo $row['id_transaksi'];?>">
                      <?php 
                        if ($row['status']=='belum dibayar') {
                      ?>
                    <a href="pembayaran?id_transaksi=<?php echo $row['id_transaksi'];?>"><?php echo $row['status'];?></a>
                    <?php }else echo $row['status']; ?>
                  </td>
                  <td><?php echo $row['tanggal_transaksi'];?></td>
                  <td><a target='_blank' href='print.php?id=<?php echo $row['id_transaksi'];?>' class="btn btn-secondary btn-sm"  onclick="lihat_bukti(<?php echo $row['id_transaksi'];?>);">Cetak Bukti Transaksi</a></td> 

                  
                </tr>
            <?php } ?>
            </tbody>
          </table>
            <div class="col-lg-12 <?php if($pages<=1){echo "d-none";} ?>">
                <nav aria-label="Page navigation example">
                    <ul class="pagination justify-content-end">
                    <?php
                    if($page>1){                        
                              $link_prev = "?hal=".($page-1);
                            }else{
                                $link_prev='#';
                            }?>
                        <li class="page-item">
                            <a class="page-link" href="<?php echo $link_prev;?>">
                                <span aria-hidden="true">
                                    «
                                </span>
                                <span class="sr-only">
                                    Previous
                                </span>
                            </a>
                        </li>
                        <?php for ($i=1; $i<=$pages; $i++) { 
                            if($page==$i){
                                $active = "active";
                            }else{
                                $active = "";
                            }

                                ?>
                        <li class="page-item <?php echo $active ?>">
                            <a class="page-link" href="?hal=<?php echo $i;?>">
                                <?php echo $i; ?>
                            </a>
                        </li>
                        <?php }
                  if($page<$pages){
                    $link_next = "?hal=".($page+1);
                        }else{
                          $link_next='#';
                      }?>
                        <li class="page-item">
                            <a class="page-link" href="<?php echo $link_next;?>">
                                <span aria-hidden="true">
                                    »
                                </span>
                                <span class="sr-only">
                                    Next
                                </span>
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>          
    </div>
  </div>
    
</div>
</div>

</div>
<!-- Button trigger modal -->
<!-- Modal -->
<div class="modal fade" id="modalbarang" tabindex="-1" role="dialog" aria-labelledby="bukti" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Semua barang Yang Dibeli</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="modal_barang_body">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>




<?php
require_once '../footer.php'; 
?>
<script type="text/javascript">
  $(document).ready(function(){
    $('.carousel').carousel();
  });

  function lihat_barang(id){
      $('#modal_barang_body').load('liba.php?lihat_barang=&id='+id);
      $('#modalbarang').modal('show');
  }
  //function lihat_bukti(id){
  //  $('#id_foto').val(id);

   //   $('#modal_barang_bukti').load('libu.php?lihat_bukti=&id='+id);
      //$('#modalbarangbukti').modal('show');
  // }
  
  $('#myModal').on('shown.bs.modal', function () {
  $('#myInput').trigger('focus');
  })
</script>

</body>
</html>
<?php } ?>