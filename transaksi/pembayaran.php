<?php 
require_once '../koneksi/koneksi.php';
$ruser = chk_login();
$litrans=0;
if(isset($_GET['id_transaksi'])&&$_GET['id_transaksi']!=null){
  $qtransaksi = $koneksi->query(
              sprintf("SELECT * FROM transaksi t INNER JOIN users u ON t.id_user = u.id_user INNER JOIN rekening_bank rb ON t.id_bank = rb.id_bank INNER JOIN kota_kab kk ON t.id_kota_kab = kk.id_kota_kab INNER JOIN provinsi p ON kk.id_provinsi = p.id_provinsi INNER JOIN kurir k ON t.id_kurir=k.id_kurir WHERE t.id_transaksi = %s AND t.id_user = %s AND status = 1",$_GET['id_transaksi'],$ruser['id_user']));
  $litrans = $qtransaksi->num_rows;
}
if(isset($_POST['form'])){

  switch ($_POST['form']) {
    case 'delete_keranjang':
      foreach ($_SESSION['user']['keranjang'] as $key => $value) {
        if($_POST['id'] == $value['id']){
          $koneksi->query(sprintf("DELETE FROM keranjang where token = '%s'",$value['token']))or die(get_error());
          unset($_SESSION['user']['keranjang'][$key]);
        }
      }
      $jumlah = get_totals();
      if(isset($_POST['kurir']) && $_POST['kurir']!=null){
        $gkur = $koneksi->query("SELECT * FROM kurir WHERE id_kurir = ".$_POST['kurir']);
        $rkur = $gkur->fetch_assoc();
        $jumlah +=$rkur['harga_kurir'];
      }
      if(count($_SESSION['user']['keranjang'])==0){
        $qtransaksi = mysqli_query($koneksi,"DELETE FROM TRANSAKSI WHERE id_transaksi = ".$_SESSION['user']['id_transaksi'])or die(get_error());
        unset($_SESSION['user']['id_transaksi']);
      }
          echo json_encode("Rp. ".number_format($jumlah));
      break;
    case 'inp_data':
      //header("Content-Type: application/json");
      //print_r($_POST);
      $qq= $koneksi->query("SELECT id_transaksi FROM transaksi ");
      $tottrans = $qq->num_rows+1;
      $iid = "";
      if($tottrans<10){
        $iid = "0000000".$tottrans;
      }else if($tottrans<100){
        $iid = "000000".$tottrans;
      }else if($tottrans<1000){
        $iid = "00000".$tottrans;
      }else if($tottrans<10000){
        $iid = "0000".$tottrans;
      }
      $kode = "INV/".date('Ymd')."/".$iid;
        $data = sprintf("INSERT INTO `transaksi`(`id_user`, `id_kota_kab`, `id_bank`, `id_kurir`, `penerima_nama`, `penerima_telp`, `penerima_kode_pos`, `penerima_alamat`, `penerima_email`, `total`, `status`, `resi`, `kode_transaksi`) values ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','belum dibayar','%s','%s')",$ruser['id_user'],$_POST['kota_kab'],$_POST['bank'],$_POST['tykurir'],$_POST['fname'],$_POST['telp'],$_POST['kode_pos'],$_POST['alamat'],$_POST['email'],get_totals(),'0',$kode);
        $qdata = mysqli_query($koneksi,$data) or die(get_error());
        if($qdata){ 
          $id_transaksi = $koneksi->insert_id;
          $qdata2 =  "";
          $data2 = 0;

          foreach ($_SESSION['user']['keranjang'] as $key => $value) {
            if($qdiskon->num_rows){
              $rdiskon = $qdiskon->fetch_assoc();
              $id_diskon = $rdiskon['ID'];
            }
            $qbarang = $koneksi->query("SELECT * FROM barang WHERE id_barang = ".$value['id']);
            $rbarang = $qbarang->fetch_object();
            $qdata2= sprintf("INSERT INTO `transaksi_barang`(`id_transaksi`, `id_barang`, `jumlah`,`harga`) VALUES (%s,%s,%s,%s)",$id_transaksi,$rbarang->id_barang,$value['jumlah'],$rbarang->harga_barang);
            $data2 .= mysqli_query($koneksi,$qdata2) or die(get_error());
            $id_tranbar = $koneksi->insert_id;

              if(isset($_SESSION['user']['login']['reseller']) && $_SESSION['user']['login']['reseller']!=null){
                $reseller = $_SESSION['user']['login']['reseller'];
                $qdiskon = $koneksi->query("SELECT * FROM diskon d join diskon_reseller dr on d.id_diskon = dr.id_diskon where id_reseller = $reseller and id_barang = ".$value['id']." AND status = 1 AND qty <= ".$value['jumlah']." ORDER BY qty DESC limit 1");
                if($qdiskon->num_rows!=0){
                  
                  while ($rdiskon = $qdiskon->fetch_assoc()) {
                  $data3 =$koneksi->query(sprintf("INSERT INTO `transaksi_diskon`(`id_tranbar`, `id_diskon`,`diskon`) VALUES (%s,%s,%s)",$id_tranbar,$rdiskon['id_diskon'],$rdiskon['diskon']))or die(get_error());
                  }
                }

              }
                $qdiskon2 = $koneksi->query("SELECT * FROM diskon d where jenis !='reseller' AND id_barang = ".$value['id']." AND status = 1 AND qty <= ".$value['jumlah']." ORDER BY qty DESC limit 1" );
                             if($qdiskon2->num_rows!=0){ 
                 
                              while ( $rdiskon2 = $qdiskon2->fetch_assoc()) {
                  $data3 =$koneksi->query(sprintf("INSERT INTO `transaksi_diskon`(`id_tranbar`, `id_diskon`,`diskon`) VALUES (%s,%s,%s)",$id_tranbar,$rdiskon2['id_diskon'],$rdiskon2['diskon']))or die(get_error());
                              }
                  }

              }

            $qbarang=mysqli_query($koneksi,"SELECT * FROM barang b INNER JOIN transaksi_barang tb on b.id_barang = tb.id_barang  WHERE b.id_barang = '".$value['id']."'") or die(get_error());
            $rbarang=mysqli_fetch_assoc($qbarang);
            $tbg = $rbarang['stok_barang']-$value['jumlah'];
            if($tbg>-1){
              mysqli_query($koneksi,"UPDATE barang set stok_barang='".$tbg."' where id_barang = ".$value['id']) or die(get_error());
            }
          }
        
        $idu = $ruser['id_user'];
        $notift = $koneksi->query("INSERT INTO `notif_trans` (`id_notif`, `id_user`, `id_transaksi`, `status`) VALUES (NULL, '$idu', '$id_transaksi', '0');");
        //$_SESSION['user']['id_transaksi'] = mysqli_insert_id($koneksi);
        if($qdata&&$data){
           /*$filename=md5(mysqli_insert_id($koneksi)).".doc";
            header("content-disposition:attachment;filename='$filename' ");
            header("content-type:application/vdn.ms.word");*/
            $dataa = "";
            foreach ($_SESSION['user']['keranjang'] as $key => $value) {
              $qbarang=mysqli_query($koneksi,sprintf("SELECT * FROM barang b INNER JOIN transaksi_barang tb on b.id_barang = tb.id_barang WHERE id_transaksi = %s AND b.id_barang = %s", $id_transaksi,$value['id']));
              $rbarang=mysqli_fetch_assoc($qbarang);
              $qdiskon=$koneksi->query("SELECT * FROM transaksi_diskon td INNER JOIN diskon d on td.id_diskon = d.id_diskon WHERE `id_tranbar` = ".$rbarang['id_tranbar']);
              $diskon = "";
              $todis=0;
              $subtotal=$rbarang['harga_barang']*$rbarang['jumlah'];
              $x = 0;
              while ($rdiskon = $qdiskon->fetch_assoc()) {
                $diskon .=($x>0?"+":"").$rdiskon['diskon']."%";
                $todis += $rdiskon['diskon'];              
                $subtotal -=$rbarang['harga_barang']*$rdiskon['diskon']/100*$rbarang['jumlah'];
                $x++;
              }
              if($diskon!=null){
                $diskon = "<tr><td>Diskon</td><td>: ".$diskon."</td></tr>";

              }else{
                $diskon="";
              }
              $dataa .= "<div class='col-sm-6 col-lg-4 p-1'><table style='border-radius: 3px; width: 100%; height: 100%'><tr><td class='judulbarang' colspan='2' style='border-radius: 3px;background-color: #ffc107'>".$rbarang['nama_barang']."</td></tr><tr><td width='25%' style='padding: 5px;'><img class='card-img-top' src='".$rbarang['foto_barang']."'style='height:100px; width:100px'></img></td><td style=''><table style='text-align: left'><tr><td>Harga </td><td>: Rp. ".number_format($rbarang['harga_barang'])."</td></tr><tr><td>Jumlah</td><td>: ".$rbarang['jumlah']."</td></tr>".$diskon."<tr><td>Subtotal</td><td>: <b>Rp. ".number_format($subtotal)."</b></td></tr></table></tr></table></div>";
                $_SESSION['user']['keranjang']=null;
                unset($_SESSION['user']['keranjang']);
                mysqli_query($koneksi,"DELETE FROM keranjang where id_user = ".$ruser['id_user']);
                $ket = "User ".$ruser['username']." Telah membeli barang sebanyak ".$value['jumlah'];
                $koneksi->query( sprintf( "INSERT INTO `histori_mbarang`(`id_barang`, `jenis`, `keterangan`) VALUES ('%s','pengeluaran','%s')", $value['id'], $ket ) )OR die( $koneksi->error );

            }
             // $qtransaksi = mysqli_query($koneksi, "SELECT * FROM transaksi t join users u ON t.id_user = u.id_user join kota_kab kk ON t.id_kota_kab = kk.id_kota_kab join provinsi p ON kk.id_provinsi = p.id_provinsi join rekening_bank rb ON t.id_bank = rb.id_bank where id_transaksi = 1" );
              $qtransaksi = mysqli_query($koneksi,sprintf("SELECT * FROM transaksi t INNER JOIN users u ON t.id_user = u.id_user INNER JOIN rekening_bank rb ON t.id_bank = rb.id_bank INNER JOIN kota_kab kk ON t.id_kota_kab = kk.id_kota_kab INNER JOIN provinsi p ON kk.id_provinsi = p.id_provinsi INNER JOIN kurir k ON t.id_kurir=k.id_kurir WHERE id_transaksi = ".$id_transaksi));
              $rtransaksi = mysqli_fetch_array($qtransaksi);
              $idnama= $ruser['id_user'];
              $qnama = mysqli_query($koneksi,"SELECT * FROM users WHERE id_user='$idnama'");
              $rownama = mysqli_fetch_assoc($qnama);
              $no=0;
              $trantv=$koneksi->query("SELECT * FROM transaksi_barang INNER JOIN transaksi ON transaksi_barang.id_transaksi = transaksi.id_transaksi INNER JOIN barang ON transaksi_barang.id_barang = barang.id_barang INNER JOIN users ON transaksi.id_user = users.id_user INNER JOIN kurir ON transaksi.id_kurir = kurir.id_kurir WHERE transaksi.id_transaksi = '$id_transaksi'")or die(get_error());
              $transaksi1 = $koneksi->query("SELECT * FROM transaksi WHERE id_transaksi=".$id_transaksi);
              $rtrans1 = $transaksi1->fetch_assoc();
              $date = new DateTime($rtrans1['tanggal_transaksi']);
              include "../classes/class.phpmailer.php";
              $mail = new PHPMailer;
              $mail->IsSMTP();
              $mail->SMTPSecure = 'ssl';
              $mail->Host = "mail.blilistrik.com"; //hostname masing-masing provider email
              $mail->SMTPDebug = 0;
              $mail->Port = 465;
              $mail->SMTPAuth = true;
              $mail->Username = "admin@blilistrik.com"; //user email
              $mail->Password = "blilistrik01"; //password email
              $mail->SetFrom("admin@blilistrik.com","admin Blilistrik"); //set email pengirim
              $mail->Subject = "Invoice Blilistrik"; //judul email
              $mail->AddAddress($_POST['email']); //tujuan email
              $a="" ;
              while ($rowww = mysqli_fetch_array($trantv)){
                $no++;
              $diskon = "";
              $todis=0;
              $subtotal=$rowww['harga_barang']*$rowww['jumlah'];
              $x = 0;
              $qdiskon=$koneksi->query("SELECT * FROM transaksi_diskon td INNER JOIN diskon d on td.id_diskon = d.id_diskon WHERE `id_tranbar` = ".$rowww['id_tranbar']);
              while ($rdiskon = $qdiskon->fetch_assoc()) {
                  $diskon .=($x>0?"+":"").$rdiskon['diskon']."%";
                  $todis += $rdiskon['diskon'];              
                  $subtotal -=$rowww['harga_barang']*$rdiskon['diskon']/100*$rowww['jumlah'];
                  $x++;
                }
           $totall = $rowww['id_transaksi'];
            if($todis==0){$diskon="-";} 
            $a.="
            <tr>
          <th style='font-weight: bold;width: 50px;text-align: center; vertical-align: top;'>".$no."</th>
          <th style='width: 270px;font-weight: bold;text-align: center;vertical-align: top;'>".$rowww['nama_barang']."</th>
          <th style='font-weight: bold;width: 150px;text-align: center;vertical-align: top;'>".$rowww['jumlah']."</th>
          <th style='width: 70px;font-weight: bold;text-align: center;vertical-align: top;'>".$diskon."</th>
          <th style='width: 70px;font-weight: bold;text-align: center;vertical-align: top;'>Rp.".number_format($rowww['harga_barang'])."</th>
          <th style='width: 70px;text-align: center;font-weight: bold;vertical-align: top;'>Rp.".$subtotal."</th>
        </tr>
              ";
        $total = $rowww['total'];
        $kurir = $rowww['harga_kurir'];
        $kutol = $total+$kurir;
            }
          $mail->MsgHTML("<p>Bukti Invoice Bapak/Ibu ".$rownama['fname']." pada tanggal ".$date->format("d F Y")." senilai seluruh Rp.".number_format($kutol)."<br><br>adalah</p>
        <div style='margin-left:unset;margin-right:unset;'>
          <table  width='auto' height='auto' border='1px'>
            <tr>
              <th style='background: red !important;text-align: center;border-bottom: 2px solid rgb(0, 0, 0) !important;color: white !important;' colspan='6' height='40px'>Detail Barang</th>
            </tr>
            <tr><th style='font-weight: bold;width: 50px;text-align: center; vertical-align: top;'>No</th>
              <th style='width: 270px;font-weight: bold;text-align: center;vertical-align: top;'>Nama Barang</th>
              <th style='font-weight: bold;width: 150px;text-align: center;vertical-align: top;'>Jumlah Barang</th>
              <th style='width: 70px;font-weight: bold;text-align: center;vertical-align: top;'>diskon</th>
              <th style='width: 70px;font-weight: bold;text-align: center;vertical-align: top;'>Harga</th>
              <th style='width: 70px;text-align: center;font-weight: bold;vertical-align: top;'>Jumlah</th>
            </tr>
            ".$a."
            <tr>
              <th colspan='4' style='font-weight: bold;text-align: center;'></th>
              <th style='font-weight: bold;width: 70px;text-align: center;'>Kurir</th>
              <th style='font-weight: bold;width: 70px;text-align: center;'>Rp.".$kurir."</th>
            </tr>   
            <tr>
              <th colspan='4' style='font-weight: bold;text-align: center;'></th>
              <th style='font-weight: bold;width: 70px;text-align: center;'>Total</th>
              <th style='font-weight: bold;width: 70px;text-align: center;'>Rp.".number_format($kutol)."</th>
            </tr>
            <tr>
              <th colspan='2' style='font-weight: bold;text-align: left;'>Status</th>
              <th colspan='4' style='font-weight: bold;width: 70px;text-align: center;'>".$rtrans1['status']."</th>
            </tr>   
          </table>
            <div><p>Terima Kasih telah datang dan memesan produk dari situs website blilistrik kami. Atas nama kami <strong>Admin Blilistrik</strong> Terima Kasih</p></div>
            </div><br> PESAN NO-REPLY");
              $date=get_paytick($id_transaksi);
              if($date['sec']<0){
                  $datee="Waktu Telah Berakhir";
                }else{
                  $datee="Sisa waktu ".$date['hour']." jam ".$date['min']." menit ".$date['sec']." Detik ." ;
                }
            $pass="012345678909876543210"; 
            $panjang='3'; 
            $len=strlen($pass); 
            $start=$len-$panjang; 
            $xx=rand('0',$start); 
            $yy=str_shuffle($pass); 
            $angkaunik=substr($yy, $xx, $panjang);
            $queryq = $koneksi->query("INSERT INTO `saldo` (`id_saldo`, `id_transaksi`, `saldo`) VALUES (NULL, '".$rtransaksi['id_transaksi']."', '".$angkaunik."')") or die(get_error()); 
              
              $jumkir = $rtransaksi['harga_kurir']+$rtransaksi['total']+$angkaunik;
              $data = "<div class='row'>
            <div class='col-lg-12 mb-4'>
                <span><h3>Batas Waktu Penjualan</h3><hr class='border-bottom border-dark '/></span>
            </div>
                <div class='col-lg-12 mb-4'>
                    <div class='row'>
                        <div id='paytick' class='col-12' style='font-size: 3rem; color: #ffc107; font-family: monospace;    font-weight: 100 !important'>".$datee."</div>
                    </div>
                </div>
              
                        <div class='col-lg-12 mb-4 p-0'><span><h3>Detail Pesanan</h3><hr class='border-bottom border-dark '/></span>
                        </div>
                        <div class='col-lg-12'><div class='row'>".$dataa."</div></div>

                        <div class='col-sm-6 mb-4 text-left'>
                            <span class='text-center'><h3>Rekening Toko</h3><hr class='border-bottom border-dark '/>
                            </span>".$rtransaksi['nama_bank']."<br>".$rtransaksi['atas_nama']."<br>".$rtransaksi['no_rekening']."<br>".$rtransaksi['cabang']."<br>
                            <div class='text-danger text-capitalize text-center mt-4'>Segera transfer ke nomer rekening tersebut.</div>
                        </div>
                        <div class='col-sm-6 mb-4 text-left'><span class='text-center'><h3>Data Penerima</h3><hr class='border-bottom border-dark '/></span>".$rtransaksi['penerima_nama']."<br>".$rtransaksi['penerima_telp']."<br>".$rtransaksi['penerima_email']."<br>".$rtransaksi['penerima_alamat']."<br>".$rtransaksi['nama_provinsi']."<br>".$rtransaksi['nama_kota_kab']."<br>".$rtransaksi['penerima_kode_pos']."<br>
                        </div>
                        <div class='col-sm-6 mb-4 text-left'><span class='text-center'><h3>Kurir</h3><hr class='border-bottom border-dark '/></span>".$rtransaksi['nama_kurir']."<br>".$rtransaksi['tipe']."<br>Rp. ".number_format($rtransaksi['harga_kurir'])."
                        </div>
                        <div class='col-sm-6 mb-4 text-left'><span class='text-center'><h3>Total Seluruh</h3><hr class='border-bottom border-dark '/></span><span class='text-center'><p class='mb-0'>Rp. ".number_format($jumkir)."</p></span><br>
                        <a href='javascript:void();' class='btn btn-secondary btn-sm w-100 '  onclick='lihat_bukti(".$rtransaksi['id_transaksi'].");'>Foto Bukti Transfer</a>
                        </div>
              
        </div>
                    <form method='post'><div class='button px-3 '><a target='_blank' href='print.php?id=".$rtransaksi['id_transaksi']."' class='btn btn-block action-button mx-0' style='width:100%; '>Print</a></div></form>";
                    $smtp = fsockopen('smtp host',25);
                    if($smtp===false){
                      $res = fread($f, 1024);
                      if(strlen($res)>0 && strpos($res, '220') === 0){
                        $mail->Send();}else{
                      //$data .="<i style='color:red; font-size: 8px;'>email tidak terkirim</i>";
                        }
                    }else{
                    }
              echo json_encode(Array('html'=>$data, 'id'=>$id_transaksi));
              //echo $data;
                  }
      break;
    case 'get_kota_kab':
      $data = "<option>Silakan Pilih Kota/Kab</option>";
      $qkot= mysqli_query($koneksi,"SELECT * FROM kota_kab WHERE id_provinsi = ".$_POST['id']);
            while ($rkot=mysqli_fetch_assoc($qkot)) {
              if($rkot['id_kota_kab'] == $rsel['id_kota_kab']){
                $selected = "SELECTED";
              }else{
                $selected = "";
              }
              $data .= "<option value='".$rkot['id_kota_kab']."' $selected>".$rkot['nama_kota_kab']."</option>";
            }
      echo json_encode($data);
      break;
    case 'get_jenkur':
      $data = "<option>Silakan pilih jenis kurir</option>";
      $qkur= mysqli_query($koneksi,"SELECT * FROM kurir WHERE nama_kurir LIKE '".$_POST['id']."'");
      while ($rkur=mysqli_fetch_assoc($qkur)) {
        if($rkur['tipe'] == $rsel['tipe']){
          $selected = "SELECTED";
        }else{
          $selected = "";
        }
        $data .= "<option value='".$rkur['id_kurir']."' $selected>".$rkur['tipe']."</option>";
      }
        echo json_encode($data); 
      break;
    case 'get_kurir':
      $gkur = $koneksi->query("SELECT * FROM kurir WHERE id_kurir = ".$_POST['id']);
      $rkur = $gkur->fetch_assoc();
      $data = array('harga_kurir'=>"Rp. ".number_format($rkur['harga_kurir']),'totals'=>"Rp. ".number_format(get_totals()+$rkur['harga_kurir']));
      $kurir = $_POST['id'];
      echo json_encode($data);
      break;
    case 'get_paytick':
      $date=get_paytick($_POST['id']);
      if($date['date']<0){
        echo json_encode(null);
      }else{
        $hou = ($date['hour']<10) ? "0".$date['hour'] : $date['hour'];
        $min = ($date['min']<10) ? "0".$date['min'] : $date['min'];
        $sec = ($date['sec']<10) ? "0".$date['sec'] : $date['sec'];
        echo json_encode("Sisa waktu ".$hou." jam ".$min." menit ".$sec." detik.") ;
      }

      break;
  } 
}else{
if(!isset($_SESSION['user']['keranjang']) || $_SESSION['user']['keranjang']==null  AND !$litrans ){
  echo "<script>alert('Anda belum memiliki barang dalam keranjang');window.location='".$root_base."';</script> ";
}
?>
<!DOCTYPE html>
<html>
<head>
  <meta content='####################' name='google-site-verification'/>
  <meta content='####################' name='msvalidate.01'/>
  <meta content='####################' name='alexaVerifyID'/>
  <meta content='index, follow, noodp, noydir' name='robots'/>
  <meta content='id' name='geo.country'/>
  <meta content='TMD' name='author'/>
  <meta content='1 days' name='revisit-after'/>
  <meta content='Indonesia' name='geo.placename'/>
  <meta content='ecommerce' name='generator'/>
  <meta content='general' name='rating'/>
  <meta content='index, follow, snipet' name='googlebot'/>
  <meta content='id_id' property='og:locale'/>
  <meta content='website' property='og:type'/>
  <meta content='Selamat Datang Di Blilistrik' name='keywords'/>
  <meta name="google-site-verification" content="googleb8be6259d98e95ac"/>
  <meta name="Description" CONTENT="Selamat Datang di Blilistrik, Situs yang menjual berbagai macam barang atau produk elektronik sesuai kebutuhan, jika ada kesulitan tolong hubungi pihak blilistrik">
  <link rel="icon" href="<?php echo $root_base?>img/logo/lobli1.png">
  <meta name="viewport" content="width=device-width, initial-scale=1.0 shrink-to-fit=no"/>
  <meta charset="utf-8" name="theme-color" content="#ffc107">
    <link href="<?php echo $root_base?>assets/css/bootstrap.css" rel="stylesheet" media="screen,projection"/>
    <link href="<?php echo $root_base?>assets/fonts/material-icons.css" rel="stylesheet"/>
    <title>blilistrik</title>
</head>
<style type="text/css">
  /*@import url(https://fonts.googleapis.com/css?family=Montserrat);*/
  * {
      margin: 0;
      padding: 0;
  }

  html {
      height: 100%;

  }

  body {
      font-family: montserrat, arial, verdana;
  background: #f5f5f5;
  }
  .bg-kuningdop{
      background-color: #ffc107;
    }
  .btn-biru {
      color: #fff;
      background-color: #3f51b5;
      border-color: #fff;
    }
    #progressbar {
    padding: 10px;
    overflow: hidden;
    /*CSS counters to number the steps*/
    counter-reset: step;
  }

  #progressbar li {
      list-style-type: none;
      color: black;
      text-transform: uppercase;
      font-size: 9px;
      width: 33.33%;
      float: left;
      position: relative;
      letter-spacing: 1px;

  }

  #progressbar li:before {
      content: counter(step);
      counter-increment: step;
      width: 24px;
      height: 24px;
      line-height: 26px;
      display: block;
      font-size: 12px;
      color: #333;
      background: white;
      border-radius: 25px;
      margin: 0 auto 10px auto;
  }

  /*progressbar connectors*/
  #progressbar li:after {
      content: '';
      width: 100%;
      height: 2px;
      background: black;
      position: absolute;
      left: -50%;
      top: 9px;
      z-index: -1; /*put it behind the numbers*/
  }

  #progressbar li:first-child:after {
      /*connector not needed before the first step*/
      content: none;
  }

  /*marking active/completed steps green*/
  /*The number of the step and the connector before it = green*/
  #progressbar li.active:before, #progressbar li.active:after {
      background: #ee0979;
      color: white;
  }
  .list-group-item{
    padding: 0px;
    background-color: transparent;
    border: transparent;
  }
  #content{
    margin-top: 170px;
  }

  #msform {
      text-align: center;
      position: relative;
      width: 100%;
      margin: auto;
      margin-bottom:50px; 
      overflow-x: hidden;
  }

  #msform fieldset {
      background: white;
      border: 0 none;
      border-radius: 0px;
      box-shadow: 0 0 15px 1px rgba(0, 0, 0, 0.4);
      padding: 20px 30px;
      box-sizing: border-box;
      width: 80%;
      margin: 0 10%;
      margin-bottom: 50px;

      /*stacking fieldsets above each other*/
      position: relative;
  }
  #msform fieldset:not(:first-of-type) {
      display: none;
  }

  #msform input, #msform textarea {
      padding: 15px;
      border: 1px solid #ccc;
      border-radius: 0px;
      margin-bottom: 10px;
      width: 100%;
      box-sizing: border-box;
      font-family: montserrat;
      color: #2C3E50;
      font-size: 13px;
  }

  #msform input:focus, #msform textarea:focus {
      -moz-box-shadow: none !important;
      -webkit-box-shadow: none !important;
      box-shadow: none !important;
      border: 1px solid #ee0979;
      outline-width: 0;
      transition: All 0.5s ease-in;
      -webkit-transition: All 0.5s ease-in;
      -moz-transition: All 0.5s ease-in;
      -o-transition: All 0.5s ease-in;
  }

  /*buttons*/
  #msform .action-button {
      width: 100px;
      background: #ee0979;
      font-weight: bold;
      color: white;
      border: 0 none;
      border-radius: 25px;
      cursor: pointer;
      padding: 10px 5px;
      margin: 10px 5px;
  }

  #msform .action-button:hover, #msform .action-button:focus {
      box-shadow: 0 0 0 2px white, 0 0 0 3px #ee0979;
  }

  #msform .action-button-previous {
      width: 100px;
      background: #C5C5F1;
      font-weight: bold;
      color: white;
      border: 0 none;
      border-radius: 25px;
      cursor: pointer;
      padding: 10px 5px;
      margin: 10px 5px;
  }

  #msform .action-button-previous:hover, #msform .action-button-previous:focus {
      box-shadow: 0 0 0 2px white, 0 0 0 3px #C5C5F1;
  }
  .fs-subtitle{
    font-size: 10px;
    color: red;
    text-align: right;
  }
    .hig{
    height: 100px;
  }

  @media(max-width: 767px){
    #content{
      margin-top: 100px;
    }
    #msform fieldset {
      background: transparent;
      box-shadow: 0 0 0 0 transparent;
      width: 100%;
      margin: 0px;
      padding: 0px;
      /*stacking fieldsets above each other*/
      position: relative;
  }
  .hig{
    height: unset;
  }
  .button{
    position: fixed;
    bottom: 0px;
    left: 0px;
    right: 0px;
  }
  .next{
  float:right;
  }
  .previous{
    float:left;
  }
  }
</style>
<body>
  <header>
    <nav class="navbar navbar-expand-lg navbar-light text-white bg-kuningdop fixed-top p-0 hig">
        <div class="container-fluid">
          <div>
            <a href="<?php echo $_SESSION['halaman']; ?>" onclick="javascript:history.back()"><i class="material-icons waves-effect waves-circle waves-light text-white" >arrow_back</i></a>
          </div>
            <div class="w-75 list-group ">
                  <ul id="progressbar" class="list-group-item text-center">
                      <li class="active">Isi Data Pengirim</li>
                      <li>Pilih Pembayaran</li>
                      <li>Invoice</li>
                  </ul>
            </div>
            <div>
              <a href="<?php echo $root_base ?>"><i class="material-icons waves-effect waves-circle waves-light text-white" >close</i></a>
            </div>
        </div>
    </nav>
  </header>
  <!-- MultiStep Form -->
  <div class="container-fluid" id="content"> 
    <form id="msform" method="post">
      <input type="hidden" name="form" value="inp_data">
      <?php if(!$litrans){?>
        <fieldset>
          <div class="fs-subtitle">Masukan Data Yang Valid*</div>
          <div class="row">
          <div class="col-lg-12">
            <div class="row">
          <div class="form-group col-lg-4">
            <label for="fname" class="float-left">Nama Penerima*</label>
            <input class="form-control" name="fname" id="fname" type="text" placeholder="Nama Panjang" required value="<?php echo $ruser['fname'] ?>">
          </div>
           <div class="form-group col-lg-4">
               <label for="telp" class="float-left">No Telp Penerima</label>
               <input type="text" name="telp" id="telp" class="form-control" placeholder="08xxxxxxxxx" required value="<?php echo $ruser['telp'] ?>">
           </div>
          <div class="form-group col-lg-4">
            <label for="kode_pos" class="float-left">Kode Pos*</label>
            <input class="form-control" name="kode_pos" id="kode_pos" type="text" placeholder="Kode pos" required value="<?php echo $ruser['kode_pos'] ?>">
          </div>
          <?php 
            $qsel = mysqli_query($koneksi,"SELECT p.id_provinsi AS id_provinsi, id_kota_kab FROM provinsi p join kota_kab k on p.id_provinsi=k.id_provinsi where id_kota_kab =".$ruser['id_kota_kab']);
              $rsel = mysqli_fetch_assoc($qsel);
              $qprov = mysqli_query($koneksi,"SELECT * FROM provinsi");
              $qkot = mysqli_query($koneksi,"SELECT * FROM kota_kab where id_provinsi=".$rsel['id_provinsi']);
          ?>
          <div class="form-group col-lg-4">
              <label for="provinsi" class="float-left">Provinsi*</label>
            <select type="text" name="provinsi" id="provinsi" class="form-control" placeholder="Provinsi" required value="<?php echo $ruser['fname'] ?>">
              <option required value="">Pilih Provinsi</option>
              <?php 
              while ($rprov=mysqli_fetch_assoc($qprov)) {
                if($rprov['id_provinsi'] == $rsel['id_provinsi']){
                  $selected = "SELECTED";
                }else{
                  $selected = "";
                }
                echo "<option value='".$rprov['id_provinsi']."' $selected>".$rprov['nama_provinsi']."</option>";
              }
              ?>
            </select>
          </div>
          <div class="form-group col-lg-4">
              <label for="kota_kab" class="float-left">Kota atau Kabupaten*</label>
              <select type="text" name="kota_kab" id="kota_kab" class="form-control" placeholder="kota_kab" required value="<?php echo $ruser['fname'] ?>">
              <option required value="">Pilih Kota/Kabupaten</option>
              <?php 
              while ($rkot=mysqli_fetch_assoc($qkot)) {
                if($rkot['id_kota_kab'] == $rsel['id_kota_kab']){
                  $selected = "SELECTED";
                }else{
                  $selected = "";
                }
                echo "<option value='".$rkot['id_kota_kab']."' $selected>".$rkot['nama_kota_kab']."</option>";
              }
              ?>
          </select>
            </div>
             <div class="form-group col-lg-6">
                 <label for="alamat" class="float-left">Alamat Lengkap*</label>
                 <textarea type="text" name="alamat" id="alamat" class="form-control" placeholder="alamat" required><?php echo $ruser['alamat'] ?></textarea>
             </div>
             <div class="form-group col-lg-6">
                 <label for="email" class="float-left">Email (Email aktif)*</label>
                 <input type="text" name="email" id="email" class="form-control" placeholder="Contoh@contoh.com" required value="<?php echo $ruser['email'] ?>">
             </div>
             </div>
             </div>
             </div>
             <div class="button">
          <input type="button" class="next action-button" id="next0" value="Next"/>
          </div>
        </fieldset>
        <fieldset>
          <div class="fs-subtitle">Masukan Data Yang Valid*</div>  
          <div class="row">
            <div class="col-lg-12 mb-4 p-0">
              <span ><b>Pesanan</b><hr class="border-bottom border-dark "></span>
            </div>          
            <div class="col-lg-12">
              <div class="row bg-white rounded"> 
                <?php 
                $total = 0;
                $banyak = 0;
                foreach ($_SESSION['user']['keranjang'] as $key => $value) {
                  $banyak ++;
                  $qbarang = mysqli_query($koneksi,"SELECT * FROM barang WHERE id_barang = ".$value['id']);
                  $rbarang = mysqli_fetch_assoc($qbarang);
                ?> 
                <div class="col-lg-6 my-1" style="border: 1px solid #ccc;">
                  <div class="row">
                    <div class="col-3 p-0" >
                      <img class="img-fluid" src="<?php echo $rbarang['foto_barang'];?>" alt="Card image cap" style="height: 100px;width: auto;max-width: 100%">
                    </div>
                    <div class="col-7 col-lg-6 text-left">
                      <span class="judulbarang text-justify"><?php echo $rbarang['nama_barang'] ?></span><br>
                      <span>Harga : Rp. <?php echo number_format($rbarang['harga_barang']) ?></span><br>
                      <span>Jumlah :<?php echo $value['jumlah'] ?>
                      </span><br>
                      <span>
                        <?php 
                        $diskon = "Diskon : ";
                        $x=0;
                        if(get_diskon($value['id'],$_SESSION['user']['keranjang'][$key]['jumlah'],"umum")){
                          $diskon .= "<i class='diskon_umum'>".($x>0?"+":"").get_diskon($value['id'],$_SESSION['user']['keranjang'][$key]['jumlah'],"umum")."%</i>";$x++;

                        }
                        if (get_diskon($value['id'],$_SESSION['user']['keranjang'][$key]['jumlah'],"reseller")) {
                            $diskon .= "<i class='diskon_reseller'>".($x>0?"+":"").get_diskon($value['id'],$_SESSION['user']['keranjang'][$key]['jumlah'],"reseller")."%</i>";$x++;
                        } 
                        echo $diskon;?></span>

                    </div>
                    <div class="col-1 col-lg-3 p-0 text-right">
                      <button type="button" class="btn btn-outline-danger delete_keranjang mr-1" style="border: transparent;" valid="<?php echo $value['id']?>"><i class="material-icons keranjang_delete">remove_shopping_cart</i></button>
                    </div>
                  </div>
                </div> 
                <?php } ?>            
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                  <label for="bikurir" class="float-left"><b>Biaya Kurir :</b></label>
                  <input type="text" readonly class="form-control-plaintext" id="bikurir" value="Rp. -">
                </div>
                <div class="form-group">
                  <label for="total" class="float-left"><b>Harga Total :</b></label>
                  <input type="text" readonly class="form-control-plaintext" id="total" value="Rp. <?php echo number_format(get_totals()); ?>">
                </div>
            </div>
            <div class="col-lg-6">
              <div class="form-group ">
                  <label for="kurir" class="float-left">Pilih Kurir*</label>
                  <select type="text" name="kurir" id="kurir" class="form-control" placeholder="Kurir" required value="">
                      <option required value="">Pilih Kurir</option>
                      <?php 
                        $qlurir = mysqli_query($koneksi,"SELECT distinct nama_kurir FROM kurir");
                        while ($rkurir = mysqli_fetch_assoc($qlurir)){
                        echo "<option value='".$rkurir['nama_kurir']."'>".$rkurir['nama_kurir']."</option>";
                        }
                      ?>
                  </select>
              </div>
              <div class="form-group ">
                  <label for="tykurir" class="float-left">Pilih Jenis Kurir*</label>
                  <select type="text" name="tykurir" id="tykurir" class="form-control" placeholder="tykurir" required value="">
                      <option required value="">Silakan pilih jenis kurir</option>
                  </select>
              </div>
              <div class="form-group ">
                  <label for="bank" class="float-left">Pilih Bank Pembayaran*</label>
                  <select type="text" name="bank" id="bank" class="form-control" placeholder="bank" required value="">
                      <option required value="">Pilih Bank Pembayaran</option>
                      <?php 
                        $qbank = mysqli_query($koneksi,"SELECT * FROM rekening_bank");
                        while ($rbank = mysqli_fetch_assoc($qbank)){
                        echo "<option value='".$rbank['id_bank']."'>".$rbank['nama_bank']."</option>";
                        }
                      ?>
                  </select>
              </div>
            </div>          
          </div>

          <div class="button">
            <input type="button" class="previous action-button-previous" value="Previous"/>
            <input type="submit" class="next action-button" id="next" value="Next"/>
          </div>
        </fieldset> 
        <?php } else{

            $rtransaksi = $qtransaksi->fetch_assoc();
            $no=0;
          $qbarang=mysqli_query($koneksi,"SELECT * FROM barang b INNER JOIN transaksi_barang tb on b.id_barang = tb.id_barang WHERE id_transaksi = ".$_GET['id_transaksi']);$date=get_paytick($_GET['id_transaksi']);
        }?>  
        <fieldset id="result">
<div class="row">
    <div class="col-lg-12 mb-4">
        <span>
            <h3>
                Batas Waktu Penjualan
            </h3>
            <hr class="border-bottom border-dark "/>
        </span>
    </div>
    <div class="col-lg-12 mb-4">
        <div class="row">
            <div class="col-12" id="paytick" style="font-size: 3rem; color: #ffc107; font-family: monospace;    font-weight: 100 !important">
                                        <?php 
                          
                          if($date['sec']<0){
                            echo "Waktu Telah Berakhir";
                          }else{
                            $hou = ($date['hour']<10) ? "0".$date['hour'] : $date['hour'];
                            $min = ($date['min']<10) ? "0".$date['min'] : $date['min'];
                            $sec = ($date['sec']<10) ? "0".$date['sec'] : $date['sec'];
                            echo "Sisa waktu ".$hou." jam ".$min." menit ".$sec." detik." ;
                          }
                        ?>
            </div>
        </div>
    </div>
    <div class="col-lg-12 mb-4 p-0">
        <span>
            <h3>
                Detail Pesanan
            </h3>
            <hr class="border-bottom border-dark "/>
        </span>
    </div>
    <div class="col-lg-12">
                  <div class="row">
                    <?php while ($rbarang = $qbarang->fetch_assoc()) {
                    $qdiskon=$koneksi->query("SELECT * FROM transaksi_diskon td INNER JOIN diskon d on td.id_diskon = d.id_diskon WHERE `id_tranbar` = ".$rbarang['id_tranbar']);
                    $diskon = "";
                    $todis=0;$x=0;
                    while ($rdiskon = $qdiskon->fetch_assoc()) {
                      $diskon .= ($x>0?"+":"").$rdiskon['diskon']."%";
                      $todis += $rdiskon['diskon'];$x++;
                    }
                    ?>
                    <div class="col-sm-6 col-lg-4 p-1">
                      <table style="border-radius: 3px; width: 100%; height: 100%">
                        <tr><td class='judulbarang' colspan="2" style="border-radius: 3px;background-color: #ffc107"><?php echo $rbarang['nama_barang']?></td></tr>
                        <tr>
                          <td width="25%" style="padding: 5px;"><img class='card-img-top' src='<?php echo $rbarang['foto_barang']?>'style='height:100px; width:100px'></img></td>
                          <td style="">
                            <table style="text-align: left">
                              <tr><td>Harga </td><td>: Rp. <?php echo number_format($rbarang['harga_barang'])?></td></tr>
                              <tr><td>Jumlah</td><td>: <?php echo $rbarang['jumlah']?></td></tr>
                              <?php if($diskon!=""){?>
                              <tr><td>Diskon</td><td>: <?php echo $diskon?></td></tr>
                              <?php } ?>
                              <tr><td>Subtotal</td><td>: <b>Rp. <?php 
                                $a=$rbarang['harga_barang']*$rbarang['jumlah'];

                                if($todis!=0){$a -=$rbarang['harga_barang']*$todis/100*$rbarang['jumlah'];}
                              echo number_format($a);
                              ?></b></td></tr>                                               
                            </table>
                        </tr>
                      </table>                    
                    </div>
                    <?php  } 

$maria=$koneksi->query("SELECT * FROM saldo WHERE id_transaksi='".$rtransaksi['id_transaksi']."'") or die(get_error());
$ozawa=$maria->fetch_assoc();
$kimochi=$ozawa['saldo'];
$jumkir = $rtransaksi['harga_kurir']+$rtransaksi['total']+$kimochi;
                    ?>
                  </div>
    </div>
    <div class="col-sm-6 mb-4 text-left">
        <span class="text-center">
            <h3>
                Rekening Toko
            </h3>
            <hr class="border-bottom border-dark "/>
        </span>
        <?php echo $rtransaksi['nama_bank']; ?>
        <br>
            <?php echo $rtransaksi['atas_nama']; ?>
            <br>
                <?php echo $rtransaksi['no_rekening']; ?>
                <br>
                    <?php echo $rtransaksi['cabang']; ?>
                    <br>
                        <div class="text-danger text-capitalize text-center mt-4">
                            Segera transfer ke nomer rekening tersebut.
                        </div>
                    </br>
                </br>
            </br>
        </br>
    </div>
    <div class="col-sm-6 mb-4 text-left">
        <span class="text-center">
            <h3>
                Data Penerima
            </h3>
            <hr class="border-bottom border-dark "/>
        </span>
        <?php echo $rtransaksi['penerima_nama']; ?>
        <br>
            <?php echo $rtransaksi['penerima_telp']; ?>
            <br>
                <?php echo $rtransaksi['penerima_email']; ?>
                <br>
                    <?php echo $rtransaksi['penerima_alamat']; ?>
                    <br>
                        <?php echo $rtransaksi['nama_provinsi']; ?>
                        <br>
                            <?php echo $rtransaksi['nama_kota_kab']; ?>
                            <br>
                                <?php echo $rtransaksi['penerima_kode_pos']; ?>
                                <br>
    </div>
    <div class="col-sm-6 mb-4 text-left">
        <span class="text-center">
            <h3>
                Kurir
            </h3>
            <hr class="border-bottom border-dark "/>
        </span>
        <?php echo $rtransaksi['nama_kurir']; ?>
        <br>
            <?php echo $rtransaksi['tipe']; ?>
            <br>
                Rp. <?php echo number_format($rtransaksi['harga_kurir']); ?>
            </br>
        </br>
    </div>
    <div class="col-sm-6 mb-4 text-left">
        <span class="text-center">
            <h3>
                Total Seluruh
            </h3>
            <hr class="border-bottom border-dark "/>
        </span>
        <span class="text-center">
            <p class="mb-0">
                Rp. <?php echo number_format($jumkir); ?>
            </p>
        </span>
        <br>
                <a href='javascript:void();' class='btn btn-secondary btn-sm w-100 '  onclick='lihat_bukti(<?php echo $rtransaksi['id_transaksi']; ?>);'>Foto Bukti Transfer</a>
        </br>
    </div>
</div>
<form method="post">
    <div class="button px-3 ">
        <a class="btn btn-block action-button mx-0" href='print.php?id=<?php echo $rtransaksi['id_transaksi']; ?>'="" style="width:100%; " target="_blank">
            Print
        </a>
    </div>
</form>

        </fieldset>
    </form>
  </div>
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <?php 

        ?>
        <h5 class="modal-title" id="exampleModalLabel">Bukti Tranfer <?php echo $upser['id_transaksi']?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
<?php

?>      
      <div class="modal-body">
         <img id="bukti_foto" class="img-thumbnail NO-CACHE" src="../img/ee11cbb19052e40b07aac0ca060c23ee/<?php echo $ruser['id_user']."".md5($ruser['id_user']."user".md5($ruser['username']))?>/f026feb40685d05ac33e1ddeddca1319/<?php echo $ruser['username']."_".$upser['id_transaksi'].".png"?>" alt="Card image" style="width:100%;">
          <br>
      </div>
      <div class="modal-footer">
          <form action="transaksi.php" method="post" enctype="multipart/form-data" >           
                <div class="form-group "id="foto">
                  <input type="hidden" name="id_foto" id="id_foto">
                    <label for="foto">Upload Foto</label>
                    <input type="file" name="foto" id="foto" accept="image/*" class="form-control-file" placeholder="foto">
                </div>           
              <button type="button" data-dismiss="modal" class="btn btn-secondary">Close</button>
              <button  class="btn btn-primary" name="foto">Save changes</button>

              <input type="hidden" name="form" value="update_foto">
          </form>  
      </div>
    </div>
  </div>
</div>


  <!-- /.MultiStep Form -->   
  <script type="text/javascript" src="<?php echo $root_base?>/assets/js/jquery.js"></script>  
  <script type="text/javascript" src="<?php echo $root_base?>/assets/js/jquery-easing/jquery.easing.min.js"></script>
  <script type="text/javascript" src="<?php echo $root_base?>/assets/js/vendor/popper.min.js"></script>
  <!--pemanggilan bootstrap js -->
  <script type="text/javascript" src="<?php echo $root_base?>/assets/js/bootstrap.js"></script>
  <script type = "text/javascript" > 
    $(document).ready(function() {
    function get_paytick(id){
      setInterval(function(){
              $.ajax({
                url: '',
                type: 'POST',
                dataType: 'json',
                data: {form: 'get_paytick',id:id},
              })
              .done(function(data) {
                if(data==null){
                  alert("waktu telah berakhir");
                  $('#paytick').html("Waktu Telah Berakhir");
                  window.location='<?php echo $root_base; ?>transaksi';
                }else{
                  $('#paytick').html(data);
                }
              })
            },1000);
      }
    <?php if($litrans){?>
      get_paytick(<?php echo $_GET['id_transaksi'] ?>);
        $("#progressbar li").addClass("active");
    <?php }else{?>
    var banyak = <?php echo $banyak; ?>

    //jQuery time
    var current_fs, next_fs, previous_fs; //fieldsets
    var left, opacity, scale; //fieldset properties which we will animate
    var animating; //flag to prevent quick multi-click glitches
    var id_trans = 0;
    $('#provinsi').change(function(event) {
        var id = $(this, 'option:selected').val();
        $.ajax({
            url: '',
            type: 'POST',
            dataType: 'json',
            data: {
                form: 'get_kota_kab',
                'id': id
            },
        }).done(function(data) {
            $('#kota_kab').html(data)
        })
    });
    $('#kurir').change(function(event) {
      data1 = $(this).val();
      $.ajax({
        url: '',
        type: 'POST',
        dataType: 'json',
        data: {form:'get_jenkur',id: data1},
      })
      .done(function(data) {
        $('#tykurir').html(data);
      })
      .fail(function() {
        console.log("error");
      })
      .always(function() {
        console.log("complete");
      });
    });
    $('#tykurir').change(function(event) {
      var id1 = $(this).val();
      $.ajax({
        url: '',
        type: 'POST',
        dataType: 'json',
        data: {form: 'get_kurir',id: id1},
      })
      .done(function(data) {
        $('#total').val(data['totals'])
        $('#bikurir').val(data['harga_kurir']);
      })
      .fail(function() {
      })
      .always(function() {
      });
      });
    function lanjut(obj) {
        if (animating) return false;
        animating = true;
        current_fs = obj.parent().parent();
        next_fs = obj.parent().parent().next();
        
        //activate next step on progressbar using the index of next_fs
        $("#progressbar li").eq($("fieldset").index(next_fs)).animate('slow').addClass("active");
        //show the next fieldset
        if ($(document).width() < 767) {
            $('.button').hide();
        }
        next_fs.show();
        //hide the current fieldset with style
        current_fs.animate({
            'opacity': '0'
        }, {
            step: function(now, mx) {
                //as the opacity of current_fs reduces to 0 - stored in "now"
                //1. scale current_fs down to 80%
                scale = 1 - (1 - now) * 0.2;
                //2. bring next_fs from the right(50%)
                left = (now * 50) + "%";
                //3. increase opacity of next_fs to 1 as it moves in
                opacity = 1 - now;
                current_fs.css({
                    'transform': 'scale(' + scale + ')',
                    'position': 'absolute',
                });
                next_fs.css({
                    'left': left,
                    'opacity': opacity
                });
            },
            duration: 800,
            complete: function() {
                current_fs.hide();
                animating = false;
                $('.button').fadeIn('slow');
            },
            //this comes from the custom easing plugin
            easing: 'easeInOutExpo'
        });
    }
    function balik(obj) {
        if (animating) return false;
        animating = true;
        current_fs = obj.parent().parent();
        previous_fs = obj.parent().parent().prev();
        //de-activate current step on progressbar
        $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");
        //show the previous fieldset
        if ($(document).width() < 767) {
            $('.button').hide();
        }
        previous_fs.show();
        //hide the current fieldset with style
        current_fs.animate({
            opacity: 0
        }, {
            step: function(now, mx) {
                //as the opacity of current_fs reduces to 0 - stored in "now"
                //1. scale previous_fs from 80% to 100%
                scale = 0.8 + (1 - now) * 0.2;
                //2. take current_fs to the right(50%) - from 0%
                left = ((1 - now) * 50) + "%";
                //3. increase opacity of previous_fs to 1 as it moves in
                opacity = 1 - now;
                current_fs.css({
                    'left': left
                });
                previous_fs.css({
                    'transform': 'scale(' + scale + ')',
                    'opacity': opacity
                });
            },
            duration: 800,
            complete: function() {
                current_fs.hide();
                animating = false;
                previous_fs.css({
                    'transform': 'unset',
                    'position': 'relative'
                });
                $('.button').fadeIn('slow');
            },
            //this comes from the custom easing plugin
            easing: 'easeInOutExpo'
        });
    }
    $('#msform').submit(function(event) {
        event.preventDefault();
        $obj = $('#next');
        var cfr = 0;
        var data = $(this).serializeArray();
        cfr = confirm('Apakah anda yakin bahwa data yang anda masukan sudah benar?');
        if (cfr) {
            $.ajax({
                url: '',
                type: 'POST',
                dataType: 'json',
                data: data,
            }).fail(function(event, xhr, settings, thrownError) {
                alert("Terjadi kesalahan, mohon periksa kembali atau hubungi pihak bantuan!");
            }).done(function(data) {
              //$('#result').html(data);
                $('#result').html(data['html']);
                lanjut($obj);
                get_paytick(data['id']);
                //window.location='?id_transaksi='+data['id'];
            })
        } else {}
    });
    $('.delete_keranjang').click(function(event) {
        id = $(this).attr('valid');
        $btn = $(this).parent().parent().parent();
        kurir=$('#tykurir').val();
        $.ajax({
            url: '',
            type: 'POST',
            dataType: 'json',
            data: {
                'form': 'delete_keranjang',
                'id': id,
                'kurir': kurir
            },
            success: (function(data) {
                $('#total').val(data);
            }),
        }).fail(function(event, xhr, settings, thrownError) {
            alert("Terjadi kesalahan, mohon periksa kembali atau hubungi pihak bantuan!");
        }).done(function(data) {
            banyak--;
            if (banyak < 1) {
                $.post('', {
                    'form': 'delete_transaksi',
                    'id': id_trans
                }, function(data, textStatus, xhr) {
                    /*optional stuff to do after success */
                });
                alert('Keranjang telah kosong');
                window.location = '<?php echo $root_base ?>';
            } else {
                $btn.remove();
            }
        })
    });
    $(".next").click(function() {
        if ($(this).attr('type') != "submit") {
            lanjut($(this));
        }
    });
    $(".previous").click(function() {
      balik($(this));
    });
    <?php } ?>
}); 

    function lihat_bukti(id){
      var path = "../img/ee11cbb19052e40b07aac0ca060c23ee/<?php echo $ruser['id_user']."".md5($ruser['id_user']."user".md5($ruser['username']))?>/f026feb40685d05ac33e1ddeddca1319/<?php echo $ruser['username']?>_"+id+".png";
      $.ajax({
          url: '',
          type: 'POST',
          dataType: 'json',
          data: {'form': 'get_bukti', 'id':id},
        })
        .done(function(data) {
          if (data==="belum dibayar") {
            $('#foto').show();
          }else{
            $('#foto').hide();
          }
          console.log("success");
        })
        .fail(function() {
          console.log("error");
        })
        .always(function() {
          console.log("complete");
        });
      $('#exampleModal').modal('show');
      $('#id_foto').html("<center>Tunggu Sebentar...</center>");
      $('#bukti_foto').attr('src', path);
      $('#id_foto').attr('value',id);
      $('#myModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) ;
        var recipient = button.data('id');
        var modal = $(this);
        modal.find('.modal-title').text('Lihat Bukti ' + recipient);
        modal.find('.modal-body input').val(recipient);
        
        
      })           
    }
  $('#myModal').on('shown.bs.modal', function () {
  $('#myInput').trigger('focus');
  })

</script>
</body>
</html>
<?php } ?>
