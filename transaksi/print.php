<?php
require_once '../koneksi/koneksi.php';
require('../ppdf/fpdf.php');
$ruser = chk_login();
$id = $_GET['id'];
// memanggil library FPDF

class PDF extends FPDF
{
    // Page header
    function Header()
    {
        // Logo
        //$this->Image('lobli1.png',10,6,30);
        // Arial bold 15
        $this->SetFont('Arial','B',15);
        // Move to the right
        $this->Cell(80);
        // Title
        $this->Cell(30,10,'BUKTI INVOICE ANDA',0,0,'C');
        // Line break
        $this->Line(10,20,200,20);
        $this->Ln(10);
    }

    // Page footer
    function Footer() 
    { 
        $this->SetY( -15 ); 

        $this->SetFont( 'Arial', '', 10 ); 

        $this->Cell(0,10,'',0,0,'L');
        $this->SetX($this->lMargin);
        $this->Cell(0,10,'Blilistrik.com',0,0,'C');
        $this->SetX($this->lMargin);
        $this->Cell( 0, 10, '', 0, 0, 'R' ); 
    } 
}
// intance object dan memberikan pengaturan halaman PDF

$pdf = new PDF('p','mm','A4');
$pdf->SetTitle("Blilistrik Invoice");
$pdf->AliasNbPages();
// membuat halaman baru
$pdf->AddPage();
// setting jenis font yang akan digunakan
 
//include 'koneksi.php';
//$row_user = $ruser['id_user'];
//$transaksi=mysqli_query($koneksi,sprintf("SELECT * FROM transaksi  WHERE id_user = '$row_user'"));
//$row_userr=mysqli_fetch_array($transaksi);
//echo "<script>alert('" . $row_userr['id_user'] . "');</script>";
//$row_tran=$row_userr['id_transaksi'];
//echo "<script>alert('" . $row_tran . "');</script>";
//$transaksii = $koneksi->query("SELECT * FROM transaksi_barang INNER JOIN transaksi ON transaksi.id_transaksi = transaksi_barang.id_transaksi WHERE transaksi_barang.id_transaksi= '$row_tran'") or die(get_error()); 
//$rowt=$transaksi->fecth_array();
//foreach ($_SESSION['user']['keranjang'] as $key => $value) {
$transaksi=$koneksi->query("SELECT * FROM transaksi_barang INNER JOIN transaksi ON transaksi_barang.id_transaksi = transaksi.id_transaksi INNER JOIN barang ON transaksi_barang.id_barang = barang.id_barang INNER JOIN users ON transaksi.id_user = users.id_user INNER JOIN kurir ON transaksi.id_kurir = kurir.id_kurir WHERE transaksi.id_transaksi = '$id'")or die(get_error());

//$roww = mysqli_fetch_array($transaksi);
//$totall = $roww['id_barang'];
$transaksi1 = $koneksi->query("SELECT * FROM transaksi WHERE id_transaksi=".$id);

$rtrans1 = $transaksi1->fetch_assoc();
$pdf->SetFont('courier','',10);
$date = new DateTime($rtrans1['tanggal_transaksi']);
$pdf->Cell(190,7,"".$date->format("d F Y"),0,1,'R');
// Memberikan space kebawah agar tidak terlalu rapat
$pdf->Ln(5);
$pdf->SetFont('Arial','B',10);
$pdf->Cell(10,6,'No',1,0,'C');
$pdf->Cell(75,6,'Nama Barang',1,0,'C');
$pdf->Cell(30,6,'Jumlah barang',1,0,'C');
$pdf->Cell(20,6,'diskon',1,0,'C');
$pdf->Cell(30,6,'Harga',1,0,'C');
$pdf->Cell(25,6,'Jumlah',1,1,'C');
 
$pdf->SetFont('courier','',10);
 
$no=0;

while ($row = mysqli_fetch_array($transaksi)){
    $diskon = "";
    $todis=0;
    $subtotal=$row['harga_barang']*$row['jumlah'];
    $x = 0;
    $qdiskon=$koneksi->query("SELECT * FROM transaksi_diskon td INNER JOIN diskon d on td.id_diskon = d.id_diskon WHERE `id_tranbar` = ".$row['id_tranbar']);
    while ($rdiskon = $qdiskon->fetch_assoc()) {
              $diskon .=($x>0?"+":"").$rdiskon['diskon']."%";
              $todis += $rdiskon['diskon'];              
              $subtotal -=$row['harga_barang']*$rdiskon['diskon']/100*$row['jumlah'];
              $x++;
            }
 $maria=$koneksi->query("SELECT * FROM saldo WHERE id_transaksi='".$row['id_transaksi']."' ORDER BY `saldo`.`id_saldo` DESC") or die(get_error());
$ozawa=$maria->fetch_assoc();
$kimochi=$ozawa['saldo'];           
$no++;
//$row = mysqli_fetch_array($transaksi);
$totall = $row['id_transaksi'];
$pdf->Cell(10,6,$no,1,0,'C');
$pdf->Cell(75,6,$row['nama_barang'],1,0);
$pdf->Cell(30,6,$row['jumlah'],1,0,'C');
if($diskon){
$pdf->Cell(20,6,$diskon,1,0,'C');
}else {$pdf->Cell(20,6,'0%',1,0,'C');
}
$pdf->Cell(30,6,'Rp.'.number_format($row['harga_barang']),1,0,'C');
$pdf->Cell(25,6,'Rp.'.number_format($subtotal),1,1,'R');
$total = $row['total'];
$kurir = $row['harga_kurir'];
$kutol = $total+$kurir+$kimochi;

}
$pdf->SetFont('Arial','B',10);
$pdf->Cell(135,6,'',1,0);
$pdf->Cell(30,6,'Kurir',1,0,'C');
$pdf->Cell(25,6,'Rp.'.$kurir,1,1,'R');
$pdf->SetFont('Arial','B',10);
$pdf->Cell(135,6,'',1,0);
$pdf->Cell(30,6,'Total',1,0,'C');
$pdf->Cell(25,6,'Rp.'.number_format($kutol),1,1,'R');
$pdf->SetFont('Arial','B',10);
$pdf->Cell(55,6,'Status',1,0,'L');
$pdf->Cell(135,6,$rtrans1['status'],1,1,'C');
$pdf->Output("I","Blilistrik Invoice ".($rtrans1['id_user'].$rtrans1['id_transaksi']."_".$rtrans1['tanggal_transaksi']).".pdf");

?>