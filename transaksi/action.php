<?php
require '../koneksi/koneksi.php';
$ruser = chk_login();




if (isset($_GET['lihat_barang']) and $_GET['id'] != '') {
    ?>
    <div class="table-responsive">   
          <table class="table table-striped table-sm">
            <thead>
               <tr>
                  <th>#</th>
                  <th>Gambar</th>
                  <th>Nama Barang</th>
                  <th>Harga Satuan Barang</th>
                  <th>Jumlah Barang</th>
                </tr>
            </thead>
            <tbody>
            <?php
            $iduser = $ruser['id_user'];
            $idtransaksi = $_GET['id'];
            $query = $koneksi->query("SELECT * FROM transaksi_barang WHERE id_transaksi='$idtransaksi'");
            $no = 0;
            while ($row = $query->fetch_array()) {
              $no++;
              $idbarang = $row['id_barang'];
              $query2 = $koneksi->query("SELECT * FROM barang WHERE id_barang='$idbarang'");
              $barang = $query2->fetch_array();
            ?>
                <tr>
                  <th scope="row"><?php echo $no;?></th>
                  <th><img src="<?php echo $root_base?><?php echo $barang['foto_barang'];?>" alt="..." class="img-fluid rounded-circle NO-CACHE" width="80px" height="80px"></td></th>
                  <td><?php echo $barang['nama_barang'];?></td>
                  <td><?php echo $barang['harga_barang'];?></td>
                  <td><?php echo $row['jumlah'];?></td>
                </tr>
            <?php } ?>
            </tbody>
          </table>
        </div>
    <?php
}
?>