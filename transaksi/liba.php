<?php
require '../koneksi/koneksi.php';
$ruser = chk_login();




if (isset($_GET['lihat_barang']) and $_GET['id'] != '') {
    ?>
    <div class="table-responsive">   
          <table class="table table-sm">
            <thead>
               <tr align="center">
                  <th>#</th>
                  <th>Gambar</th>
                  <th>Nama</th>
                  <th>Harga</th>
                  <th>Diskon</th>
                  <!--th>Harga Kurir</th-->
                  <th>Jumlah</th>
                  <th>Sub total</th>
                </tr>
            </thead>
            <tbody>
            <?php
            $iduser = $ruser['id_user'];
            $idtransaksi = $_GET['id'];
            $qbarang = $koneksi->query("SELECT * FROM transaksi_barang tb inner join barang b ON tb.id_barang = b.id_barang WHERE id_transaksi='$idtransaksi'");
             //$qbarang = $koneksi->query("SELECT * FROM transaksi_barang inner join barang ON transaksi_barang.id_barang = barang.id_barang INNER JOIN transaksi ON transaksi_barang.id_transaksi = transasi.id_transaksi INNER JOIN kurir ON transasi.id_kurir = kurir.id_kurir WHERE id_transaksi='$idtransaksi'");
            $no = 0;
            while ($rbarang = mysqli_fetch_array($qbarang)) {
              $no++;
                $x = 0;
                $qdiskon=$koneksi->query("SELECT * FROM transaksi_diskon td INNER JOIN diskon d on td.id_diskon = d.id_diskon WHERE `id_tranbar` = ".$rbarang['id_tranbar']);
                while ($rdiskon = $qdiskon->fetch_assoc()) {
                          $diskon .=($x>0?"+":"").$rdiskon['diskon']."%";
                          $todis += $rdiskon['diskon'];              
                          $x++;
                        }
                        $subtotal = ($rbarang['harga']-( $rbarang['harga'] * $todis / 100)) * $rbarang['jumlah'];
                    ?>
                <tr>
                  <th scope="row"><?php echo $no;?></th>
                  <th><img src="<?php echo $rbarang['foto_barang'];?>" alt="..." class="img-fluid NO-CACHE" width="80px" height="80px"></td></th>
                  <td><?php echo $rbarang['nama_barang'];?></td>
                  <td align="right">Rp. <?php echo number_format($rbarang['harga'],0,',','.');?></td>
                  <td align="right"><?php echo $diskon; ?></td>
                  <td align="right"><?php echo $rbarang['jumlah'];?></td>
                  <td>Rp. <?php echo number_format($subtotal,0,',','.');?></td>
                </tr>
            <?php } ?>
            </tbody>
          </table>
        </div>
    <?php
}
?>