import './bootstrap'
import Vue from 'vue'
import axios from 'axios'
import VueAxios from 'vue-axios'

import router from './router'
import store from './store'

//Font awesome
// import 'vue-awesome/icons'
// import Icon from 'vue-awesome/components/Icon'
// Vue.component('v-icon', Icon)
// import NProgress from 'nprogress'
import 'nprogress/nprogress.css';

import AppLayout from './layouts/app.vue'

Vue.use(VueAxios, axios)

Vue.config.productionTip = false

const app = new Vue({
    el: '#app',
    render: h => h(AppLayout),
    store,
    router
})


window.vue = app