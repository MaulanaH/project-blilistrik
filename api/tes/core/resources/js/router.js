import './bootstrap'
import Vue from 'vue'
import VueRouter from 'vue-router'
import VueMeta from 'vue-meta'

Vue.use(VueRouter)
Vue.use(VueMeta)

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'home',
            component: require('./pages/home.vue').default
        },
        {
            path: '/post/:slug',
            name: 'post',
            component: require('./pages/post.vue').default
        }
    ]
})

router.beforeEach((to, from, next) => {
    if (to.name) {
        window.nprogress.start()
    }

    setTimeout(() => { next() }, 300)
})

router.beforeResolve((to, from, next) => {
    if (to.name) {
        window.nprogress.inc()
    }
    setTimeout(() => { next() }, 200)
})

router.afterEach((to, from) => {
    setTimeout(() => { window.nprogress.done() }, 100)
})
  


export default router;