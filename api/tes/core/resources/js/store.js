import Vue from 'vue'
import Vuex from 'vuex'


Vue.use(Vuex)



const store = new Vuex.Store({
    state:  {
        article: null
    },
	mutations: {
        setTempArticles(state, data){
            state.article = data
        }
    }
})


export default store