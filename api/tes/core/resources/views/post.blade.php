<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Page Title -->
    <title>ShareILmu - POST</title>

    <!-- Meta -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    
    <!-- Style -->
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
</head>
<body>
    <!-- App -->
    <div id="app"></div>
    
    <!-- Script -->
    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>