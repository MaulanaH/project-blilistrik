<?php

require_once "koneksi/koneksi.php";

if ( isset( $_POST[ 'view' ] ) ) {

	// $koneksi = mysqli_koneksinect("localhost", "root", "", "notif");

	if ( $_POST[ "view" ] != '' )

	{
		$update_query = "UPDATE notif_trans1 SET status = 1 WHERE status=0";
		mysqli_query( $koneksi, $update_query );
	}

	$query = "SELECT * FROM notif_trans1 INNER JOIN users ON notif_trans1.id_user = users.id_user INNER JOIN transaksi ON notif_trans1.id_transaksi = transaksi.id_transaksi ORDER BY id_notif DESC LIMIT 5";
	$result = mysqli_query( $koneksi, $query );
	$output = '';

	if ( mysqli_num_rows( $result ) > 0 ) {

		while ( $row = mysqli_fetch_array( $result ) )

		{

			$output .= '
                    <li><a rel="nofollow" href="admin.php?page=transaksi" class="dropdown-item"> 
                        <div class="notification">
                          <div class="notification-content"><i class="fa fa-envelope bg-green"></i>Atas Nama <b>' . $row[ "fname" ] . '</b>telah membayar<br>klik untuk melihat semuanya</div>
                          <div class="notification-time"><small>' . $row[ "tanggal" ] . '</small></div>
                        </div></a></li>
                       
  ';
		}
		$output .= '
                        <li><a rel="nofollow" href="#" class="dropdown-item all-notifications text-center"> <strong>view all notifications                                            </strong></a></li> 
';
	} else {
		$output .= '<li><a href="#" class="text-bold text-italic">No Noti Found</a></li>';
	}

	$status_query = "SELECT * FROM notif_trans1 WHERE status=0";
	$result_query = mysqli_query( $koneksi, $status_query );
	$count = mysqli_num_rows( $result_query );

	$data = array(
		'notification' => $output,
		'unseen_notification' => $count
	);

	echo json_encode( $data );
}
?>