<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SubKategori extends Model
{
    protected $table = 'kategori_sub';
    protected $primaryKey = 'id_sub_kategori';
    protected $fillable = [
    	'nama_sub_kategori', 'id_kategori'
    ];

    public function kategori()
    {
    	return $this->hasOne('App\Models\Kategori', 'id_kategori', 'id_kategori');
    }
}
