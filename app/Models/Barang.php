<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Barang extends Model
{
    protected $table = 'barang';
    protected $primaryKey = 'id_barang';
    protected $fillable = [
        'kode_barang','id_sub_kategori','id_brand','nama_barang','deskripsi','harga_barang','stok_barang','satuan_stok',
        'berat_barang'  ,  'satuan_berat'  , 'status'
    ];

    public function brand()
    {
    	return $this->hasOne('App\Models\Brand', 'id_brand', 'id_brand');
    }
    public function subkategori()
    {
    	return $this->hasOne('App\Models\SubKategori', 'id_sub_kategori', 'id_sub_kategori');
    }
}
