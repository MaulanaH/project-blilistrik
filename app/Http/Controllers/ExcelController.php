<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class ExcelController extends Controller
{
	public function index(){
		//return \App\Models\Barang::with('brand', 'subkategori', 'subkategori.kategori')->get();

		return view('excel.index');
	}

    public function import(Request $request){
    	if ($request->has('truncate')) {
    		\App\Models\Barang::truncate();
    	}
    	$request->validate([
    		'excel_file' => 'required',
    	]);
		  Excel::import(new \App\Imports\BarangImport, $request->excel_file);
		  echo "impor berhasil";
    }
    
    public function export(Request $request){
      
    }
}
