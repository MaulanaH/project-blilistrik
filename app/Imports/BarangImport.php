<?php

namespace App\Imports;

use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

use App\Models\Barang;
use App\Models\Brand;
use App\Models\Kategori;
use App\Models\SubKategori;

class BarangImport implements ToModel, WithHeadingRow
{
    protected $init         = false;
    protected $brand        = [];
    protected $kategori     = [];
    protected $subkategori  = [];

    public function chunkSize(): int
    {
        return 1000;
    }

    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        if (!$this->init) $this->init();
        
        $row = $this->decRow($row);

        // echo "<pre>";
        // print_r($row);
        // echo "</pre>";

        return new Barang([
            'kode_barang'   => $row['kode'],
            'id_sub_kategori'   => $row['sub_kategori'],
            'id_brand'      => $row['brand'],
            'nama_barang'   => $row['nama'],
            'deskripsi'     => $row['deskripsi'],
            'harga_barang'  => $row['harga'],
            'stok_barang'   => $row['stok'],
            'satuan_stok'   => $row['satuan_stok'],
            'berat_barang'  => $row['berat'],
            'satuan_berat'  => $row['satuan_berat'],
            'status'        => 'aktif'
        ]);

        // if (count($this->subkategori) > 1) print_r($this->subkategori);
    }

    protected function init()
    {

        $this->init = true;
    }

    public function decRow($row)
    {
        $search_brand = Brand::where('nama_brand', $row['brand'])->get();
        if (count($search_brand) == 0)
        {
            $brand = Brand::create(['nama_brand' => $row['brand']]);
            $row['brand'] = $brand->id_brand;
        } else {
            $row['brand'] = $search_brand[0]->id_brand;
        }


        $search_kategori = Kategori::where('nama_kategori', $row['kategori'])->get();
        if (count($search_kategori) == 0)
        {
            $kategori = Kategori::create(['nama_kategori' => $row['kategori']]);
            $subkategori = $this->makeSubKategori($kategori->id_kategori, $row['sub_kategori']);

            $row['kategori'] = $kategori->id_kategori;
            $row['sub_kategori'] = $subkategori->id_sub_kategori;
        } else {
            $row['kategori'] = $search_kategori[0]->id_kategori;

            $search_subkategori = SubKategori::where('nama_sub_kategori', $row['sub_kategori'])->get();
            if (count($search_subkategori) == 0)
            {
                $subkategori = $this->makeSubKategori($search_kategori[0]->id_kategori, $row['sub_kategori']);
                $row['sub_kategori'] = $subkategori->id_sub_kategori;
            } else {
                $row['sub_kategori'] = $search_subkategori[0]->id_sub_kategori;
            }
            //$row['kategori'] = $search_kategori[0]->id_kategori;
        }

        return $row;
    }

    public function makeSubKategori($id_kategori, $nama_sub_kategori)
    {
        return SubKategori::create([
            'id_kategori' => $id_kategori,
            'nama_sub_kategori' => $nama_sub_kategori
        ]);
    }
}
