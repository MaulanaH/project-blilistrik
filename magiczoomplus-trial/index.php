<?php
/**
 * V-CMS (CMS Yang Dapat Anda Kelola Lebih Baik!)
 *
 * @author   Vian Dwi <fiandwi0424@gmail.com>
 * @version  1.0.0
 */
define('VCMS_START', microtime(true));

/*
|--------------------------------------------------------------------------
| Composer Auto Load
|--------------------------------------------------------------------------
|
| Composer telah menyediakan manajemen package yang baik, jadi kami memakai
| Composer untuk membuat aplikasi ini agar lebih baik lagi.
|
*/
require __DIR__ . "/vendor/autoload.php";

/*
|--------------------------------------------------------------------------
| Persiapan V-CMS
|--------------------------------------------------------------------------
|
| Ini adalah proses untuk mempersiapkan aplikasi ini untuk berjalan, berisi
| banyak persiapan yang kami lakukan untuk kedepannya.
|
*/
require __DIR__ . "/core/boot.php";

/*
|--------------------------------------------------------------------------
| Kernel Respon
|--------------------------------------------------------------------------
|
| Berhubungan dengan booting ke system tadi, maka sekarang kami akan membuka
} pendengaran request dari user dan mengolah datanya.
|
*/
$kernel = new core\kernel\router;

$kernel->process();