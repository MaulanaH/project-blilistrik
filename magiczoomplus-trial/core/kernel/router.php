<?php
namespace core\kernel;

use core\kernel\response as HTTPresponse;

class router {
private $getRequestUrl;

	function __construct(){
		$this->getRequestUrl = $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
	}

	public function getRequestUrl() {
        return $this->getRequestUrl;
    }

    Public function process(){
    	echo HTTPresponse::CallResponse( $this->getRequestUrl() );
    }

}