<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBarangTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('barang', function (Blueprint $table) {
            $table->increments('id_barang');
            $table->string('kode_barang');
            $table->integer('id_sub_kategori');
            $table->integer('id_brand');
            $table->string('nama_barang');
            $table->text('deskripsi')->nullable();
            $table->integer('harga_barang');
            $table->string('stok_barang');
            $table->integer('berat_barang');
            $table->string('satuan_berat');
            $table->string('satuan_stok');
            $table->string('foto_barang')->nullable();
            $table->enum('status', ['aktif', 'nonaktif']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('barang');
    }
}
