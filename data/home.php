<header class="page-header">
					<div class="container-fluid">
						<h2 class="no-margin-bottom">Dashboard</h2>
					</div>
				</header>

				<section class="dashboard-counts">
					<div class="container-fluid">
						<div class="row bg-white has-shadow">
							<!-- Item -->
							<div class="col-xl-3 col-sm-6">
								<div class="item d-flex align-items-center">
									<div class="icon bg-violet"><i class="icon-user"></i>
									</div>
									<div class="title"><span>Jumlah<br>Admin</span>
										<div class="">
											<a href="?page=admin">
												<p style="margin: 0;padding: 0;font-size: 15px;">Lihat Sekarang</p>
											</a>
										</div>
									</div>
									<?php
					$querya=mysqli_query($koneksi,"SELECT * FROM admin");
                      //$hasil=mysqli_num_rows($query);
                      //$data = array();
                      while(($rowa = mysqli_fetch_array($querya)) != null){
                          $dataa[] = $rowa; 
                        }
                      $countadmin = count($dataa);
                      ?>
									<div class="number">
										<strong>
											<?php echo "$countadmin" ?>
										</strong>
									</div>
								</div>
							</div>
							<!-- Item -->
							<div class="col-xl-3 col-sm-6">
								<div class="item d-flex align-items-center">
									<div class="icon bg-red"><i class="icon-user"></i>
									</div>
									<div class="title"><span>Jumlah<br>Users</span>
										<div class="">
											<a href="?page=user">
												<p style="margin: 0;padding: 0;font-size: 15px;">Lihat Sekarang</p>
											</a>
										</div>
									</div>
									<?php
						  $queryu=mysqli_query($koneksi,"SELECT * FROM users");
					//$hasil=mysqli_num_rows($query);
					//$data = array();
					while(($rowu = mysqli_fetch_array($queryu)) != null){
						$datau[] = $rowu;
					}
					$countuser = count($datau);
									?>
									<div class="number">
										<strong>
											<?php echo "$countuser" ?>
										</strong>
									</div>
								</div>
							</div>
							<!-- Item -->
							<div class="col-xl-3 col-sm-6">
								<div class="item d-flex align-items-center">
									<div class="icon bg-green"><i class="icon-padnote"></i>
									</div>
									<div class="title"><span>Jumlah<br>Produk</span>
										<div class="">
											<a href="?page=barang">
												<p style="margin: 0;padding: 0;font-size: 15px;">Lihat Sekarang</p>
											</a>
										</div>
									</div>
									<?php 
                        $queryb=mysqli_query($koneksi,"SELECT * FROM barang");
                        //$hasil=mysqli_num_rows($query);
                        //$data = array();
                        while(($rowb = mysqli_fetch_array($queryb)) != null){
                            $datab[] = $rowb; 
                          }
                        $countbarang = count($datab);
                        ?>
									<div class="number">
										<strong>
											<?php echo "$countbarang" ?>
										</strong>
									</div>
								</div>
							</div>
							<!-- Item -->
							<div class="col-xl-3 col-sm-6">
								<div class="item d-flex align-items-center">
									<div class="icon bg-orange"><i class="icon-check"></i>
									</div>
									<div class="title"><span>Transaksi</span>
										<!--div class="progress">
                          <div role="progressbar" style="width: 50%; height: 4px;" aria-valuenow="{#val.value}" aria-valuemin="0" aria-valuemax="100" class="progress-bar bg-orange"></div>
                        </div-->
									</div>
									<div class="number">
										<strong>
											<?php 
                        $qtrans =$koneksi->query("SELECT COUNT(id_transaksi) AS id FROM transaksi ");
                        $qe = $qtrans->fetch_object();
                        echo $qe->id;

                       ?>
										</strong>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<!-- slider bar -->
				<section class="feeds no-padding-top ">
					<div class="container-fluid">
						<div class="row">
							<!-- Trending Articles-->
							<div class="col-lg-12">
								
								<div class="articles card">
									<div class="card-close">
										<div class="dropdown">
											<button type="button" id="closeCard4" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle"><i class="fa fa-ellipsis-v"></i></button>
											<div aria-labelledby="closeCard4" class="dropdown-menu dropdown-menu-right has-shadow"><a href="#" class="dropdown-item remove"> <i class="fa fa-times"></i>Close</a><a href="#" class="dropdown-item edit"> <i class="fa fa-gear"></i>Edit</a>
											</div>
										</div>
									</div>
									<div class="card-header d-flex align-items-center">
										<h2 class="h3">Upload Logo - Navbar  </h2>

									</div>
									<div class="card-body">
										<div style="text-align: center;margin-bottom: 15px;">
										    <img src="../img/logo/navbar_default.png" class="img img-fluid img-border d-block w-100 NO-CACHE" style="border: 2px solid black;" />
										</div>
										
										<form name="upload_logo" action="" method="post" enctype="multipart/form-data">
											<div class="custom-file">
												<input type="file" class="custom-file-input" id="customFile" accept="image/*" name="logo">
												<label class="custom-file-label" for="customFileLang">Silakan Pilih Logo</label>
											</div>
											<button type="submit" name="upload_logo" value="upload_logo" class="btn btn-primary float-right m-4">Upload Logo</button>
										</form>
										<?php 
                                        $var =0;
                                        if (isset($_POST["upload_logo"])) {
                                           $fnm=$_FILES["logo"]["tmp_name"];
                                           $ext = pathinfo($fnm, PATHINFO_EXTENSION);
                                           $dst="../img/logo/navbar_default.png"; //.md5($i).sha1($i).".png";
                                           $logom=move_uploaded_file($fnm,$dst);
                                           
                                             if ($logom){
                                              echo "<script>alert('Update berhasil');window.location='admin.php';</script> ";
                                            }else {
                                              echo "<script>alert('Data belum di masukan atau Update gagal');window.location='admin.php';</script> ";
                                            }
                
                                        }
                                        ?>
									</div>
								</div>
								
								
								
								
								<div class="articles card">
									<div class="card-close">
										<div class="dropdown">
											<button type="button" id="closeCard4" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle"><i class="fa fa-ellipsis-v"></i></button>
											<div aria-labelledby="closeCard4" class="dropdown-menu dropdown-menu-right has-shadow"><a href="#" class="dropdown-item remove"> <i class="fa fa-times"></i>Close</a><a href="#" class="dropdown-item edit"> <i class="fa fa-gear"></i>Edit</a>
											</div>
										</div>
									</div>
									<div class="card-header d-flex align-items-center">
										<h2 class="h3">Upload Logo - Favicon  </h2>

									</div>
									<div class="card-body">
										<div style="text-align: center;margin-bottom: 15px;">
										    <img src="../img/logo/favicon_default.png" class="img img-fluid img-border d-block w-100 NO-CACHE" style="border: 2px solid black;" />
										</div>
										
										<form name="upload_favicon" action="" method="post" enctype="multipart/form-data">
											<div class="custom-file">
												<input type="file" class="custom-file-input" id="customFile" accept="image/*" name="favicon">
												<label class="custom-file-label" for="customFileLang">Silakan Pilih Logo</label>
											</div>
											<button type="submit" name="upload_favicon" value="upload_favicon" class="btn btn-primary float-right m-4">Upload Favicon</button>
										</form>
										<?php 
                                        $var =0;
                                        if (isset($_POST["upload_favicon"])) {
                                           $fnm=$_FILES["favicon"]["tmp_name"];
                                           $ext = pathinfo($fnm, PATHINFO_EXTENSION);
                                           $dst="../img/logo/favicon_default.png"; //.md5($i).sha1($i).".png";
                                           $logom=move_uploaded_file($fnm,$dst);
                                           
                                             if ($logom){
                                              echo "<script>alert('Update berhasil');window.location='admin.php';</script> ";
                                            }else {
                                              echo "<script>alert('Data belum di masukan atau Update gagal');window.location='admin.php';</script> ";
                                            }
                
                                        }
                                        ?>
									</div>
								</div>
									    
									    
								
									    
									    
								<div class="articles card">
									<div class="card-close">
										<div class="dropdown">
											<button type="button" id="closeCard4" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle"><i class="fa fa-ellipsis-v"></i></button>
											<div aria-labelledby="closeCard4" class="dropdown-menu dropdown-menu-right has-shadow"><a href="#" class="dropdown-item remove"> <i class="fa fa-times"></i>Close</a><a href="#" class="dropdown-item edit"> <i class="fa fa-gear"></i>Edit</a>
											</div>
										</div>
									</div>
									<div class="card-header d-flex align-items-center">
										<h2 class="h3">Upload Slider   </h2>

									</div>
									<div class="card-body no-padding">
										<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
											<ol class="carousel-indicators">
												<li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
												<li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
												<li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
											</ol>
											<div class="carousel-inner">
												<div class="carousel-item active">
													<img alt="First slide" class="d-block w-100 NO-CACHE" src="../img/10bf08f0bbd6689475be65b4ae441bd9/c4ca4238a0b923820dcc509a6f75849b356a192b7913b04c54574d18c28d46e6395428ab.png" style="height: 200px">

												</div>
												<div class="carousel-item">
													<img alt="Second slide" class="d-block w-100 NO-CACHE" src="../img/10bf08f0bbd6689475be65b4ae441bd9/c81e728d9d4c2f636f067f89cc14862cda4b9237bacccdf19c0760cab7aec4a8359010b0.png" style="height: 200px">

												</div>
												<div class="carousel-item">
													<img alt="Third slide" class="d-block w-100 NO-CACHE" src="../img/10bf08f0bbd6689475be65b4ae441bd9/eccbc87e4b5ce2fe28308fd9f2a7baf377de68daecd823babbb58edb1c8e14d7106e83bb.png" style="height: 200px">

												</div>
											</div>
											<a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                          <span class="carousel-control-prev-icon" aria-hidden="true" style="height: 100%"></span>
                          <span class="sr-only">Previous</span>
                        </a>
										






											<a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                          <span class="carousel-control-next-icon" aria-hidden="true" style="height: 100%"></span>
                          <span class="sr-only">Next</span>
                        </a>
										






										</div>
										<form name="slider" action="" method="post" enctype="multipart/form-data">
											<?php 
                          for($i=1;$i<=3;$i++)
                          {
                            ?>
											<div class="item d-flex align-items-center ">

												<div class="text col-lg-12"><a href="#"></a>
													<div class="custom-file">
														<input type="file" class="custom-file-input" id="customFile" accept="image/*" name="foto<?php echo $i;?>">
														<label class="custom-file-label" for="customFileLang">Silakan Pilih Foto</label>
													</div>
												</div>
											</div>
											<?php }
                        ?>
											<button type="slider" name="slider" value="update slider" class="btn btn-primary float-right m-4">Upload Slider
                        </button>
										</form>


										<?php 
                        $var =0;

                        if (isset($_POST["slider"])) {
                          for($i=1;$i<=3;$i++){
                           $fnm=$_FILES["foto$i"]["tmp_name"];
                           $dst="../img/10bf08f0bbd6689475be65b4ae441bd9/".md5($i).sha1($i).".png";
                           $sliderm=move_uploaded_file($fnm,$dst);
                        //move_uploaded_file($_FILES['foto$i']['tmp_name'],'../../../img/slider/');
                         }
                             if ($sliderm){
                              echo "<script>alert('Update berhasil');window.location='admin.php';</script> ";
                            }else {
                              echo "<script>alert('Data belum di masukan atau Update gagal');window.location='admin.php';</script> ";
                            }

                        }
                        ?>
                        
                        
									</div>
								</div>
								<div class="articles card">
									<div class="card-close">
										<div class="dropdown">
											<button type="button" id="closeCard4" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle"><i class="fa fa-ellipsis-v"></i></button>
											<div aria-labelledby="closeCard4" class="dropdown-menu dropdown-menu-right has-shadow"><a href="#" class="dropdown-item remove"> <i class="fa fa-times"></i>Close</a><a href="#" class="dropdown-item edit"> <i class="fa fa-gear"></i>Edit</a>
											</div>
										</div>
									</div>
									<div class="card-header d-flex align-items-center">
										<h2 class="h3">Social Media Link</h2>
									</div>
									<div class="card-body">
									    <?php
									   // error_reporting(1);
									    ### FB LINK
									    $config = mysqli_query($koneksi, "SELECT * FROM config WHERE id='1'");
									    $fb = $config->fetch_array();
									    if ($fb['value'] == null or $fb['value'] == '') {
									        $fb =  $fb['default'];
									    } else {
									        $fb = $fb['value'];
									    }
									    ### GOOGLE LINK
									    $config = mysqli_query($koneksi, "SELECT * FROM config WHERE id='2'");
									    $gp = $config->fetch_array();
									    if ($gp['value'] == null or $gp['value'] == '')  {
									        $gp =  $gp['default'];
									    } else {
									        $gp = $gp['value'];
									    }
									    ### IG LINK
									    $config = mysqli_query($koneksi, "SELECT * FROM config WHERE id='3'");
									    $ig = $config->fetch_array();
									    if ($ig['value'] == null or $ig['value'] == '')  {
									        $ig =  $ig['default'];
									    } else {
									        $ig = $ig['value'];
									    }
									    ### WA LINK
									    $config = mysqli_query($koneksi, "SELECT * FROM config WHERE id='4'");
									    $wa = $config->fetch_array();
									    if ($wa['value'] == null or $wa['value'] == '')  {
									        $wa =  $wa['default'];
									    } else {
									        $wa = $wa['value'];
									    }
									    
									    
									   // die('');
									   // $fb = $config['']
									    ?>
									    
										<form name="sosmed-link" action="" method="post">
										    <div class="form-group">
										        <label>Facebook :</label>
										        <input type="text" value="<?php echo $fb;?>" name="link-fb" class="form-control">
										    </div>
										    <div class="form-group">
										        <label>Google+ :</label>
										        <input type="text" value="<?php echo $gp;?>" name="link-gp" class="form-control">
										    </div>
										    <div class="form-group">
										        <label>Instagram :</label>
										        <input type="text" value="<?php echo $ig;?>" name="link-ig" class="form-control">
										    </div>
										    <div class="form-group">
										        <label>Whatsapp :</label>
										        <input type="text" value="<?php echo $wa;?>" name="link-wa" class="form-control">
										    </div>
										    <button type="submit" name="sosmed-link" value="sosmed-link" class="btn btn-primary float-right m-4">Simpan</button>
										</form>
									    <?php
									    if (isset($_POST["sosmed-link"])) {
									        if ( isset($_POST['link-fb']) and isset($_POST['link-gp']) and isset($_POST['link-ig']) and isset($_POST['link-wa']) ) {
									            //and $_POST['link-fb'] != '' and $_POST['link-gp'] != '' and $_POST['link-ig'] != '' and $_POST['link-wa'] != '' 
									            
									            for($i=1;$i<5;$i++) {
									                switch($i){
									                    case 1:
									                        $col = $_POST['link-fb'];
									                        break;
									                    case 2:
									                        $col = $_POST['link-gp'];
									                        break;
									                    case 3:
									                        $col = $_POST['link-ig'];
									                        break;
									                    case 4:
									                        $col = $_POST['link-wa'];
									                        break;
									                    default:
									                        $col = $_POST['link-fb'];
									                        break;
									                }
									                
    									            $update = "UPDATE config SET value='".$col."' WHERE id = ".$i.";";
    									            $result = mysqli_query($koneksi, $update);
    									            if ($result) {
    									                echo "<script>alert('Update Berhasil!');window.location='admin.php';</script> ";
    									            } else {
    									                echo "<script>alert('Update gagal!');window.location='admin.php';</script> ";
    									                echo mysqli_error($koneksi);
    									                break;
    									            }
									            }
									        } else {
									            echo "<script>alert('Data ada yang tidak terisi!');window.location='admin.php';</script> ";
									        }
									    }
									    ?>
										
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>