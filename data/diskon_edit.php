<?php
require_once "../../koneksi/koneksi.php";
$id = $_GET[ 'id' ];
$token = $_SESSION[ 'token' ];
$where = sprintf( "WHERE md5(concat('%s',jenis,id_diskon)) = '%s'", $_SESSION[ 'token' ], $id );
$query = $koneksi->query( "SELECT * FROM diskon $where" );
$row = $query->fetch_assoc();
?>
<div class="form-group row">
 <label class="col-sm-3 form-control-label" for="id_barang">Nama barang</label>
              <div class="col-sm-9">
    <select class="form-control" id="id_barang" name="id_barang">
      <option value="">pilih barang</option>
        <?php
        $query2 = $koneksi->query("SELECT * FROM barang");
            while ($row2 = $query2->fetch_assoc()) {
                $selected = ($row['id_barang']==$row2['id_barang']) ? "SELECTED" : "";
                echo '<option value="' . $row2['id_barang'] . '"'.$selected.'>' . $row2['nama_barang'] . '</option>';
            }
        ?>
    </select>                                    
    </div>
</div>
<div class="form-group row">
    <label class="col-sm-3 form-control-label">Jenis diskon</label><div class="col-sm-9">
    <input name="jenis" type="text" id="jenis" placeholder="Tulis jenis diskon" class="form-control" value="<?php echo $row['jenis'] ?>">
    </div>
</div>
<div class="form-group row">
    <label class="col-sm-3 form-control-label">Diskon %</label><div class="col-sm-9">
  <input name="diskon" type="number" min="0" max="100" placeholder="diskon %" class="form-control" value="<?php echo $row['diskon'] ?>">
</div>
</div>
            <div class="form-group row">
                        <label class="col-sm-3 form-control-label">Minimum pembelian</label><div class="col-sm-9">
  <input name="qty" type="number" placeholder="minimum pembelian" class="form-control" value="<?php echo $row['qty'] ?>">
</div>
</div>
            <div class="form-group row">
                        <label class="col-sm-3 form-control-label">Status</label><div class="col-sm-9">
    <select class="form-control" id="status" name="status">
      <option value="">pilih status</option>
      <option value="1" <?php echo ($row['status']=="aktif") ? "SELECTED" : ""; ?>>Aktif</option>
      <option value="2" <?php echo ($row['status']=="nonaktif") ? "SELECTED" : ""; ?>>Nonaktif</option>

    </select></div>
</div>
   <div class="form-group ">
</div>                                
<input type="hidden" name="form" value="update">
<input id="edit_id" type="hidden" value="<?php echo $id?>" style="display:none;" name="id">
<script type="text/javascript">
    $(document).ready(function ()
    {           
    $('.NO-CACHE').attr('src',function () { return $(this).attr('src') + "?upload=" + Math.random() });
    });
    $(document).ready(function(){
    $('#kategorie').on('change',function(){
    var barangID = $(this).val();
    if(barangID){
    $.ajax({
    type:'POST',
    url:'data/aedit_barang.php',
    data:'id_kategori='+barangID,
    success:function(html){
    $('#subkategorie').html(html);
    }
    });
    }else{
    $('#subkategorie').html('<option value="">Kosong</option>');
    }
    });
    });     
</script>