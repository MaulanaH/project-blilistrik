<?php
require_once '../../koneksi/koneksi.php';
header( "Content-type:application/vnd.ms-excel;" );
$name = "Barang".date("d-m-Y_H:i:s",strtotime('NOW')).".xls";

header( "Content-Disposition: attachment;filename=$name" );
?>
	<table border="1" valign = "middle" style="vertical-align: middle;">
		<thead align="center">
			<tr>
				<th align="center" valign="middle" colspan="13"  style="border-bottom: none">Laporan Stok barang Blilistrik</th>
			</tr>
			<tr>
				<th align="center" valign="middle" colspan="13" style="border-top: none"><?php echo date("d - M - Y ",strtotime("NOW")); ?></th>
			</tr>
			<tr>
				<th>No</th>
				<th>Kode barang</th>
				<th>Nama barang</th>
				<th>Brand</th>
				<th>Kategori</th>
				<th>Sub Kategori</th>
				<th>Deskripsi</th>
				<th>Harga</th>
				<th>Stok</th>
				<th>Satuan stok</th>
				<th>Berat</th>
				<th>Satuan Berat</th>
				<th>Status</th>
				<th>
					<table border="1" align="center">
						<tr>
							<th>Minimal pembelian diskon</th>
							<th>Diskon umum</th>
							<th>Diskon reseller</th>
							<th>komisi</th>
						</tr>
					</table>
				</th>
			</tr>
		</thead>
		<tbody>
			<?php
			$articles = "SELECT * FROM barang_detail";
			$result = mysqli_query( $koneksi, $articles );
			while ( $row = $result->fetch_assoc() ) {
				$start++;
				?>
			<tr>
				<td align=""><?php echo $start; ?></td>
				<td align=""><?php echo $row['kode_barang']; ?></td>
				<td align=""><?php echo $row['nama_barang']; ?></td>
				<td align=""><?php echo $row['nama_brand']; ?></td>
				<td align=""><?php echo $row['nama_kategori']; ?></td>
				<td align=""><?php echo $row['nama_sub_kategori']; ?></td>
				<td align=""><?php echo $row['deskripsi']; ?></td>
				<td align=""><?php echo $row['harga_barang']; ?></td>
				<td align="right"><?php echo $row['stok_barang']; ?></td>
				<td align="left"><?php echo $row['satuan_stok']; ?></td>
				<td align="right"><?php echo $row['berat_barang']; ?></td>
				<td align="left"><?php echo $row['satuan_berat']; ?></td>
				<td align="left"><?php echo $row['status']; ?></td>
				<td align="center">
					<table border="1" align="center">
					<?php 
					$articles1 = "SELECT * FROM diskon d left join diskon_reseller dr on d.id_diskon = dr.id_diskon where id_barang = ".$row['id_barang'];
					$result1 = mysqli_query( $koneksi, $articles1 );
				if($result1->num_rows==0){
						echo "<tr><td align='right'>-</td><td align='right'>-</td><td align='right'>-</td><td align='right'>-</td></tr>";
				}
					while ( $row1 = $result1->fetch_assoc() ) {
						echo "<tr><td align='right'>";
						echo $row1['qty']."</td><td align='right'>";
						echo $row1['id_reseller'] == null? $row1['diskon']."%" : "-";
						echo "</td><td align='right'>";
						echo $row1['id_reseller'] != null? $row1['diskon']."%" : "-";
						echo "</td><td align='right'>";
						echo $row1['id_reseller'] != null? $row1['komisi_reseller']."%" : "-";
						echo "</td></tr>";
					}
					?>
					</table>
				</td>
			</tr>
			<?php
			}
			?>
		</tbody>
	</table>