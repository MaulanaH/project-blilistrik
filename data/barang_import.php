<?php
require( '../../koneksi/spreadsheet-reader-master/php-excel-reader/excel_reader2.php' );
require( '../../koneksi/spreadsheet-reader-master/SpreadsheetReader.php' );
require_once '../../koneksi/koneksi.php';
//header( 'Content-Type: text/plain' );
$Filepath = basename( $_FILES[ 'file' ][ 'name' ] );
move_uploaded_file( $_FILES[ 'file' ][ 'tmp_name' ], $Filepath );

date_default_timezone_set( 'UTC' );
$StartMem = memory_get_usage();

$error = "";

$update = isset( $_POST[ 'update' ] ) ? true : false;
$drop = isset( $_POST[ 'drop' ] ) ? true : false;
if ( $drop ) {
	$koneksi->query( "truncate barang" );
	$koneksi->query( "truncate diskon" );
	$koneksi->query( "truncate diskon_reseller" );
	$pfiles = "../../img/177cbf2b2fda8daf8688bd68a5ea6e14";
	$files = glob($pfiles."/*");
	foreach ($files as $file) {
		if(is_file($file)){
			unlink($file);
		}
	}
}
try {
	$Spreadsheet = new SpreadsheetReader( $Filepath );
	$BaseMem = memory_get_usage();
	$Sheets = $Spreadsheet->Sheets();

	$Spreadsheet->ChangeSheet( 0 );
	$Time = microtime( true );
	$id_barang="";
	foreach ( $Spreadsheet as $Key => $Row ) {
		$keyx = $Key;
		if ( $Key >= 3 ) {

			 $Key . ': ';
			if ( $Row ) {
				//print_r( $Row );
			} else {
				//var_dump( $Row );
			}
			$kode ;
			$id_barang ;
				$kode_barang = $Row[ 1 ];
				$nama_barang = $Row[ 2 ];
				$brand = $Row[ 3 ];
				$kategori = $Row[ 4 ];
				$sub_kategori = $Row[ 5 ];
				$deskripsi = $Row[ 6 ];
				$harga_barang = $Row[ 7 ];
				$stok_barang = $Row[ 8 ];
				$satuan_stok = $Row[ 9 ];
				$berat = $Row[ 10 ];
				$satuan_berat = $Row[ 11 ];
				$minimal_pembelian = $Row[ 12 ];
				$diskon_umum = $Row[ 13 ] == "-" ? NULL : substr($Row[ 13 ],-1,1) =='%' ? str_replace('%','',$Row[ 13 ]) : $Row[ 13 ];
				$diskon_reseller = $Row[ 14 ] == "-" ? NULL : substr($Row[ 14 ],-1,1) =='%' ? str_replace('%','',$Row[ 14 ]) : $Row[ 14 ];
				$diskon_komisi = $Row[ 15 ] == "-" ? NULL : substr($Row[ 15 ],-1,1) =='%' ?str_replace('%','',$Row[ 15 ]) : $Row[ 15 ];
			if ( $kode_barang != null ) {
				$kode =  $kode_barang;
				$qbrand = $koneksi->query( sprintf( "SELECT id_brand FROM brand WHERE nama_brand LIKE '%s'", $brand ) );
				if ( $qbrand->num_rows == 0 ) {
					$koneksi->query( sprintf( "INSERT INTO `brand`(`nama_brand`) VALUES ('%s')", $brand ) );
					$id_brand = $koneksi->insert_id;
				} else {
					$rbrand = $qbrand->fetch_object();
					$id_brand = $rbrand->id_brand;
				}
				$qkategori = $koneksi->query( sprintf( "SELECT id_kategori FROM kategori WHERE nama_kategori LIKE '%s'", $kategori ) );
				if ( $qkategori->num_rows == 0 ) {
					$koneksi->query( sprintf( "INSERT INTO `kategori`(`nama_kategori`) VALUES ('%s')", $kategori ) );
					$id_kategori = $koneksi->insert_id;
				} else {
					$rkategori = $qkategori->fetch_object();
					$id_kategori = $rkategori->id_kategori;
				}
				$qkategori_sub = $koneksi->query( sprintf( "SELECT id_sub_kategori FROM kategori_sub WHERE nama_sub_kategori LIKE '%s'", $sub_kategori ) );
				if ( $qkategori_sub->num_rows == 0 ) {
					$koneksi->query( sprintf( "INSERT INTO `kategori_sub`(`id_kategori`,`nama_sub_kategori`) VALUES (%s,'%s')", $id_kategori, $sub_kategori ) );
					$id_kategori_sub = $koneksi->insert_id;
				} else {
					$rkategori_sub = $qkategori_sub->fetch_object();
					$id_kategori_sub = $rkategori_sub->id_sub_kategori;
				}
				$data = $koneksi->query("SELECT * FROM barang where kode_barang like '".$kode_barang."'");
				$dataa = $data->fetch_assoc();
				if($data->num_rows==0){
					$query = "INSERT INTO `barang`(`kode_barang`, `id_sub_kategori`, `id_brand`, `nama_barang`, `deskripsi`, `harga_barang`, `stok_barang`, `satuan_stok`, `berat_barang`, `satuan_berat`, `foto_barang`, `status`) VALUES ('$kode_barang','$id_kategori_sub','$id_brand','$nama_barang','$deskripsi', '$harga_barang','$stok_barang','$satuan_stok','$berat','$satuan_berat','..',1)";
					// $query . "<br>" . PHP_EOL;
				}else if($data->num_rows>0 && $update){
						$query = "UPDATE `barang` SET `id_sub_kategori` = '$id_kategori_sub', `id_brand` ='$id_brand', `nama_barang`='$nama_barang', `deskripsi`='$deskripsi', `harga_barang`='$harga_barang', `stok_barang`='$stok_barang', `satuan_stok`='$satuan_stok', `berat_barang`='$berat', `satuan_berat`='$satuan_berat' WHERE `kode_barang` ='$kode_barang'";
				}
				$hasil = $koneksi->query( $query ) or die($koneksi->error);
				if($update){
					$id_barang = $dataa['id_barang'];
				}else{
					$id_barang = $koneksi->insert_id;
				}
				if ($diskon_umum != null ) {
					if($update){
						$qdiskon = sprintf( "UPDATE `diskon` SET `diskon`=%s, `qty`=%s WHERE `id_barang`=%s ", $diskon_umum , $minimal_pembelian, $id_barang );
					}else{
						$qdiskon = sprintf( "INSERT INTO `diskon`(`id_barang`, `diskon`, `qty`) VALUES (%s,%s,%s)", $id_barang, $diskon_umum , $minimal_pembelian );
					}
					if($koneksi->query( $qdiskon )){
						
					}else{
						 echo "<script>alert('diskon reguler barang '$kode_barang' tidak tersimpan')</script>";
					}
				}
				if ($diskon_reseller != null ) {						
					if($update){
							$qdiskon = sprintf( "UPDATE `diskon` SET  `diskon`=%s,`jenis`='reseller', `qty`=%s, WHERE `id_barang`=%s ", $diskon_reseller, $minimal_pembelian, $id_barang );
							$qdiskon2 = sprintf( "UPDATE `diskon_reseller` SET  `id_reseller`=%s,`diskon`=%s,`diskonreseller`=%s, `qty`=%s, WHERE `id_barang`=%s ",'0', $diskon_reseller, $diskon_komisi , $minimal_pembelian, $id_barang );
							$koneksi->query( $qdiskon );
						}else{
							$qdiskon = sprintf( "INSERT INTO `diskon`(`id_barang`, `jenis` , `diskon`, `qty`) VALUES (%s,'reseller',%s,%s)", $id_barang, $diskon_reseller, $minimal_pembelian );
							$koneksi->query( $qdiskon );
							$id_diskon = $koneksi->insert_id;
							$qdiskon2 = sprintf( "INSERT INTO `diskon_reseller`(`id_diskon`, `id_reseller`, `komisi_reseller`) VALUES (%s,%s,%s)", $id_diskon, '0', $diskon_komisi );
						}
						// "<br>$qdiskon<br>";
					if($koneksi->query( $qdiskon2 )){

					}else{
						 echo "<script>alert('diskon reseller barang '$kode_barang' tidak tersimpan')</script>";
					}
				}

				if ( !$hasil ) {
					//die( $koneksi->error );
					 "Data " . $key . " gagal diimpor.<br>" . PHP_EOL;
				} else {
					 "Data berhasil diimpor.<br>" . PHP_EOL;
				}
				unlink( $_FILES[ 'file' ][ 'tmp_name' ] );

			} 
			else if ( $kode_barang == NULL ) {
				if ($diskon_umum != null ) {
					if($update){
						$qdiskon = sprintf( "UPDATE `diskon` SET `diskon`=%s, `qty`=%s WHERE `id_barang`=%s ", $diskon_umum , $minimal_pembelian, $id_barang );
					}else{
						$qdiskon = sprintf( "INSERT INTO `diskon`(`id_barang`, `diskon`, `qty`) VALUES (%s,%s,%s)", $id_barang, $diskon_umum , $minimal_pembelian );
					}
					if($koneksi->query( $qdiskon )){
						
					}else{
						 echo "<script>alert('diskon reguler barang '$kode' tidak tersimpan')</script>";
					}
				}
				if ($diskon_reseller != null  ) {						
					if($update){
							$qdiskon = sprintf( "UPDATE `diskon` SET  `diskon`=%s,`jenis`='reseller', `qty`=%s, WHERE `id_barang`=%s ", $diskon_reseller, $minimal_pembelian, $id_barang );
							$qdiskon2 = sprintf( "UPDATE `diskon_reseller` SET  `id_reseller`=%s,`diskon`=%s,`diskonreseller`=%s, `qty`=%s, WHERE `id_barang`=%s ",'0', $diskon_reseller, $diskon_komisi , $minimal_pembelian, $id_barang );
							$koneksi->query( $qdiskon );
						}else{
							$qdiskon = sprintf( "INSERT INTO `diskon`(`id_barang`, `jenis` , `diskon`, `qty`) VALUES (%s,'reseller',%s,%s)", $id_barang, $diskon_reseller, $minimal_pembelian );
							$koneksi->query( $qdiskon );
							$id_diskon = $koneksi->insert_id;
							$qdiskon2 = sprintf( "INSERT INTO `diskon_reseller`(`id_diskon`, `id_reseller`, `komisi_reseller`) VALUES (%s,%s,%s)", $id_diskon, '0', $diskon_komisi );
						}
						// "<br>$qdiskon<br>";
					if($koneksi->query( $qdiskon2 )){

					}else{
						 echo "<script>alert('diskon reseller barang '$kode' tidak tersimpan')</script>";
					}
				}

			} else {
				// "Data " . $key . " gagal diimpor.<br>" . PHP_EOL;
			}
		}
	}
	unlink($Filepath);
	 echo "<script>alert('Data berhasil diimpor.');window.location='../admin.php?page=barang'</script>";
} catch ( Exception $E ) {
	 $E->getMessage();
}
?>