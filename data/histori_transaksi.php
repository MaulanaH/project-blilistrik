<!-- Breadcrumb>
	<div class="breadcrumb-holder container-fluid">
		<ul class="breadcrumb">
			<li class="breadcrumb-item"><a href="admin.php">Home</a>
			</li>
			<li class="breadcrumb-item active">Barang</li>
		</ul>
	</div>
<!-- Forms Section-->
<section class="forms p-0">
	<div class="container-fluid m-0 p-0">
		<div class="row">
			<div class="col-lg-12">
				<div class="card">
					<div class="card-header d-flex align-items-center m-0 p-0">
						<nav class="navbar bg-white text-dark" style="min-width: 100%;z-index: 1;">
							<div class="container-fluid">
								<h2 class="no-margin-bottom">Historical Admin</h2>
							</div>
						</nav>
					</div>
					<div class="card-body">
            <?php
              $cari = '';
              $id = '';
              if ( $cari != '' ) {
                echo '<center>Menampilkan pencarian barang <b>' . $cari . '</b>, <a href="admin.php?page=barang">Klik disini</a> untuk menampilkan semua barang.</center> ';
              }else if($id!=''){
                echo '<center>Menampilkan kode barang <b>' . $id . '</b>, <a href="admin.php?page=barang">Klik disini</a> untuk menampilkan semua barang.</center> ';
              }
              
              if ( isset( $cari ) && $cari != null ) {
                $query .= " AND nama_barang LIKE '%$cari%'";
              } else if ( isset( $id ) && $id != null ) {
                $query .= " AND b.kode_barang = '" . $id . "'";
              }else{
                $query = "";
              }
            ?>
						<div class="table-responsive">
							<table class="table table-striped table-bordered" id="example">
                <thead align="center">
									<tr>
										<th>#</th>
                            <th >
                                user
                            </th>
                            <th >
                                barang
                            </th>
                            <th >
                                jumlah
                            </th>
                            <th >
                                harga
                            </th>
                            <th >
                                harga total
                            </th>
                            <th >
                                diskon%
                            </th>
                            <th >
                                harga total + diskon
                            </th>
                            <th >
                                komisi
                            </th>                            
                            <th>
                                Tanggal
                            </th>
                </thead>
                <tbody>
                      <?php
                      $perpage = 25; //per halaman
                      $page = isset($_GET["hal"]) ? (int)$_GET["hal"] : 1;         
                      $start = ($page > 1) ? ($page * $perpage) - $perpage :0;
                      $articles = sprintf("SELECT * FROM diskon d INNER JOIN barang b on d.id_barang=b.id_barang INNER JOIN transaksi_barang tb ON b.id_barang = tb.id_barang INNER JOIN transaksi t ON t.id_transaksi = tb.id_transaksi INNER JOIN users u ON u.id_user = t.id_user WHERE 0 group by t.id_transaksi order by tanggal_transaksi desc");
                      $limit = $articles." LIMIT $start, $perpage";
                      $tampil = mysqli_query($koneksi, $limit);
                      $result=mysqli_query($koneksi,$articles);
                      $total = mysqli_num_rows($result);
                      $pages = ceil($total/$perpage);
                      while ($rdiskon = $result->fetch_assoc()) {
                        $start++;
                      ?>
                      <tr>
                        <td align="center"><?php echo $start; ?></td>
                        <td class="px-2">
                                <?php echo $rdiskon['fname']; ?>
                            </td>
                            <td class="px-2">
                                <?php echo $rdiskon['nama_barang']; ?>
                            </td>
                            <td align="right" class="px-2">
                                <?php echo $rdiskon['jumlah']; ?>
                            </td>
                            <td align="right" class="px-2">
                                <?php echo "Rp. ".number_format($rdiskon['harga']); ?>
                            </td>
                            <td align="right" class="px-2">
                                <?php echo "Rp. ".number_format($rdiskon['harga']*$rdiskon['jumlah']); ?>
                            </td>
                            <td align="right" class="px-2">
                               <?php echo $rdiskon['diskon']; ?>%
                            </td>
                            <td align="right" class="px-2">
                              <?php echo "Rp. ".number_format($rdiskon['total']); ?>
                            </td>
                        <td align="right" class="px-2">
                            <?php echo "Rp. ".number_format(($rdiskon['diskonreseller']/100*$rdiskon['harga'])*$rdiskon['jumlah']); ?>
                        </td>
                            <td align="center" class="px-2">
                                <?php echo date("d M Y",strtotime($rdiskon['tanggal_transaksi'])); ?>
                            </td>
                      </tr>
                      <?php
                      } ?>
                      </table>
                    </div>
                    
                  </div>
                </div>
              </div>
            </div>
          </section>            


<script type="text/javascript">
$(document).ready(function() {
  
});
</script>