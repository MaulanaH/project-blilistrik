
<!-- Breadcrumb>
	<div class="breadcrumb-holder container-fluid">
		<ul class="breadcrumb">
			<li class="breadcrumb-item"><a href="admin.php">Home</a>
			</li>
			<li class="breadcrumb-item active">Barang</li>
		</ul>
	</div>
<!-- Forms Section-->
<section class="forms p-0">
	<div class="container-fluid m-0 p-0">
		<div class="row">
			<div class="col-lg-12">
				<div class="card">
					<div class="card-header d-flex align-items-center m-0 p-0">
						<nav class="navbar bg-white text-dark" style="min-width: 100%;z-index: 1;">
							<div class="container-fluid">
								<h2 class="no-margin-bottom">Historical Admin</h2>
							</div>
						</nav>
					</div>
					<div class="card-body">
            <?php
              $cari = '';
              $id = '';
              if ( $cari != '' ) {
                echo '<center>Menampilkan pencarian barang <b>' . $cari . '</b>, <a href="admin.php?page=barang">Klik disini</a> untuk menampilkan semua barang.</center> ';
              }else if($id!=''){
                echo '<center>Menampilkan kode barang <b>' . $id . '</b>, <a href="admin.php?page=barang">Klik disini</a> untuk menampilkan semua barang.</center> ';
              }
              
              if ( isset( $cari ) && $cari != null ) {
                $query .= " AND nama_barang LIKE '%$cari%'";
              } else if ( isset( $id ) && $id != null ) {
                $query .= " AND b.kode_barang = '" . $id . "'";
              }else{
                $query = "";
              }
            ?>
						<div class="table-responsive">
							<table class="table table-striped table-bordered" id="example">
                <thead align="center">
									<tr>
										<th>#</th>
                    <th>Username Admin</th>
                    <th>Keterangan</th>
                    <th>Link</th>
                    <th>Tanggal</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $articles = "SELECT *,b.id_histori AS id FROM histori_view b " . $query;
                  //$articles = "SELECT *,b.id_barang AS id FROM barang b " . $query;
                  $result = mysqli_query( $koneksi, $articles );
                  $total = mysqli_num_rows( $result );
                  if ( $total == 0 ) {
                    echo "<tr><td colspan='5' align='center'><h2>Tidak ada histori.<h2></tr></td>";
                  }
                  while ( $row = $result->fetch_assoc() ) {
                    $start++;
                    $id = md5( $_SESSION[ 'token' ] . $row[ 'kode_barang' ] . $row[ 'id' ] );
                    ?>
                    <tr>
                      <th scope="row">
                        <?php echo $start;?>
                      </th>
                      <td>
                        <?php echo $row['username'] ?>
                      </td>
                      <td>
                        <?php
                          switch ($row['jenis_histori']) {
                            case 'update':
                                $jenis = "mengubah";
                                break;
                            case 'insert':
                              $jenis = "menambahkan";
                              break;
                            case 'delete':
                              $jenis = "menghapus";
                              break;
                            default:
                                # code...
                                break;
                            }
                          if ($row['id_barang']!=null) {
                            $jenis2 = "Barang";
                            $link="barang";
                            $qid = "SELECT kode_barang from barang where id_barang = ".$row['id_barang'];
                            $rid = $koneksi->query($qid)->fetch_object(); 
                            $id_ket = $rid->kode_barang;
                          }else if ($row['id_diskon']!=null) {
                            $jenis2 = "Diskon";
                            $link="diskon";
                            $id_ket = $row['id_diskon'];
                          }else if ($row['id_diskonreseller']!=null) {
                            $jenis2 = "Diskon Reseller";
                            $link="diskon_reseller";
                            $id_ket = $row['id_diskon'];
                          }else if ($row['id_reseller']!=null) {
                            $jenis2 = "Reseller";
                            $link="reseller";
                            $id_ket = $row['id_reseller'];
                          }else if ($row['id_user']!=null) {
                            $jenis2 = "User";
                            $link="user";
                            $id_ket = $row['id_user'];
                          }else if ($row['id_transaksi']!=null) {
                            $jenis2 = "Transaksi";
                            $link="transaksi";
                            $qid = "SELECT kode_transaksi from transaksi where id_transaksi = ".$row['id_transaksi'];
                            $rid = $koneksi->query($qid)->fetch_object(); 
                            $id_ket = $rid->kode_transaksi;
                          }else if ($row['id_admin']!=null) {
                            $jenis2 = "Admin";
                            $link="admin";
                            $id_ket = $row['id_admin'];
                          }
                          else{
                            $jenis2 =null;
                            $link=null;
                            $id_ket =null;
                          }
                          echo " Telah ".$jenis." ".$jenis2;
                        ?>
                      </td>
                      <td>
                        <a href="?page=<?php echo $link; ?>&&id=<?php echo $id_ket; ?> ">Lihat <?php echo $jenis2; ?></a>
                      </td>
                      <td>
                        <?php echo date("d - M - Y, H:i",strtotime($row['tanggal']));?>
                      </td>
                    </tr>
                    <?php
                    }
                    ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>