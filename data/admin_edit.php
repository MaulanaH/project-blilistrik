<?php
require_once "../../koneksi/koneksi.php";
$id = $_GET[ 'id' ];
$token = $_SESSION[ 'token' ];
$where = sprintf( "WHERE md5(concat('%s',username,id_admin)) = '%s'", $_SESSION[ 'token' ], $id );
$query = $koneksi->query( "SELECT * FROM `admin` $where" );
$row = $query->fetch_assoc();
?>          
  <!-- modal  -->
  <div class="form-group row">
    <label class="col-sm-3 form-control-label">Nama<i style='font-size:10px;color:red;'>*</i></label>
    <div class="col-sm-9">
      <input type="text" placeholder="Nama" class="form-control" value="<?php echo $row['fnama'] ?>" name="fname">
    </div>
  </div>
  <div class="form-group row">
    <label class="col-sm-3 form-control-label">Username<i style='font-size:10px;color:red;'>*</i></label>
    <div class="col-sm-9">
      <input type="text" placeholder="Username" class="form-control" value="<?php echo $row['username'] ?>" name="username" >
    </div>
  </div>
  <div class="form-group row">
    <label class="col-sm-3 form-control-label">email</label>
    <div class="col-sm-9">
      <input type="text" placeholder="email" class="form-control" value="<?php echo $row['email'] ?>" name="email" readonly=true>
      <i style='font-size:10px;color:red;'>Email tidak dapat diganti</i>
    </div>
  </div>
  <div class="form-group row">
      <label class="col-sm-3 form-control-label">
          Jenis Kelamin<i style='font-size:10px;color:red;'>*</i>
      </label>
      <div class="btn-group col-sm-9" id="rd_jenkel">
          <label class="btn w-50 btn-outline-secondary form-control" for="pria">
              <input class="rd_jenkel" id="pria" name="jenkel" <?php if($row['jenkel']=="Pria"){echo 'checked="checked"';}?> required="" type="radio" value="pria">
                  Pria
              </input>
          </label>
          <label class="btn w-50 btn-outline-secondary form-control" for="wanita">
              <input class="rd_jenkel" id="wanita" name="jenkel" <?php if($row['jenkel']=="Wanita"){echo 'checked="checked"';} ?> required="" type="radio" value="wanita">
                  Wanita
              </input>
          </label>
      </div>
  </div>
      <div class="form-group">       
      <img alt="Card image cap" class="card-img-top NO-CACHE" src="asset/foto/<?php echo $row['id_admin']."_".$row['username'].".png"?>"  style="height: 190px; cursor: pointer;">
      </div>                                
      <div class="form-group ">
        <div class="custom-file">
          <input type="file" class="custom-file-input" id="customFile" accept="image/*" name="foto">
          <label class="custom-file-label" for="customFileLang">Silakan Pilih Foto<i style='font-size:10px;color:red;'>*</i></label>
        </div>
      </div> 
</div>    
<input type="hidden" name="form" value="update">
<input id="edit_id" type="hidden" value="<?php echo $id?>" style="display:none;" name="id">
      <script type="text/javascript">
        $(document).ready(function ()
          {           
              $('.NO-CACHE').attr('src',function () { return $(this).attr('src') + "?upload=" + Math.random() });
          }); 
      </script>                                