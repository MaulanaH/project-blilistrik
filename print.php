<?php
require_once '../koneksi/koneksi.php';
require('../ppdf/fpdf.php');
$ruser = chk_login();
$id = $_GET['id'];
// memanggil library FPDF

class PDF extends FPDF
{
    // Page header
    function Header()
    {
        // Logo
        //$this->Image('lobli1.png',10,6,30);
        // Arial bold 15
        $this->SetFont('Arial','B',15);
        // Move to the right
        $this->Cell(80);
        // Title
        $this->Cell(30,10,'BUKTI INVOICE ANDA',0,0,'C');
        // Line break
        $this->Line(10,20,200,20);
        $this->Ln(10);
    }

    // Page footer
    function Footer() 
    { 
        $this->SetY( -15 ); 

        $this->SetFont( 'Arial', '', 10 ); 

        $this->Cell(0,10,'',0,0,'L');
        $this->SetX($this->lMargin);
        $this->Cell(0,10,'Blilistrik.com',0,0,'C');
        $this->SetX($this->lMargin);
        $this->Cell( 0, 10, '', 0, 0, 'R' ); 
    } 
}
// intance object dan memberikan pengaturan halaman PDF

$pdf = new PDF('p','mm','A4');
$pdf->SetTitle("Blilistrik Invoice");
$pdf->AliasNbPages();
// membuat halaman baru
$pdf->AddPage();
// setting jenis font yang akan digunakan
 
//include 'koneksi.php';
//$row_user = $ruser['id_user'];
//$transaksi=mysqli_query($koneksi,sprintf("SELECT * FROM transaksi  WHERE id_user = '$row_user'"));
//$row_userr=mysqli_fetch_array($transaksi);
//echo "<script>alert('" . $row_userr['id_user'] . "');</script>";
//$row_tran=$row_userr['id_transaksi'];
//echo "<script>alert('" . $row_tran . "');</script>";
//$transaksii = $koneksi->query("SELECT * FROM transaksi_barang INNER JOIN transaksi ON transaksi.id_transaksi = transaksi_barang.id_transaksi WHERE transaksi_barang.id_transaksi= '$row_tran'") or die(get_error()); 
//$rowt=$transaksi->fecth_array();
//foreach ($_SESSION['user']['keranjang'] as $key => $value) {
$transaksi=$koneksi->query("SELECT * FROM transaksi_barang INNER JOIN transaksi ON transaksi_barang.id_transaksi = transaksi.id_transaksi INNER JOIN barang ON transaksi_barang.id_barang = barang.id_barang INNER JOIN users ON transaksi.id_user = users.id_user WHERE transaksi.id_transaksi = '$id'")or die(get_error());
//$roww = mysqli_fetch_array($transaksi);
//$totall = $roww['id_barang'];
$transaksi1 = $koneksi->query("SELECT * FROM transaksi WHERE id_transaksi=".$id);

$rtrans1 = $transaksi1->fetch_assoc();
$pdf->SetFont('courier','',10);
$date = new DateTime($rtrans1['tanggal_transaksi']);
$pdf->Cell(190,7,"".$date->format("d F Y"),0,1,'R');
// Memberikan space kebawah agar tidak terlalu rapat
$pdf->Ln(5);
$pdf->SetFont('Arial','B',10);
$pdf->Cell(10,6,'No',1,0,'C');
$pdf->Cell(95,6,'Nama Barang',1,0,'C');
$pdf->Cell(30,6,'Jumlah barang',1,0,'C');
$pdf->Cell(30,6,'Harga',1,0,'C');
$pdf->Cell(25,6,'Jumlah',1,1,'C');
 
$pdf->SetFont('courier','',10);
 

$no=0;

while ($row = mysqli_fetch_array($transaksi)){
$no++;
$jumlah =($row['harga_barang']*$row['jumlah']);
//$row = mysqli_fetch_array($transaksi);
$totall = $row['id_transaksi'];
$pdf->Cell(10,6,$no,1,0,'C');
$pdf->Cell(95,6,$row['nama_barang'],1,0);
$pdf->Cell(30,6,$row['jumlah'],1,0,'C');
$pdf->Cell(30,6,'Rp.'.number_format($row['harga_barang']),1,0,'C');
$pdf->Cell(25,6,'Rp.'.number_format($jumlah),1,1,'R');
$total = $row['total'];
}
$pdf->SetFont('Arial','B',10);
$pdf->Cell(135,6,'',1,0);
$pdf->Cell(30,6,'Total',1,0,'C');
$pdf->Cell(25,6,'Rp.'.number_format($total),1,1,'R');
$pdf->Output("I","Blilistrik Invoice ".($rtrans1['id_user'].$rtrans1['id_transaksi']."_".$rtrans1['tanggal_transaksi']).".pdf");

?>