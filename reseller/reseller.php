<?php
@session_start();
require_once "../koneksi/koneksi.php";
ob_start();
if(!(isset($_SESSION['id_reseller'])) || $_SESSION['id_reseller']== null){
  header("location: login.php");

}
$a = mysqli_query($koneksi,sprintf("SELECT * FROM reseller WHERE id_reseller = '%s'",$_SESSION['id_reseller'])) or die(get_error());
$upser = mysqli_fetch_assoc($a);
if(isset($_POST['form']) && $_POST['form'] == "update_foto"){
    $foto = move_uploaded_file($_FILES['foto']['tmp_name'], "../reseller/asset/foto/".md5($upser['id_reseller']."_".$upser['username']).".png");
    if ($foto){
      echo "<script>alert('update berhasil');window.location='reseller.php';</script> ";
    }else {
      echo "<script>alert('update gagal;');window.location='reseller.php';</script> ";
    }


  }
        
         ?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>reseller Blilistrik.com</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="asset/vendor/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="asset/vendor/font-awesome/css/font-awesome.min.css">
    <!-- Fontastic Custom icon font-->
    <link rel="stylesheet" href="asset/css/fontastic.css">
    <!-- Google fonts - Poppins >
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,700">
    <!-- theme stylesheet>
    <link rel="stylesheet" href="asset/css/style.default.css" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="asset/css/style.green.css">
    <!-- Favicon-->
    <link rel="shortcut icon" href="asset/img/favicon.ico">
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
    <!-- Javascript files-->
    <script src="asset/js/jquery-3.2.1.min.js"></script>   
    <script src="asset/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="asset/vendor/popper.js/umd/popper.min.js"> </script>
    <script src="asset/vendor/jquery.cookie/jquery.cookie.js"> </script>
    <script src="asset/vendor/chart.js/Chart.min.js"></script>
    <script src="asset/vendor/jquery-validation/jquery.validate.min.js"></script>
    <!--- <script src="asset/js/charts-home.js"></script> -->
    <!-- Main File-->
    <script src="asset/js/front.js"></script>
  </head>
  <body>

 
    <div class="page">
      <!-- Main Navbar-->
      <header class="header">
        <nav class="navbar">
          <div class="container-fluid">
            <div class="navbar-holder d-flex align-items-center justify-content-between">
              <!-- Navbar Header-->
              <div class="navbar-header">
                <!-- Navbar Brand --><a href="reseller.php" class="navbar-brand">
                  <div class="brand-text brand-big"><span>reseller</span><strong> Blilistrik.com</strong></div>
                  <div class="brand-text brand-small"><strong>reseller</strong></div></a>
                <!-- Toggle Button--><a id="toggle-btn" href="#" class="menu-btn active"><span></span><span></span><span></span></a>
              </div>
              <!-- Navbar Menu -->
              <ul class="nav-menu list-unstyled d-flex flex-md-row align-items-md-center">
                <!-- Notifications>
                <li class="nav-item dropdown"> <a id="notifications" rel="nofollow" data-target="#" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link"><i class="fa fa-bell-o"></i><span class="badge bg-red">12</span></a>
                  <ul aria-labelledby="notifications" class="dropdown-menu">
                    <li><a rel="nofollow" href="#" class="dropdown-item"> 
                        <div class="notification">
                          <div class="notification-content"><i class="fa fa-envelope bg-green"></i>You have 6 new messages </div>
                          <div class="notification-time"><small>4 minutes ago</small></div>
                        </div></a></li>
                    <li><a rel="nofollow" href="#" class="dropdown-item"> 
                        <div class="notification">
                          <div class="notification-content"><i class="fa fa-twitter bg-blue"></i>You have 2 followers</div>
                          <div class="notification-time"><small>4 minutes ago</small></div>
                        </div></a></li>
                    <li><a rel="nofollow" href="#" class="dropdown-item"> 
                        <div class="notification">
                          <div class="notification-content"><i class="fa fa-upload bg-orange"></i>Server Rebooted</div>
                          <div class="notification-time"><small>4 minutes ago</small></div>
                        </div></a></li>
                    <li><a rel="nofollow" href="#" class="dropdown-item"> 
                        <div class="notification">
                          <div class="notification-content"><i class="fa fa-twitter bg-blue"></i>You have 2 followers</div>
                          <div class="notification-time"><small>10 minutes ago</small></div>
                        </div></a></li>
                    <li><a rel="nofollow" href="#" class="dropdown-item all-notifications text-center"> <strong>view all notifications                                            </strong></a></li>
                  </ul>
                </li>
                <!-- Messages                        >
                <li class="nav-item dropdown"> <a id="messages" rel="nofollow" data-target="#" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link"><i class="fa fa-envelope-o"></i><span class="badge bg-orange">10</span></a>
                  <ul aria-labelledby="notifications" class="dropdown-menu">
                    <li><a rel="nofollow" href="#" class="dropdown-item d-flex"> 
                        <div class="msg-profile"> <img src="img/avatar-1.jpg" alt="..." class="img-fluid rounded-circle"></div>
                        <div class="msg-body">
                          <h3 class="h5">Jason Doe</h3><span>Sent You Message</span>
                        </div></a></li>
                    <li><a rel="nofollow" href="#" class="dropdown-item d-flex"> 
                        <div class="msg-profile"> <img src="img/avatar-2.jpg" alt="..." class="img-fluid rounded-circle"></div>
                        <div class="msg-body">
                          <h3 class="h5">Frank Williams</h3><span>Sent You Message</span>
                        </div></a></li>
                    <li><a rel="nofollow" href="#" class="dropdown-item d-flex"> 
                        <div class="msg-profile"> <img src="img/avatar-3.jpg" alt="..." class="img-fluid rounded-circle"></div>
                        <div class="msg-body">
                          <h3 class="h5">Ashley Wood</h3><span>Sent You Message</span>
                        </div></a></li>
                    <li><a rel="nofollow" href="#" class="dropdown-item all-notifications text-center"> <strong>Read all messages    </strong></a></li>
                  </ul>
                </li>
                <!-- Logout    -->
                <!--li class="nav-item"><a href="points.php" class="nav-link"><i class="fa fa-money"></i></a></li-->
                <li class="nav-item"><a href="logout.php" class="nav-link logout">Logout<i class="fa fa-sign-out"></i></a></li>
              </ul>
            </div>
          </div>
        </nav>
      </header>
      <div class="page-content d-flex align-items-stretch"> 
        <!-- Side Navbar -->
        <nav class="side-navbar">
           <?php
               if(isset($_SESSION['id_reseller']) && $_SESSION['id_reseller'] !=NULL){
              $quser = mysqli_query($koneksi,sprintf("SELECT * FROM reseller WHERE id_reseller = '%s'",$_SESSION['id_reseller'])) or die(get_error());
              $row = mysqli_fetch_assoc($quser)
              ?>
          <!-- Sidebar Header-->
          <div class="sidebar-header d-flex align-items-center">
            <a href="" data-toggle="modal" data-target="#foto">
            <div class="avatar"><img src="asset/foto/<?php echo md5($row['id_reseller']."_".$row['username']).".png"?>" alt="..." class="img-fluid rounded-circle NO-CACHE"></div></a>
            <div class="title">
              <p>Selamat Datang</p>
              <h1 class="h4"><?php echo $row["fname"];?></h1>
              
            </div>
          </div>
          <?php }?>
          <!-- Sidebar Navidation Menus--><span class="heading">Main</span>
          <ul class="list-unstyled kategori">
                    <li ><a href="reseller.php"> <i class="fa fa-home"></i>Home</a></li>
                    <li><a href="?page=profile"> <i class="fa fa-users"></i>Profile</a></li>
                    <li><a href="?page=referral"> <i class="fa fa-barcode"></i>Referral</a></li>
                    <li><a href="?page=komisi"> <i class="fa fa-bar-chart"></i>Komisi </a></li>
                    <li><a href="?page=diskon"> <i class="fa fa-bar-chart"></i>Diskon Aktif </a></li>
                    <li><a href="?page=histori_diskon"> <i class="fa fa-th-list"></i>Detail Komisi</a></li>
                    <li><a href="?page=histori_tarik"> <i class="fa fa-th-list"></i>Histori Tarik</a></li>
                    <!--<li><a href="?page="> <i class="fa fa-th-list"></i>histori masuk</a></li>-->
          </ul>
        </nav>
        <div class="content-inner">


      <?php
      $page = @$_GET['page'];
        if ($page == "profile"){
          include "data/profile.php";
        } else if ($page == "referral"){
          include "data/referral.php";
        }else if ($page == "komisi"){
          include "data/komisi.php";
        }else if ($page == "detail"){
          include "data/detail.php";
        }else if ($page == "diskon"){
          include "data/diskon.php";
        }else if ($page == "histori_diskon"){
          include "data/detail.php";
        }else if ($page == "histori_tarik"){
          include "data/histori_penarikan.php";
        }else if ($page == "histori_masuk"){
          include "data/histori_pemasukan.php";
        }else if ($page == ""){
           ?>     
          <header class="page-header">
            <div class="container-fluid">
              <h2 class="no-margin-bottom">Dashboard</h2>
            </div>
          </header>
          
          <section class="dashboard-counts">
            <div class="container-fluid">
              <div class="row bg-white has-shadow">
                <!-- Item -->
                <div class="col-xl-6 col-sm-6">
                  <div class="item d-flex align-items-center">
                    <div class="icon bg-green"><i class="icon-padnote"></i></div>
                    <div class="title"><span>Referal<br>terpakai</span>
                      <div class="">
                        <a href="?page=history">
                        <p style="margin: 0;padding: 0;font-size: 15px;">Lihat Sekarang</p></a>
                      </div>
                    </div>
                      <?php 
                      $querya=mysqli_query($koneksi,sprintf("SELECT * 
                      FROM diskon d 
                      join diskon_reseller dr on d.id_diskon = dr.id_diskon
                      INNER JOIN barang b on d.id_barang=b.id_barang 
                      INNER JOIN transaksi_barang tb ON b.id_barang = tb.id_barang 
                      INNER JOIN transaksi t ON t.id_transaksi = tb.id_transaksi 
                      INNER JOIN users u ON u.id_user = t.id_user  WHERE id_reseller = %s",$_SESSION['id_reseller']));
                      //$hasil=mysqli_num_rows($query);
                      //$data = array();
                      while(($rowa = mysqli_fetch_array($querya)) != null){
                          $dataa[] = $rowa; 
                        }
                      $countreseller = count($dataa);
                      ?>                           
                    <div class="number"><strong><?php echo "$countreseller" ?></strong></div>
                  </div>
                </div>
                <!-- Item -->
                <div class="col-xl-6 col-sm-6">
                  <div class="item d-flex align-items-center">
                    <div class="icon bg-red"><i class="icon-user"></i></div>
                    <div class="title"><span>Diskon<br>terpasang</span>
                      <div class="">
                        <a href="?page=diskon">
                        <p style="margin: 0;padding: 0;font-size: 15px;">Lihat Sekarang</p></a>
                      </div>
                    </div>
                      <?php 
                      $queryu=mysqli_query($koneksi,sprintf("SELECT * FROM diskon d 
                      join diskon_reseller dr on d.id_diskon = dr.id_diskon INNER JOIN barang b on d.id_barang=b.id_barang WHERE d.status = 1 AND id_reseller = %s",$_SESSION['id_reseller']));
                      //$hasil=mysqli_num_rows($query);
                      //$data = array();
                      while(($rowu = mysqli_fetch_array($queryu)) != null){
                          $datau[] = $rowu; 
                        }
                      $countuser = count($datau);
                      ?>                     
                    <div class="number"><strong><?php echo "$countuser" ?></strong></div>
                  </div>
                </div>
                <!-- Item >
                <div class="col-xl-3 col-sm-6">
                  <div class="item d-flex align-items-center">
                    <div class="icon bg-green"><i class="icon-padnote"></i></div>
                    <div class="title"><span>Jumlah<br>Produk</span>
                      <div class="">
                        <a href="?page=barang">
                        <p style="margin: 0;padding: 0;font-size: 15px;">Lihat Sekarang</p></a>
                      </div>
                    </div>
                      <?php 
                      $queryb=mysqli_query($koneksi,"SELECT * FROM barang");
                      //$hasil=mysqli_num_rows($query);
                      //$data = array();
                      while(($rowb = mysqli_fetch_array($queryb)) != null){
                          $datab[] = $rowb; 
                        }
                      $countbarang = count($datab);
                      ?>                     
                    <div class="number"><strong><?php echo "$countbarang" ?></strong></div>
                  </div>
                </div>
                <!-- Item >
                <div class="col-xl-3 col-sm-6">
                  <div class="item d-flex align-items-center">
                    <div class="icon bg-orange"><i class="icon-check"></i></div>
                    <div class="title"><span>Open<br>Cases</span>
                      <div class="progress">
                        <div role="progressbar" style="width: 50%; height: 4px;" aria-valuenow="{#val.value}" aria-valuemin="0" aria-valuemax="100" class="progress-bar bg-orange"></div>
                      </div>
                    </div>
                    <div class="number"><strong>50</strong></div>
                  </div>
                </div>
              </div>
            </div><-->
            
          </section>
<!-- slider bar -->
          <section class="feeds no-padding-top ">
            <div class="container-fluid">
              <div class="row">

              </div>
            </div>
          </section>
          <?php
        }else {
          echo "tidak ada";
          
        }
      ?>
          <!-- Page Footer-->
          <footer class="main-footer">
            <div class="container-fluid">
              <div class="row">
                <div class="col-sm-6">
                  <p>Blilistrik.com &copy; 2018</p>
                </div>
                <div class="col-sm-6 text-right text-lowercase" style="font-size: 0.7em">
                  <p>Creator and Design by <a href="https://bootstrapious.com/admin-templates" class="external">Bootstrapious</a></p>
                       <!-- Please do not remove the backlink to us unless you support further theme's development at https://bootstrapious.com/donate. It is part of the license conditions. Thank you for understanding :)-->
                   <p>Redesign by Team Mojokerto Developer</p>
                </div>
              </div>
            </div>
          </footer>
        </div>
      </div>
    </div>
    <!-- modal  -->

  <div id="foto" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
   <div role="document" class="modal-dialog">

      <div class="modal-content">
       
      <div class="modal-header">

        <h4 id="exampleModalLabel" class="modal-title">Foto Profile <?php echo $upser['id_reseller']?></h4>
        <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
      </div>
         <div class="modal-body">
          <img class="img-thumbnail NO-CACHE" src="asset/foto/<?php echo md5($upser['id_reseller']."_".$upser['username']).".png"?>" alt="Card image" style="width:100%;">
          <br>
   
         </div>
        <div class="modal-footer">
          <form action="" method="post" enctype="multipart/form-data" >           
                <div class="form-group ">
                    <label for="foto">Upload Foto</label>
                    <input type="file" name="foto" id="foto" accept="image/*" class="form-control-file" placeholder="foto">
                </div>           
              <button type="button" data-dismiss="modal" class="btn btn-secondary">Close</button>
              <button  class="btn btn-primary" name="foto">Save changes</button>

              <input type="hidden" name="form" value="update_foto">
          </form>          
        </div>           
       </div>
    </div>
  </div>






    <script type="text/javascript">
        $('#q').keydown(function(e){
          if (e.keyCode == 13){
            window.location.href = 'reseller.php?page=barang&halaman=1&q='+$('#q').val();
          }
        });
        $('#qa').keydown(function(e){
          if (e.keyCode == 13){
            window.location.href = 'reseller.php?page=reseller&halaman=1&qa='+$('#qa').val();
          }
        }); 
        $('#qu').keydown(function(e){
          if (e.keyCode == 13){
            window.location.href = 'reseller.php?page=user&halaman=1&qu='+$('#qu').val();
          }
        });                
        function edit_barang(id){
         // $('#myModal').modal('show');
          $('#edit_form').html("<center>Tunggu Sebentar...</center>");
          $('#edit_form').load('data/edit_barang.php?id='+id);
          $('#myModal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget) 
            var recipient = button.data('id')
            var modal = $(this)
            modal.find('.modal-title').text('Edit Barang ' + recipient)
            //modal.find('.modal-body input').val(recipient)
          })           
        }
        function hapus_barang(id) {
          
              var retVal = confirm("Apakah Anda Yakin?");
               if( retVal == true ){
             window.location.href = "reseller.php?page=barang&del=&id=" + id;
                  return true;
               }
               else{
                  window.location='reseller.php?page=barang&halaman=1&q=';
                  return false;
               }
        }
        function hapus_data(id) {
        var retVal = confirm("Apakah Anda Yakin?");
          if( retVal == true ){
          window.location.href = "reseller.php?page=reseller&del=" + id;
              return true;
           }
           else{
             window.location='reseller.php?page=reseller';
              return false;
               }
             } 
        function hapus_user(id) {
        var retVal = confirm("Apakah Anda Yakin?");
          if( retVal == true ){
          window.location.href = "reseller.php?page=user&del=" + id;
              return true;
           }
           else{
             window.location='reseller.php?page=user';
              return false;
               }
             }                      
        function edit_reseller(id){
       //   $('#myModal').modal('show');
          $('#edit_form').html("<center>Tunggu Sebentar...</center>");
          $('#edit_form').load('data/edit_reseller.php?id='+id);
          $('#myModal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget) 
            var recipient = button.data('id')
            var modal = $(this)
            modal.find('.modal-title').text('Edit reseller ' + recipient)
            //modal.find('.modal-body input').val(recipient)
$(document).ready(function ()
    {           
        $('.NO-CACHE').attr('src',function () { return $(this).attr('src') + "?upload=" + Math.random() });
    });          
          })
                       
        }
        function edit_user(id){
         // $('#myModal').modal('show');
          $('#edit_form').html("<center>Tunggu Sebentar...</center>");
          $('#edit_form').load('data/edit_user.php?id='+id);
          $('#myModal1').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget) 
            var recipient = button.data('id')
            var modal = $(this)
            modal.find('.modal-title').text('Edit User ' + recipient)
            //modal.find('.modal-body input').val(recipient)
          })           
        }    
    $(document).ready(function ()
    {           
        $('.NO-CACHE').attr('src',function () { return $(this).attr('src') + "?upload=" + Math.random() });
    });
//    var nods = document.getElementsByClassName('NO-CACHE');
//for (var i = 0; i < nods.length; i++)
//{
//    nods[i].attributes['src'].value += "?upload=" + Math.random();
//}

      function lihat_barang(idbarang){
          $('#barang_form').html("<center>Tunggu Sebentar...</center>");
          $.ajax({
            url: 'data/transaksi.php',
            type: 'POST',
            dataType: 'json',
            data: {'get': 'barang','id': idbarang},
            success: function(data){
              $('#barang_form').html(data.tabel);
            },
            error: function(xhr) {
              alert('Gagal mengambil detail barang!');
            }
          });
          
      }
      function lihat_alamat(iduser){
          $('#barang_form').html("<center>Tunggu Sebentar...</center>");
          $.ajax({
            url: 'data/transaksi.php',
            type: 'POST',
            dataType: 'json',
            data: {'get': 'alamat','id': iduser},
            success: function(data){
              $('#alamat_form').html('<textarea class="form-control" readonly="">'+data.alamat+'</textarea>');
            },
            error: function(xhr) {
              alert('Gagal mengambil alamat pembeli!');
            }
          });
          
      }
     
        function lihat_fota(id){
         // $('#myModal').modal('show');
          $('#edit_form').html("<center>Tunggu Sebentar...</center>");
          $('#edit_form').load('data/lifo.php?id='+id);
          $('#myModal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget) 
            var recipient = button.data('id')
            var modal = $(this)
            modal.find('.modal-title').text('Edit Barang ' + recipient)
            //modal.find('.modal-body input').val(recipient)
          })           
        }
      function edit_transaksi(id){
        $('#id-edit-transaksi').val(id);
      }

      function hapus_transaksi(id) {
         var retVal = confirm("Yakin Ingin Menghapus Transaksi Ini?");
         if( retVal == true ){
          //window.location.href = "?"+id;                  
              return true;
           } else {
              return false;
          }
      }
    </script>


  </body>
</html>