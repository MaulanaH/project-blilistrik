<?php
$quser = $koneksi->query(sprintf("SELECT * FROM reseller WHERE id_reseller = %s", $_SESSION['id_reseller'])) or die(get_error());
$row_user = mysqli_fetch_assoc($quser);
if (isset($_POST['form']) && $_POST['form'] != NULL) {
    switch ($_POST['form']) {
        case 'update':
            $GoTo   = "?page=profile";
            $error  = null;
            $update = $koneksi->query(sprintf("UPDATE `reseller` SET `fname`='%s',`jenkel`='%s',`ttl`='%s',`username`='%s',`email`='%s',`no_atm`='%s',`no_ktp`='%s',`telp`='%s',`alamat`='%s',`id_kota_kab`='%s',`kode_pos`='%s' WHERE `id_reseller`=%s", $_POST['fname'], $_POST['jenkel'], $_POST['ttl'], $_POST['username'], $_POST['email'], $_POST['no_atm'], $_POST['no_ktp'], $_POST['telp'], $_POST['alamat'], $_POST['kota_kab'], $_POST['kode_pos'], $_SESSION['id_reseller'])) or die(get_error());
            if ($update) {
                echo "<script>alert('update berhasil');window.location='?page=profile';</script> ";
            } else {
                echo "<script>alert('update gagal');window.location='?page=profile';</script> ";
            }
            break;
        case 'passwordbaru':
            $a = $koneksi->query(sprintf("SELECT password from `reseller` WHERE password = '%s' and id_reseller = '%s'",md5($_POST['oldpassword']),$_SESSION['id_reseller']));
            $rpwd = $a->fetch_assoc();
            if(mysqli_num_rows($a)){
              if ($_POST['password'] == $_POST['repassword']) {
                  $qupdate = mysqli_query($koneksi, sprintf("UPDATE `reseller` SET `password`='%s' WHERE password = '%s' and id_reseller = '%s'", md5($_POST['password']),md5($_POST['oldpassword']),$_SESSION['id_reseller']));
                  if ($qupdate) {
                      echo "<script>alert('berhasil')</script>";
                  } else {
                      echo "<script>alert('gagal')</script>";
                  }
              } else {
                  echo "<script>alert('password dan repassword tidak sama!')</script>";
              }
            }else {
              echo "<script>alert('password salah')</script>";
            }
            break;
        case 'update_foktp':
          $foto = move_uploaded_file($_FILES['foto']['tmp_name'],"../img/9efc4ac970619de711752d818c29884a/92849aeec004d64f3cdd984a81100ee7".md5("ktp".$_SESSION['id_reseller'].".png"));
            if ($foto){
              echo "<script>alert('update berhasil');window.location='?page=profile';</script> ";
            }else {
              echo "<script>alert('update gagal;');window.location='?page=profile';</script> ";
            }
          break;
          case 'update_forek':
            $foto = move_uploaded_file($_FILES['foto']['tmp_name'],"../img/9efc4ac970619de711752d818c29884a/ed3743d3a98e9167be3dae7c46e4852a".md5("rekening".$_SESSION['id_reseller'].".png"));
            if ($foto){
              echo "<script>alert('update berhasil');window.location='?page=profile';</script> ";
            }else {
              echo "<script>alert('update gagal;');window.location='?page=profile';</script> ";
            }
            break;
        default:
            # code...
            break;
    }

}
?>
          <!-- Breadcrumb-->
          <div class="breadcrumb-holder container-fluid">
            <ul class="breadcrumb">
              <li class="breadcrumb-item"><a href="index.html">Home</a></li>
              <li class="breadcrumb-item active">Profile</li>
            </ul>
          </div>
          <!-- Forms Section-->
          <section class="forms"> 
            <div class="container-fluid">
              <div class="row">
                <!-- Form Elements -->
                <div class="col-lg-12">
                  <div class="card">
                    <div class="card-header d-flex align-items-center m-0 p-0">
                    	<nav class="navbar" style="min-width: 100%;    z-index: 1;">
          	            <div class="container-fluid">
          	              <h2 class="no-margin-bottom">Halaman Profile</h2>
            		       		 <div class="card-close ml-2 mr-2">
                                  <div class="dropdown">
                                    <button type="button" id="closeCard4" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle text-white"><i class="fa fa-ellipsis-v"></i></button>
                                    <div aria-labelledby="closeCard4" class="dropdown-menu dropdown-menu-right has-shadow"><a href="#" class="dropdown-item remove"> <i class="fa fa-times"></i>Close</a><a href="#" class="dropdown-item edit"> <i class="fa fa-gear"></i>Edit</a></div>
                                  </div>
                                </div>
            	            </div>
                  	 </nav>
                    </div>
                    <div class="card-body">
                      <form class="form-horizontal" method="post" action="">
                        <div class="form-group row">
                          <label class="col-sm-3 form-control-label">Nama</label>
                          <div class="col-sm-9">
                            <input type="text" placeholder="placeholder" class="form-control" value="<?php echo $row_user['fname'] ?>" name="fname">
                          </div>
                        </div>
                        <div class="form-group row">
                          <label class="col-sm-3 form-control-label">Username</label>
                          <div class="col-sm-9">
                            <input type="text" placeholder="placeholder" class="form-control" value="<?php echo $row_user['username'] ?>" name="username" readonly>
                          </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 form-control-label">
                                Jenis Kelamin
                            </label>
                            <div class="btn-group col-sm-9" id="rd_jenkel">
                                <label class="btn w-50 btn-outline-secondary form-control" for="pria">
                                    <input class="rd_jenkel" id="pria" name="jenkel" <?php if($row_user['jenkel']=="Pria"){echo 'checked="checked"';}?> required="" type="radio" value="pria">
                                        Pria
                                    </input>
                                </label>
                                <label class="btn w-50 btn-outline-secondary form-control" for="wanita">
                                    <input class="rd_jenkel" id="wanita" name="jenkel" <?php if($row_user['jenkel']=="Wanita"){echo 'checked="checked"';} ?> required="" type="radio" value="wanita">
                                        Wanita
                                    </input>
                                </label>
                            </div>
                        </div>
                        <div class="form-group row">
                          <label class="col-sm-3 form-control-label">ttl</label>
                          <div class="col-sm-9">
                            <input type="text" placeholder="placeholder" class="form-control" value="<?php echo $row_user['ttl'] ?>" name="ttl">
                          </div>
                        </div>
                        <div class="form-group row">
                          <label class="col-sm-3 form-control-label">email</label>
                          <div class="col-sm-9">
                            <input type="text" placeholder="placeholder" class="form-control" value="<?php echo $row_user['email'] ?>" name="email">
                          </div>
                        </div>
                        <div class="form-group row">
                          <label class="col-sm-3 form-control-label">no_atm</label>
                          <div class="col-sm-9">
                            <input type="text" placeholder="placeholder" class="form-control" value="<?php echo $row_user['no_atm'] ?>" name="no_atm">
                          </div>
                        </div>
                        <div class="form-group row">
                          <label class="col-sm-3 form-control-label">no_ktp</label>
                          <div class="col-sm-9">
                            <input type="text" placeholder="placeholder" class="form-control" value="<?php echo $row_user['no_ktp'] ?>" name="no_ktp">
                          </div>
                        </div>
                        <div class="form-group row">
                          <label class="col-sm-3 form-control-label">telp</label>
                          <div class="col-sm-9">
                            <input type="text" placeholder="placeholder" class="form-control" value="<?php echo $row_user['telp'] ?>" name="telp">
                          </div>
                        </div>
                                                    <div class="form-group row">
                                <label class="col-sm-3 form-control-label">
                                    Provinsi
                                </label>
                                <div class="col-sm-9">
                                <select class="form-control" id="provinsi" name="provinsi">
                                  <option>Pilih Provinsi</option>
                                    <?php
                                    $kota = $row_user['id_kota_kab'];
                                    $query = $koneksi->query("SELECT * FROM kota_kab kk inner join provinsi p on kk.id_provinsi = p.id_provinsi WHERE kk.id_kota_kab='$kota'");
                                    $row = $query->fetch_array();
                                    $provinsi_id = $row['id_provinsi'];
                                    $kota_id = $row['id_kota_kab'];

                                    $query = $koneksi->query("SELECT * FROM provinsi");
                                    while ($row = $query->fetch_array()) {
                                      $selected = ($row['id_provinsi']==$provinsi_id) ? "SELECTED":"";
                                            echo '<option value="'.$row['id_provinsi'].'"'.$selected.'>'.$row['nama_provinsi'].'</option>';
                                    }
                                    ?>
                                </select>
                              </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 form-control-label">
                                    Kota/Kabupaten
                                </label>
                                <div class="col-sm-9">
                                <div id="kota_kab_div">
                                    <select class="form-control" id="kota_kab" name="kota_kab">
                                      <option>Pilih Kota/Kabupaten</option>
                                        <?php
                                          $query = $koneksi->query("SELECT * FROM kota_kab WHERE id_provinsi=$provinsi_id");
                                          while ($row = $query->fetch_array()) {
                                            $selected = ($row['id_kota_kab']==$kota_id) ? "SELECTED":"";
                                                  echo '<option value="'.$row['id_kota_kab'].'"'.$selected.'>'.$row['nama_kota_kab'].'</option>';
                                          }

                                        ?>
                                    </select>
                                  </div>
                                </div>
                            </div> 
                        <div class="form-group row">
                          <label class="col-sm-3 form-control-label">alamat</label>
                          <div class="col-sm-9">
                            <input type="text" placeholder="placeholder" class="form-control" value="<?php echo $row_user['alamat'] ?>" name="alamat">
                          </div>
                        </div>
                        <div class="form-group row">
                          <label class="col-sm-3 form-control-label">kode_pos</label>
                          <div class="col-sm-9">
                            <input type="text" placeholder="placeholder" class="form-control" value="<?php echo $row_user['kode_pos'] ?>" name="kode_pos">
                          </div>
                        </div>
                        <div class="line"></div>
                        <div class="form-group row">
                          <div class="col-12 offset-sm-3">
                            <input type="hidden" name="form" value="update">
                            <button type="submit" class="btn btn-primary" name="update">Save changes</button>
                            <button class="btn btn-outline-warning" type="button" data-toggle="modal" data-target="#modalpassword">Ganti Password</button>
                            <button class="btn btn-outline-warning" type="button" data-toggle="modal" data-target="#modalfoktp">Foto KTP</button>
                            <button class="btn btn-outline-warning" type="button" data-toggle="modal" data-target="#modalforek">Foto Buku Rekening</button>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>

<div id="modalpassword" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
    <div role="document" class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
              <h4 id="exampleModalLabel" class="modal-title">Ganti Password</h4>
              <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
            </div>
            <div class="col-lg-12">
                <form action="" method="POST">
                    <div class="form-group">
                      <label>Password Lama</label>
                      <input name="oldpassword" type="password" placeholder="Password..." class="form-control" value="">
                    </div>
                    <div class="form-group">
                      <label>Password baru</label>
                      <input name="password" type="password" placeholder="Password..." class="form-control" value="">
                    </div>
                    <div class="form-group">
                      <label>Tulis ulang Password baru</label>
                      <input name="repassword" type="password" placeholder="Password..." class="form-control" value="">
                    </div>
                    </div>  
                    <div class="modal-footer">
                    <input type="hidden" name="form" value="passwordbaru">
                      <button type="button" data-dismiss="modal" class="btn btn-secondary">Tutup</button>
                      <button type="submit" class="btn btn-primary" name="gantipassword">Ganti Password</button>
                    </div>
                </form>
            </div>
    </div>
</div>
<div class="modal fade" id="modalfoktp" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Ganti Foto ktp</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
         <div class="modal-body">
          <img class="img-thumbnail NO-CACHE" src="../img/9efc4ac970619de711752d818c29884a/92849aeec004d64f3cdd984a81100ee7<?php echo md5("ktp".$_SESSION['id_reseller'].".png")?>" alt="Card image" style="width:100%;height: 200px;">
          <br>
   
         </div>
        <div class="modal-footer">
          <form action="" method="post" enctype="multipart/form-data" >           
                <div class="form-group ">
                    <label for="foto">Ganti Baru</label>
                    <input type="file" name="foto" id="foto" accept="image/*" class="form-control-file" placeholder="foto" required="">
                </div>           
              <button type="button" data-dismiss="modal" class="btn btn-secondary">Close</button>
              <button  class="btn btn-primary" name="foto">Ganti Foto</button>

              <input type="hidden" name="form" value="update_foktp">
          </form>          
        </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modalforek" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Ganti Foto rekening</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
         <div class="modal-body">
          <img class="img-thumbnail NO-CACHE" src="../img/9efc4ac970619de711752d818c29884a/ed3743d3a98e9167be3dae7c46e4852a<?php echo md5("rekening".$_SESSION['id_reseller'].".png")?>" alt="Card image" style="width:100%;height: 200px;">
          <br>
   
         </div>
        <div class="modal-footer">
          <form action="" method="post" enctype="multipart/form-data" >           
                <div class="form-group ">
                    <label for="foto">Ganti Baru</label>
                    <input type="file" name="foto" id="foto" accept="image/*" class="form-control-file" placeholder="foto" required="">
                </div>           
              <button type="button" data-dismiss="modal" class="btn btn-secondary">Close</button>
              <button  class="btn btn-primary" name="foto">Ganti Foto</button>
              <input type="hidden" name="form" value="update_forek">
          </form>          
        </div>
    </div>
  </div>
</div> 
<script type="text/javascript">
$(document).ready(function() {
  
});
</script>