<?php
$quser = $koneksi->query(sprintf("SELECT * FROM reseller WHERE id_reseller = %s", $_SESSION['id_reseller'])) or die(get_error());
$row_user = mysqli_fetch_assoc($quser);
if (isset($_POST['form']) && $_POST['form'] != NULL) {
    switch ($_POST['form']) {
        default:
            # code...
            break;
    }

}
?>
          <!-- Breadcrumb-->
          <div class="breadcrumb-holder container-fluid">
            <ul class="breadcrumb">
              <li class="breadcrumb-item"><a href="index.html">Home</a></li>
              <li class="breadcrumb-item active">Profile</li>
            </ul>
          </div>
          <!-- Forms Section-->
          <section class="forms"> 
            <div class="container-fluid">
              <div class="row">
                <!-- Form Elements -->
                <div class="col-lg-12">
                  <div class="card">
                    <div class="card-header d-flex align-items-center m-0 p-0">
                    	<nav class="navbar" style="min-width: 100%;    z-index: 1;">
          	            <div class="container-fluid">
          	              <h2 class="no-margin-bottom">Halaman Histori</h2>
            		       		 <div class="card-close ml-2 mr-2">
                                  <div class="dropdown">
                                    <button type="button" id="closeCard4" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle text-white"><i class="fa fa-ellipsis-v"></i></button>
                                    <div aria-labelledby="closeCard4" class="dropdown-menu dropdown-menu-right has-shadow"><a href="#" class="dropdown-item remove"> <i class="fa fa-times"></i>Close</a><a href="#" class="dropdown-item edit"> <i class="fa fa-gear"></i>Edit</a></div>
                                  </div>
                                </div>
            	            </div>
                  	 </nav>
                    </div>
                    <div class="card-body">
                      <table border="1" class="w-100">
                          <head>
                            <tr> 
                                <th>Nominal</th>
                                <th>Status</th>
                                <th>Tgl</th>
                            </tr>

                          </head>
                      <?php
                        $qkom = $koneksi->query(sprintf("SELECT * FROM `reseller_komisi_ambil` WHERE id_reseller = %s",$_SESSION['id_reseller']));
                       // $rkom = $qkom->fetch_object();
                      while ($kom = $qkom->fetch_assoc()) {
                      ?>
                      <tr>
                          <td>
                              <?php echo "Rp. ".number_format($kom['nominal'],0,',','.');?>
                          </td>
                          <td>
                              <?php 
                              if ($kom['status'] == 'sudah') {
                                  echo 'Ditarik';
                              } elseif ($kom['status'] == 'proses penarikan') {
                                  echo "Proses Penarikan";
                              }
                              ?>
                          </td>
                          <td>
                              <?php echo $kom['tanggal'];?>
                          </td>
                      </tr>
                      <?php
                      } ?>
                      </table>
                    </div>
                    <div class="card-footer">
                                <div class="col-lg-12 <?php if($pages<=1){echo "d-none";} ?>">
                <nav aria-label="Page navigation example">
                    <ul class="pagination justify-content-end">
                    <?php
                    if($page>1){                        
                              $link_prev = "?page=histori&hal=".($page-1);
                            }else{
                                $link_prev='#';
                            }?>
                        <li class="page-item">
                            <a class="page-link" href="<?php echo $link_prev;?>">
                                <span aria-hidden="true">
                                    «
                                </span>
                                <span class="sr-only">
                                    Previous
                                </span>
                            </a>
                        </li>
                        <?php for ($i=1; $i<=$pages; $i++) { 
                            if($page==$i){
                                $active = "active";
                            }else{
                                $active = "";
                            }

                                ?>
                        <li class="page-item <?php echo $active ?>">
                            <a class="page-link" href="?page=histori&hal=<?php echo $i;?>">
                                <?php echo $i; ?>
                            </a>
                        </li>
                        <?php }
                  if($page<$pages){
                    $link_next = "?page=histori&hal=".($page+1);
                        }else{
                          $link_next='#';
                      }?>
                        <li class="page-item">
                            <a class="page-link" href="<?php echo $link_next;?>">
                                <span aria-hidden="true">
                                    »
                                </span>
                                <span class="sr-only">
                                    Next
                                </span>
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>            


<script type="text/javascript">
$(document).ready(function() {
  
});
</script>