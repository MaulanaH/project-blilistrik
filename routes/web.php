<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group([], function(){
	Route::get('/excel/barang', 'ExcelController@index')->name('excel');;
	Route::post('/excel/barang/import', 'ExcelController@import')->name('excel.import');
	Route::post('/excel/barang/export', 'ExcelController@export')->name('excel.export');
});

route::get('dev', function(){
	\App\Models\Barang::truncate();
	\App\Models\Brand::truncate();
	\App\Models\Kategori::truncate();
	\App\Models\SubKategori::truncate();
});