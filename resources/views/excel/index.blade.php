<!DOCTYPE html>
<html>
<head>
	<title>IMPORT</title>
</head>
<body>
	<h1>Impor Excel</h1>
	<form method="POST" action="{{ route('excel.import') }}" enctype="multipart/form-data">
		@csrf
		<b>Perhatian</b><br>
		<ul>
		    <li>
		    	Unduh dahulu contoh projek impor excel, 
		    	<a href="{{ asset('assets/file/example-import.xlsx') }}">klik disini</a>
		    </li>
		    <li>
		    	Isi sesuai dengan contoh projek excel tersebut
		    </li>
		    <li>
		    	Pastikan tidak ada yang satupun kolom yang kosong / tidak terisi, karena dapat menyebabkan error.
		    </li>
		    <li>
		    	Isi Kategori dan Sub Kategori sesuai dengan Kategori Yang Sudah Dibuat. Perhatikan besar kecilnya huruf karena kami membedakan besar atau kecilnya huruf. Contoh komputer dan Komputer akan kami anggap berbeda meski hanya perbedaan besar kecil huruf k saja.
		    </li>
		    <li>
		    	Jika Kategori dan SubKategori yang ditulis tidak ada di database, maka kami akan langsung otomatis membuat Kategori atau SubKategori tersebut.
		    </li>
		    <li>	
		    	Jangan Ada Formatting Apa Pun Di dokument Excel anda. Pastikan semua dokumen Excel clean plain text.
		    </li>
		    <li>
		    	Mencegah kerusakan data yang telah ada karena impor ini, lakukan backup data dari database maupun dengan metode
		    	expor dibawah.
		    </li>
		    <li>
		    	Simpan dengan format Excel 2003 (.xls) atau Excel 2013 (.xlsx)
		    </li>
		    <li>
		    	Klik Impor
		    </li>
		</ul>
		<b>Opsi Lain :</b>
		<div style="margin-bottom: 10px;">
			<input type="checkbox" name="truncate" id="truncate">
			<label for="truncate">Hapus Semua Data Sebelum Import.</label>
		</div>

		<b>Excel File :</b><br>
		<input type="file" name="excel_file">		

		<br><hr><br>
		<button type="submit">Impor Sekarang</button>
	</form>

</body>
</html>