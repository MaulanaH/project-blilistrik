<?php 
require_once "koneksi/koneksi.php";

if (isset($_SESSION['user']['login']['username'])) {
        $username = $_SESSION['user']['login']['username'];
        $email = $_SESSION['user']['login']['email'];
  $login = $koneksi->query("SELECT * FROM  users where username='$username' OR email='$email'") or die(get_error());
        $ketemu = mysqli_num_rows($login);
         if ($ketemu == 1) {
            header("location:../index.php ");
          }else{
            header("location:logout.php ");
          }
}
    else if (isset($_POST['form']) && $_POST['form'] == "reset") {
  $sql =sprintf("SELECT * FROM users WHERE password LIKE '%s' ",$_POST['password']);
  $res = mysqli_query($koneksi, $sql);
  $count = mysqli_num_rows($res);
  $row_user = mysqli_fetch_assoc($res);
  $repass = md5($_POST['repass']);
  if($row_user){
    $query = sprintf("UPDATE `users` SET `password`='%s' WHERE id_user = '%s'", $repass,  $row_user['id_user']);
    $res = mysqli_query($koneksi, $query) or die(mysqli_error($koneksi));
  if ($res){
        echo "<script>alert('Password Anda Telah Anda Reset Silakan Untuk Login');window.location='user/login.php';</script> ";
  } 
  }else{
      echo "<script>alert('Password Yang Anda  Masukan Salah');window.location='reset.php';</script> ";
  }
    
     
    }
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content='####################' name='google-site-verification'/>
  <meta content='####################' name='msvalidate.01'/>
  <meta content='####################' name='alexaVerifyID'/>
  <meta content='index, follow, noodp, noydir' name='robots'/>
  <meta content='id' name='geo.country'/>
  <meta content='TMD' name='author'/>
  <meta content='1 days' name='revisit-after'/>
  <meta content='Indonesia' name='geo.placename'/>
  <meta content='ecommerce' name='generator'/>
  <meta content='general' name='rating'/>
  <meta content='index, follow, snipet' name='googlebot'/>
  <meta content='id_id' property='og:locale'/>
  <meta content='website' property='og:type'/>
  <meta content='Selamat Datang Di Blilistrik' name='keywords'/>
  <meta name="google-site-verification" content="googleb8be6259d98e95ac"/>
  <meta name="Description" CONTENT="Selamat Datang di Blilistrik, Situs yang menjual berbagai macam barang atau produk elektronik sesuai kebutuhan, jika ada kesulitan tolong hubungi pihak blilistrik">
  <link rel="icon" href="<?php echo $root_base?>img/logo/lobli2.png">
  <meta name="viewport" content="width=device-width, initial-scale=1.0 shrink-to-fit=no"/>
  <meta charset="utf-8" name="theme-color" content="#ffc107">
  <link href="<?php echo $root_base?>assets/css/bootstrap.min.css" rel="stylesheet" media="screen,projection"/>
  <!--link href="<?php echo $root_base?>../assets/css/materialize.min.css" rel="stylesheet"/-->
  <link href="<?php echo $root_base?>assets/fonts/material-icons.css" rel="stylesheet"/>
  <title>Reset Password - Blilistrik</title>

</head>

  <body>
  <div class="container bg-transparent">
    <div class="card card-login  mx-auto mt-5">
      <div class="card-header card-header-merah">Halaman Reset Password
          <i id="loader" class="fa fa-circle-o-notch fa-spin" style="font-size:20px;color:#fff;float: right;display: none">
          </i>          
        </div>
      <div class="card-body card-body-putih">
          <form action="" id="frm_reset" method="post">
            <div class="form-group">
                <label for="password">Password Lama</label>
                <input type="password" name="password" id="password" class="form-control" placeholder="********" required value="">
            </div>
          <div class="form-group">
                <label for="repass">Password Baru</label>
                <input type="password" name="repass" id="repass" class="form-control" placeholder="********" required value="">
            </div>
          <input type="hidden" name="form" value="reset">
        <input type="submit" class="btn btn-merah btn-block" value="Lanjut">
        </form>
        <br>
        <div class="text-center">
          <p>Belum Punya Akun ?<a class=" large mt-3" href="user/register.php"> Daftar Baru</a><br>
              <a href="<?php echo $root_base?>"><i class="fa fa-home"></i> kembali ke Halaman Utama</a>
            </p>
        </div>
      </div>
    </div>
  </div>
  <!-- Bootstrap core JavaScript-->
    <script type="text/javascript" src="<?php echo $root_base?>assets/js/jquery.js"></script> 
      <script type="text/javascript" src="<?php echo $root_base?>assets/js/vendor/popper.min.js"></script>
      <!--pemanggilan bootstrap js -->
      <script type="text/javascript" src="<?php echo $root_base?>assets/js/bootstrap.min.js"></script>
      <script type="text/javascript" src="<?php echo $root_base?>assets/js/jquery.elevateZoom-3.0.8.min.js"></script> 
  <script>
      $(document).ready(function(e){
	  	//on submit
      	$('#frm_reset').validate({
	  		rules:{
				password:{required:true,minlength:8},
				repass:{required:true,minlength:8},
			},
			errorElement: "small",
			errorPlacement: function ( error, element ) {
					// Add the `help-block` class to the error element
					error.addClass( "text-danger form-control-feedback" );

					// Add `has-feedback` class to the parent div.form-group
					// in order to add icons to inputs
					//element.parents( ".col-sm-12" ).addClass( "has-danger" );

					if ( element.prop( "type" ) === "checkbox" ) {
						error.insertAfter( element.parent( "label" ) );
					} else {
						error.insertAfter( element );
					}

					// Add the span element, if doesn't exists, and apply the icon classes to it.
					//if ( !element.next( "i" )[ 0 ] ) {
					//	$( "<i class='fa fa-times-circle form-control-feedback' aria-hidden='true'></i>" ).insertAfter( element );
					//}
				},
			success: function ( label, element ) {
					// Add the span element, if doesn't exists, and apply the icon classes to it.
					//if ( !$( element ).next( "i" )[ 0 ] ) {
					//	$( "<i class='fa fa-check-circle ' aria-hidden='true'></i>" ).insertAfter( $( element ) );
					//}
				},
			highlight: function ( element, errorClass, validClass ) {
					$( element ).parents( ".col-sm-12" ).addClass( "has-danger" ).removeClass( "has-success" );
					$('.form-control').addClass('form-control-danger');
					//$( element ).next( "i" ).addClass( "fa-times-circle" ).removeClass( "fa-check-circle" );
				},
			unhighlight: function ( element, errorClass, validClass ) {
					$( element ).parents( ".col-sm-12" ).addClass( "has-success" ).removeClass( "has-danger" );
					$('.form-control').removeClass('form-control-danger');
					//$( element ).next( "i" ).addClass( "fa-check-circle" ).removeClass( "fa-times-circle" );
				},
	  	});
    });
  </script>

</body>
</html>