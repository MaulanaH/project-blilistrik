<?php 
    require_once '../header.php';
    $ruser=chk_login();
    if(isset( $_POST[ 'form' ] ) && $_POST[ 'form' ] != NULL ) {
        if(isset($_POST['update'])){
            $id = $ruser['id_user'];
            $fname = $_POST['fname'];
            $jenkel = $_POST['jenkel'];
            $ttl = $_POST['ttl'];
            $email = $_POST['email'];
            $telp = $_POST['telp'];
            $kodepos = $_POST['kodepos'];
            $provinsi = $_POST['provinsi'];
            $kota_kab = $_POST['kota_kab'];
            $alamat = $_POST['alamat'];
            $query = $koneksi->query("UPDATE users SET fname='$fname',jenis_kelamin='$jenkel',tempat_tanggal_lahir='$ttl',email='$email',telp='$telp',alamat='$alamat',kode_pos='$kodepos', alamat='$alamat',id_kota_kab='$kota_kab' WHERE id_user = '$id'");
            if ($query) {}   
        } else if(isset($_POST['form']) && $_POST['form'] == "passwordbaru"){
            $id = $ruser['id_user'];
            if ($_POST['password'] == $_POST['repassword']) {
                $qupdate = mysqli_query($koneksi,sprintf("UPDATE `users` SET `password`='%s' WHERE id_user = '$id'",md5($_POST['password']),$_SESSION['id_user']));
                if ($qupdate) {
                    echo "<script>alert('berhasil')</script>";
                }else{
                    echo "<script>alert('gagal')</script>";
                }
            } else {
                echo "<script>alert('password dan repassword tidak sama!')</script>";
            }
        }else if(isset($_POST['form']) && $_POST['form'] == "update_fopro"){
            $foto = move_uploaded_file($_FILES['foto']['tmp_name'],"../img/ee11cbb19052e40b07aac0ca060c23ee/".$ruser['id_user'].".png");
            if ($foto){
              echo "<script>alert('update berhasil');window.location='profile.php';</script> ";
            }else {
              echo "<script>alert('update gagal;');window.location='profile.php';</script> ";
            }
        }
    }
?>
<div class="container-fluid mb-4" style="width: 100%;padding: 0;margin-top: 80px;">
    <div class="row" style="margin: unset;">
        <div class="col-lg-2">
            <?php get_sidebar(); ?>
        </div>
        <div class="col-lg-10">
            <div class="card card-register mx-auto card-body-putih">
                <form action="" method="post">
                    <div class="card-header bg-biru text-white h4">
                        Edit Profil
                    </div>
                    <div class="card-body card-body-putih">
                        <div class=" form-row">
                            <div class="form-group col-lg-4 col-sm-12">
                                <label for="fname">
                                    Nama
                                </label>
                                <input class="form-control" id="fname" name="fname" placeholder="Diponegoro" required="" type="text" value="<?php echo $ruser['fname'] ?>">
                                </input>
                            </div>
                            <div class="col-lg-4 col-sm-12">
                                <div class="form-group">
                                    <label>
                                        Jenis Kelamin
                                    </label>
                                    <div class="btn-group w-100" id="rd_jenkel">
                                        <label class="btn w-50 btn-outline-secondary form-control" for="pria">
                                            <input class="rd_jenkel" id="pria" name="jenkel" <?php if($ruser['jenis_kelamin']=="Pria"){echo 'checked="checked"';}?> required="" type="radio" value="pria">
                                                Pria
                                            </input>
                                        </label>
                                        <label class="btn w-50 btn-outline-secondary form-control" for="wanita">
                                            <input class="rd_jenkel" id="wanita" name="jenkel" <?php if($ruser['jenis_kelamin']=="Wanita"){echo 'checked="checked"';} ?> required="" type="radio" value="wanita">
                                                Wanita
                                            </input>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-lg-4 col-sm-12">
                                <label for="ttl">
                                    Tempat, Tanggal Lahir
                                </label>
                                <input class="form-control" id="ttl" name="ttl" placeholder="Jakarta, 17-agustus-1945" required="" type="text" value="<?php echo  $ruser['tempat_tanggal_lahir'] ?>">
                                </input>
                            </div>
                            <div class="form-group col-lg-4 col-sm-12">
                                <label for="email">
                                    Email
                                </label>
                                <input class="form-control" id="email" name="email" placeholder="contoh@contoh.com" required="" type="email" value="<?php echo  $ruser['email'] ?>">
                                </input>
                            </div>
                            <div class="form-group col-lg-4 col-sm-12">
                                <label for="telp">
                                    No Telp (WhatsApp)
                                </label>
                                <input class="form-control" id="telp" name="telp" placeholder="08xxxxxxxxx" required="" type="tel" value="<?php echo  $ruser['telp'] ?>">
                                </input>
                            </div>
                            <div class="form-group col-lg-4 col-sm-12">
                                <label for="facebook">
                                    Kode Pos
                                </label>
                                    <input class="form-control" id="kodepos" name="kodepos" placeholder="62233441" required="" type="text" value="<?php echo  $ruser['kode_pos'] ?>">
                                    </input>
                            </div>
                            <div class="form-group col-lg-4 col-sm-12">
                                <label for="provinsi">
                                    Provinsi
                                </label>
                                <select class="form-control" id="provinsi" name="provinsi">
                                    <?php
                                    $kota = $ruser['id_kota_kab'];
                                    $query = $koneksi->query("SELECT * FROM kota_kab WHERE id_kota_kab='$kota'");
                                    $row = $query->fetch_array();
                                    $provinsi_id = $row['id_provinsi'];
                                   
                                    $query = $koneksi->query("SELECT * FROM provinsi WHERE id_provinsi='$provinsi_id'");
                                    $row = $query->fetch_array();
                                    echo '<option value="'.$row['id_provinsi'].'">'.$row['nama_provinsi'].'</option>';
                                       
                                        $query = $koneksi->query("SELECT * FROM provinsi");
                                        while ($row = $query->fetch_array()) {
                                            if ($provinsi_id != $row['id_provinsi']) {
                                                echo '<option value="'.$row['id_provinsi'].'">'.$row['nama_provinsi'].'</option>';
                                            }
                                        
                                        }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group col-lg-4 col-sm-12">
                                <label for="kota">
                                    Kota/Kabupaten
                                </label>
                                <div id="kota_kab_div">
                                    <select class="form-control" id="kota_kab" name="kota_kab">
                                        <?php
                                        $kota = $ruser['id_kota_kab'];

                                        $query = $koneksi->query("SELECT * FROM kota_kab WHERE id_kota_kab='$kota'");
                                        $row = $query->fetch_array();
                                        $provinsi = $row['id_provinsi'];
                                        echo '<option value="'.$row['id_kota_kab'].'">'.$row['nama_kota_kab'].'</option>';

                                        $query = $koneksi->query("SELECT * FROM kota_kab WHERE id_provinsi='$provinsi'");
                                        while($row = $query->fetch_array()){
                                            if ($row['id_kota_kab'] != $kota) {
                                                echo '<option value="'.$row['id_kota_kab'].'">'.$row['nama_kota_kab'].'</option>';
                                            }
                                        }

                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group col-lg-4 col-sm-12">
                                <label for="alamat">
                                    Alamat Lengkap
                                </label>
                                <textarea class="form-control" id="alamat" name="alamat" placeholder="alamat" required="" type="text" value=""><?php echo  $ruser['alamat'] ?></textarea>
                            </div>
                            <input name="form" type="hidden" value="update">
                                <!-- <div class="form-group col-lg-4 col-sm-12" type="hidden">
                          <label for="doc_kk">Doc_kk</label>
                          <input type="file" name="doc_kk" id="doc_kk" class="form-control-file" placeholder="doc_kk" required value="">
                        </div> -->
                            </input>
                        </div>
                    </div>
                    <div class="card-footer card-body-putih">
                        <div class="text-right">
                            <button class="btn btn-outline-warning" type="button" data-toggle="modal" data-target="#modalfopro">Ganti Foto Profil</button>
                            <button class="btn btn-outline-warning" type="button" data-toggle="modal" data-target="#modalpassword">Ganti Password</button>
                            <button class="btn btn-outline-primary" type="submit" name="update" value="Ubah Profil">
                                Simpan Profil
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="modalpassword" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
    <div role="document" class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
              <h4 id="exampleModalLabel" class="modal-title">Ganti Password</h4>
              <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
            </div>
            <div class="col-lg-12">
                <form action="" method="post">
                    <div class="form-group">
                      <label>Password</label>
                      <input name="password" type="password" placeholder="Password..." class="form-control" value="">
                    </div>
                    <div class="form-group">
                      <label>Tulis ulang Password</label>
                      <input name="repassword" type="password" placeholder="Password..." class="form-control" value="">
                    </div>
                    </div>  
                    <div class="modal-footer">
                    <input type="hidden" name="form" value="passwordbaru">
                      <button type="button" data-dismiss="modal" class="btn btn-secondary">Tutup</button>
                      <button type="submit" class="btn btn-primary" name="gantipassword">Ganti Password</button>
                    </div>
                </form>
            </div>
    </div>
</div>

<div class="modal fade" id="modalfopro" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Ganti Foto Profil</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
         <div class="modal-body">
          <img class="img-thumbnail NO-CACHE" src="../img/ee11cbb19052e40b07aac0ca060c23ee/<?php echo $ruser['id_user']?>.png" alt="Card image" style="width:100%;height: 200px;">
          <br>
   
         </div>
        <div class="modal-footer">
          <form action="" method="post" enctype="multipart/form-data" >           
                <div class="form-group ">
                    <label for="foto">Ganti Baru</label>
                    <input type="file" name="foto" id="foto" accept="image/*" class="form-control-file" placeholder="foto" required="">
                </div>           
              <button type="button" data-dismiss="modal" class="btn btn-secondary">Close</button>
              <button  class="btn btn-primary" name="foto">Ganti Foto</button>

              <input type="hidden" name="form" value="update_fopro">
          </form>          
        </div>
    </div>
  </div>
</div>                      
<?php
require_once '../footer.php';
?>
<script>
$('#provinsi').on('change', function (){
     var selectVal = $("#provinsi option:selected").val();
     $('#kota_kab').load('action.php?kab_kota=&id='+selectVal);
});
$(document).ready(function (){           
    $('.NO-CACHE').attr('src',function () { return $(this).attr('src') + "?upload=" + Math.random() });
});
</script>






